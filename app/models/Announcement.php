<?php

class Announcement extends Eloquent {

    protected $table = 'announcements';
    protected $fillable = array('message', 'class', 'active', 'admin_id');

    public static $type = [
        1 => 'Information',
        2 => 'Critical Announcement', 
        3 => 'Important Announcement',
    ];

    public function admin() {
        return $this->hasOne('User', 'id', 'admin_id');
    }

}