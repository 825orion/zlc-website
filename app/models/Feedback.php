<?php



class Feedback extends Eloquent {



    public static $Positions = [

     'JAX_CTR' => 'Enroute (Jacksonville Center)',

     'APP' => 'TRACON (Approach/Departure)',
		
     'TWR' => 'Tower',
		
     'GND' => 'Ground',
		
     'DEL' => 'Delivery',

     'UNKNOWN' => 'Unknown'

    ];



    protected $table = 'feedback';



    protected $fillable = array('controller_id', 'position', 'level', 'comments', 'staff_comments', 'pilot_name', 'pilot_id', 'pilot_email', 'flight_callsign', 'status');



    public function controller() {

        return $this->hasOne('User', 'id', 'controller_id');

    }



    public function getLevelTextAttribute()

    {

    	switch($this->level)

    	{

    		case 0: return "Unsatisfactory";

    		case 1: return "Poor";

    		case 2: return "Fair";

    		case 3: return "Good";

    		case 4: return "Excellent";

    	}

    }



    public function getFeedbackPosAttribute()

    {

        foreach (Feedback::$Positions as $id => $Long) {

            if ($this->position == $id) {

                return $Long;

            }

        }



        return "";

    }



    public function sendPilotEmail()

    {

        return Mail::send('emails.feedbackpilot', ['feedback' => $this], function($message){

            $message->from('no-reply@zjxartcc.org', 'ZJX No-Reply');

            $message->to($this->pilot_email);

            $message->subject('ZJX - Feedback Response');

        });

    }



    public function sendControllerEmail()

    {

        return Mail::send('emails.feedbackcontroller', ['feedback' => $this], function($message){

            $message->from('no-reply@zjxartcc.org', 'ZJX No-Reply');

            $message->to($this->controller->email);

            $message->subject('ZJX - New Feedback');

        });

    }



}