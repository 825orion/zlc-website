<?php

class Weather extends Eloquent {

	protected $table = 'weather';

	public $fillable = ['id', 'type', 'wind', 'baro', 'metar'];
	public $timestamps = false;


	public function getMcoDepartureRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);
		
		if (($wind_kts <= 7))
			return '17R, 18L';
		elseif (($wind_kts > 7) && ($wind_dir >= 95 && $wind_dir <= 275))
			return '17R, 18L';
		elseif (($wind_kts > 7) && ($wind_dir < 95 || $wind_dir > 275))
			return '35L, 36R';
		else
			return '17R, 18L';
	}

	public function getMcoArrivalRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);

		if (($wind_kts <= 7))
			return '17L, 18R';
		elseif (($wind_kts > 7) && ($wind_dir >= 95 && $wind_dir <= 275))
			return '17L, 18R';
		elseif (($wind_kts > 7) && ($wind_dir < 95 || $wind_dir > 275))
			return '35R, 36L';
		else
			return '17L, 18R';
	}
	
	public function getCaeDepartureRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);
		
		if (($wind_kts <= 3))
			return '11, 5';
		elseif (($wind_kts > 3) && ($wind_dir >= 22 && $wind_dir <= 202))
			return '11, 5';
		elseif (($wind_kts > 3) && ($wind_dir < 22 || $wind_dir > 202))
			return '29, 23';
		else
			return '11, 5';
	}

	public function getCaeArrivalRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);

		if (($wind_kts <= 3))
			return '11, 5';
		elseif (($wind_kts > 3) && ($wind_dir >= 22 && $wind_dir <= 202))
			return '11, 5';
		elseif (($wind_kts > 3) && ($wind_dir < 22 || $wind_dir > 202))
			return '29, 23';
		else
			return '11, 5';
	}
	
	public function getChsDepartureRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);
		
		if (($wind_kts <= 3) && ($wind_dir >= 301 || $wind_dir <= 121))
			return '15, 3';
		elseif (($wind_kts <= 3) && ($wind_dir < 301 && $wind_dir > 121))
			return '15, 21';
		elseif (($wind_kts > 3) && ($wind_dir >= 64 && $wind_dir <= 121))
			return '15, 3';
		elseif (($wind_kts > 3) && ($wind_dir > 122 && $wind_dir <= 244))
			return '15, 21';
		elseif (($wind_kts > 3) && ($wind_dir > 244 && $wind_dir <= 301))
			return '33, 21';
		elseif (($wind_kts > 3) && ($wind_dir > 301 || $wind_dir < 64))
			return '33, 3';
		else
			return '15, 3';
	}

	public function getChsArrivalRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);

		if (($wind_kts <= 3) && ($wind_dir >= 301 || $wind_dir <= 121))
			return '15, 3';
		elseif (($wind_kts <= 3) && ($wind_dir < 301 && $wind_dir > 121))
			return '15, 21';
		elseif (($wind_kts > 3) && ($wind_dir >= 64 && $wind_dir <= 121))
			return '15, 3';
		elseif (($wind_kts > 3) && ($wind_dir > 122 && $wind_dir <= 244))
			return '15, 21';
		elseif (($wind_kts > 3) && ($wind_dir > 244 && $wind_dir <= 301))
			return '33, 21';
		elseif (($wind_kts > 3) && ($wind_dir > 301 || $wind_dir < 64))
			return '33, 3';
		else
			return '15, 3';
	}
	
	public function getDabDepartureRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);
		
		if (($wind_kts < 10) && ($wind_dir > 252 || $wind_dir < 60))
			return '7L, 7R, 34';
		elseif (($wind_kts < 10) && ($wind_dir >= 60 && $wind_dir <= 80))
			return '7L, 7R';
		elseif (($wind_kts < 10) && ($wind_dir > 80 && $wind_dir <= 252))
			return '7L, 7R, 16';
		elseif (($wind_kts >= 10) && ($wind_dir >= 332 && $wind_dir < 342))
			return '34, 25L, 25R';
		elseif (($wind_kts >= 10) && ($wind_dir >= 342 && $wind_dir <= 352))
			return '34, 7L, 7R';
		elseif (($wind_kts >= 10) && ($wind_dir > 352 || $wind_dir < 60))
			return '7L, 7R, 34';
		elseif (($wind_kts >= 10) && ($wind_dir >= 60 && $wind_dir <= 80))
			return '7L, 7R';
		elseif (($wind_kts >= 10) && ($wind_dir > 80 && $wind_dir < 152))
			return '7L, 7R, 16';
		elseif (($wind_kts >= 10) && ($wind_dir >= 152 && $wind_dir < 162))
			return '16, 7L, 7R';
		elseif (($wind_kts >= 10) && ($wind_dir >= 162 && $wind_dir <= 172))
			return '16, 25L, 25R';
		elseif (($wind_kts >= 10) && ($wind_dir > 172 && $wind_dir < 240))
			return '25L, 25R, 16';
		elseif (($wind_kts >= 10) && ($wind_dir >= 240 && $wind_dir <= 260))
			return '25L, 25R';
		elseif (($wind_kts >= 10) && ($wind_dir > 260 && $wind_dir < 332))
			return '25L, 25R, 34';
		else
			return '7L, 7R';
	}

	public function getDabArrivalRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);

		if (($wind_kts < 10) && ($wind_dir > 252 || $wind_dir < 60))
			return '7L, 7R, 34';
		elseif (($wind_kts < 10) && ($wind_dir >= 60 && $wind_dir <= 80))
			return '7L, 7R';
		elseif (($wind_kts < 10) && ($wind_dir > 80 && $wind_dir <= 252))
			return '7L, 7R, 16';
		elseif (($wind_kts >= 10) && ($wind_dir >= 332 && $wind_dir < 342))
			return '34, 25L, 25R';
		elseif (($wind_kts >= 10) && ($wind_dir >= 342 && $wind_dir <= 352))
			return '34, 7L, 7R';
		elseif (($wind_kts >= 10) && ($wind_dir > 352 || $wind_dir < 60))
			return '7L, 7R, 34';
		elseif (($wind_kts >= 10) && ($wind_dir >= 60 && $wind_dir <= 80))
			return '7L, 7R';
		elseif (($wind_kts >= 10) && ($wind_dir > 80 && $wind_dir < 152))
			return '7L, 7R, 16';
		elseif (($wind_kts >= 10) && ($wind_dir >= 152 && $wind_dir < 162))
			return '16, 7L, 7R';
		elseif (($wind_kts >= 10) && ($wind_dir >= 162 && $wind_dir <= 172))
			return '16, 25L, 25R';
		elseif (($wind_kts >= 10) && ($wind_dir > 172 && $wind_dir < 240))
			return '25L, 25R, 16';
		elseif (($wind_kts >= 10) && ($wind_dir >= 240 && $wind_dir <= 260))
			return '25L, 25R';
		elseif (($wind_kts >= 10) && ($wind_dir > 260 && $wind_dir < 332))
			return '25L, 25R, 34';
		else
			return '7L, 7R';
	}
	
	public function getJaxDepartureRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);
		
		if (($wind_kts < 10))
			return '14';
		elseif (($wind_kts >= 10) && ($wind_dir >= 347 || $wind_dir <= 167))
			return '14';
		elseif (($wind_kts >= 10) && ($wind_dir > 167 && $wind_dir < 347))
			return '32';
		else
			return '14';
	}

	public function getJaxArrivalRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);

		if (($wind_kts < 10))
			return '8';
		elseif (($wind_kts >= 10) && ($wind_dir >= 347 || $wind_dir <= 167))
			return '8';
		elseif (($wind_kts >= 10) && ($wind_dir > 167 && $wind_dir < 347))
			return '26';
		else
			return '8';
	}
	
	public function getMyrDepartureRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);
		
		if (($wind_kts <= 3))
			return '18';
		elseif (($wind_kts > 3) && ($wind_dir >= 87 && $wind_dir <= 267))
			return '18';
		elseif (($wind_kts > 3) && ($wind_dir < 87 || $wind_dir > 267))
			return '36';
		else
			return '18';
	}

	public function getMyrArrivalRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);

		if (($wind_kts <= 3))
			return '18';
		elseif (($wind_kts > 3) && ($wind_dir >= 87 && $wind_dir <= 267))
			return '18';
		elseif (($wind_kts > 3) && ($wind_dir < 87 || $wind_dir > 267))
			return '36';
		else
			return '18';
	}
	
	public function getPnsDepartureRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);
		
		if (($wind_kts <= 3) && ($wind_dir >= 350 || $wind_dir <= 170))
			return '17, 8';
		elseif (($wind_kts <= 3) && ($wind_dir < 350 && $wind_dir > 170))
			return '17, 26';
		elseif (($wind_kts > 3) && ($wind_dir >= 350 || $wind_dir < 79))
			return '35, 8';
		elseif (($wind_kts > 3) && ($wind_dir >= 79 && $wind_dir < 170))
			return '17, 8';
		elseif (($wind_kts > 3) && ($wind_dir >= 170 && $wind_dir < 259))
			return '17, 26';
		elseif (($wind_kts > 3) && ($wind_dir >= 259 && $wind_dir < 350))
			return '35, 26';
		else
			return '17, 8';
	}

	public function getPnsArrivalRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);

		if (($wind_kts <= 3) && ($wind_dir >= 350 || $wind_dir <= 170))
			return '17, 8';
		elseif (($wind_kts <= 3) && ($wind_dir < 350 && $wind_dir > 170))
			return '17';
		elseif (($wind_kts > 3) && ($wind_dir >= 350 || $wind_dir < 79))
			return '35, 8';
		elseif (($wind_kts > 3) && ($wind_dir >= 79 && $wind_dir < 170))
			return '17, 8';
		elseif (($wind_kts > 3) && ($wind_dir >= 170 && $wind_dir < 259))
			return '17';
		elseif (($wind_kts > 3) && ($wind_dir >= 259 && $wind_dir < 350))
			return '35';
		else
			return '17, 8';
	}
	
	public function getSavDepartureRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);
		
		if (($wind_kts <= 5))
			return '10, 1';
		elseif (($wind_kts > 5) && ($wind_dir >= 276 || $wind_dir < 7))
			return '28, 1';
		elseif (($wind_kts > 5) && ($wind_dir >= 7 && $wind_dir < 96))
			return '10, 1';
		elseif (($wind_kts > 5) && ($wind_dir >= 96 && $wind_dir < 187))
			return '10, 19';
		elseif (($wind_kts > 5) && ($wind_dir >= 187 && $wind_dir < 276))
			return '28, 19';
		else
			return '10, 1';
	}

	public function getSavArrivalRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);
		
		if (($wind_kts <= 5))
			return '10, 1';
		elseif (($wind_kts > 5) && ($wind_dir >= 276 || $wind_dir < 7))
			return '28, 1';
		elseif (($wind_kts > 5) && ($wind_dir >= 7 && $wind_dir < 96))
			return '10, 1';
		elseif (($wind_kts > 5) && ($wind_dir >= 96 && $wind_dir < 187))
			return '10, 19';
		elseif (($wind_kts > 5) && ($wind_dir >= 187 && $wind_dir < 276))
			return '28, 19';
		else
			return '10, 1';

	}
	
	public function getSfbArrivalRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);
		
		if (($wind_kts <= 3))
			return '9L, 9C, 9R';
		elseif (($wind_kts > 3) && ($wind_dir >= 5 && $wind_dir < 185))
			return '9L, 9C, 9R';
		elseif (($wind_kts > 3) && ($wind_dir >= 185 || $wind_dir < 5))
			return '27L, 27C, 27R';
		else
			return '9L, 9C, 9R';

	}
	
	public function getSfbDepartureRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);
		
		if (($wind_kts <= 3))
			return '9L, 9C, 9R';
		elseif (($wind_kts > 3) && ($wind_dir >= 5 && $wind_dir < 185))
			return '9L, 9C, 9R';
		elseif (($wind_kts > 3) && ($wind_dir >= 185 || $wind_dir < 5))
			return '27L, 27C, 27R';
		else
			return '9L, 9C, 9R';

	}
	
	public function getTlhArrivalRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);
		
		if (($wind_dir >= 4 && $wind_dir < 94))
			return '36, 9';
		elseif (($wind_dir >= 94 && $wind_dir < 184))
			return '18, 9';
		elseif (($wind_dir >= 184 && $wind_dir < 274))
			return '18, 27';
		elseif (($wind_dir >= 274 || $wind_dir < 4))
			return '36, 27';
		else
			return '36, 9';

	}
	
	public function getTlhDepartureRunwaysAttribute()
	{
		$wind_kts = $wind_dir = 0;

		if ($this->wind != 'Calm')
			list($wind_dir, $wind_kts) = explode("@", $this->wind);
		
		if (($wind_dir >= 4 && $wind_dir < 94))
			return '36, 9';
		elseif (($wind_dir >= 94 && $wind_dir < 184))
			return '18, 9';
		elseif (($wind_dir >= 184 && $wind_dir < 274))
			return '18, 27';
		elseif (($wind_dir >= 274 || $wind_dir < 4))
			return '36, 27';
		else
			return '36, 9';
	}
	
	
}