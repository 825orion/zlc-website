<?php

class AdminController extends BaseController {

    public function index()
	{
        if(Auth::user()->can('ATM') ||
        Auth::user()->can('DATM') ||
        Auth::user()->can('TA') ||
        Auth::user()->can('ATA') ||
        Auth::user()->can('WM') ||
        Auth::user()->can('AWM') ||
        Auth::user()->can('FE') ||
        Auth::user()->can('AFE') ||
        Auth::user()->can('EC') ||
        Auth::user()->can('AEC') 
        ){
            return View::make('site.home');
        }
		return View::make('admin.home');
    }
    
    public function setLOA($id)
    {
        $user = User::find($id);
        $user->loa = 1;
        ActivityLog::create(['note' => "{$id} marked as LOA", 'user_id' => Auth::id(), 'log_state' => 2, 'log_type' => 9]);
        $user->save();

        return Redirect::to('/admin/roster')->with('message', 'User Updated!');
    }

    public function deploy()
    {
        $user_agent = Request::server('HTTP_USER_AGENT');
        $secret_header = Request::header('X-Hub-Signature');
        list($algo, $secret_hash) = explode('=', $secret_header);

        $calc_secret = hash_hmac($algo, Request::getContent(), Config::get('services.github.secret'));

        if (stripos($user_agent, "GitHub-Hookshot/") === 0 && $secret_hash == $calc_secret)
        {
            $spec = [
                    0 => ["pipe", "r"],
                    1 => ["pipe", "w"],
                    2 => ["pipe", "w"],
            ];

            $process = proc_open('./build.sh', $spec, $pipes, __DIR__ . '/../..');

            if (is_resource($process)) {
                $stdout = stream_get_contents($pipes[1]);
                $stderr = stream_get_contents($pipes[2]);

                fclose($pipes[1]);
                fclose($pipes[2]);

                return Response::json(['stdout' => explode("\n", $stdout), 'stderr' => explode("\n", $stderr)]);
            } else {
                return Response::json(['error' => "Couldn't start the proc_open"]);
            }
        }
        else
        {
            return Redirect::to('/');
        }
    }

    public function setActive($id)
    {
        $user = User::find($id);
        $user->loa = 0;
        ActivityLog::create(['note' => "{$id} marked as Active", 'user_id' => Auth::id(), 'log_state' => 2, 'log_type' => 9]);
        $user->save();


        return Redirect::to('/admin/roster')->with('message', 'User Updated!');
    }

    public function setController($id)
    {
        $user = User::find($id);
        $user->status = 0;
        $visitor = $user->visitor;
        $user->save();

        Mail::send('emails.newmember', ['user' => $user], function($message) use ($user)
        {
            $message->to($user->email)->subject('Welcome to ZJX');
            $message->cc('atm@zjxartcc.org');
            $message->cc('datm@zjxartcc.org');
        });
		
		if ($user->visitor == 0) {
			ActivityLog::create(['note' => "{$id} added as a home controller", 'user_id' => Auth::id(), 'log_state' => 2, 'log_type' => 9]);
		}
		else {
			ActivityLog::create(['note' => "{$id} added as a visiting controller", 'user_id' => Auth::id(), 'log_state' => 2, 'log_type' => 9]);
		}
        
        $forum_user = SMFMember::find($id); 
        $forum_user->id_group = $visitor == 1 ? '20' : '21';
        $forum_user->save();


        return Redirect::to('/admin/roster')->with('message', 'User set as active controller!');
    }
    
    public function sendWarning($id)
    {
        $user = User::find($id);
        Mail::send('emails.activitywarning', ['user' => $user], function($message) use ($user)
        {
            $message->to($user->email)->subject('ZJX Activity Requirements Not Met!');
            $message->cc('atm@zjxartcc.org');
            $message->cc('datm@zjxartcc.org');
        });
        ActivityLog::create(['note' => "{$id} sent activity warning email", 'user_id' => Auth::id(), 'log_state' => 2, 'log_type' => 9]);
        return Redirect::to('/admin/rostertidy')->with('message', '1 week activity warning sent!');
    }

    public function setFormerController($id)
    {
        $user = User::find($id);
        $user->status = 1;
        $user->save();
		
		if ($user->visitor == 0) {
			 ActivityLog::create(['note' => "{$id} marked as a former home controller", 'user_id' => Auth::id(), 'log_state' => 2, 'log_type' => 9]);
		}
		else {
			ActivityLog::create(['note' => "{$id} marked as a former visiting controller", 'user_id' => Auth::id(), 'log_state' => 2, 'log_type' => 9]);
		}

        $forum_user = SMFMember::find($id);
        $forum_user->id_group = 0;
        $forum_user->additional_groups = '';
        $forum_user->save();

        Mail::send('emails.formermember', ['user' => $user], function($message) use ($user)
        {
            $message->to($user->email)->subject('Removed From ZJX Roster');
        });

        return Redirect::to('/admin/roster')->with('message', 'Controller removed from active roster!');
    }

    public function saveVisit()
    {
        $rules = array(
            'id'=>'required|numeric|unique:visit_requests|min:700000|max:2000000',
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required|email',
            'rating_id'=>'required',
            'home'=>'required|min:3|max:8',
            'reason'=>'required'
        );
        
        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails())
        {
            return Redirect::to('/visit')->withErrors($validator)->withInput();
        }
        else
        {
            $rating_id = Input::get('rating_id');
            $request = Visit::create([
                'cid'=>Input::get('id'),
                'first_name'=>Input::get('first_name'),
                'last_name'=>Input::get('last_name'),
                'email'=>Input::get('email'),
                'rating_id'=> $rating_id,
                'home'=>Input::get('home'),
                'reason'=>Input::get('reason'),
                'del' => $rating_id>1 ? 2 : 0,
                'gnd' => $rating_id>1 ? 2 : 0,
                'twr' => $rating_id>2 ? 2 : 0,
                'app' => $rating_id>3 ? 2 : 0,
                'ctr' => $rating_id>4 ? 2 : 0,
            ]);

            
            Mail::send('emails.pendingvrequests', ['request' => $request], function($message){
                $message->from('no-reply@zjxartcc.org', 'ZJX No-Reply');
                $message->to('visit@zjxartcc.org');
                $message->subject('ZJX - New Visiting Request');
            });
            
            return Redirect::to('/')->with('message', 'Visiting Application received and is pending review from the ARTCC staff.');
        }
    }

    public function showVisitRequests()
    {   
        $visit = Visit::where('accepted', '0')->get();
        return View::make('admin.roster.visit')->with('visit', $visit);     
    }

    public function updateVRequest($id)
    {
        $visitr = Visit::find($id);
        $visitr->updated = 1;
        $visitr->sendVReqUpdateEmail();
        $visitr->save();

        ActivityLog::create(['note' => "Sent Visiting Request {$id} 2 week update email", 'user_id' => Auth::id(), 'log_state' => 2, 'log_type' => 10]);

        return Redirect::to('admin/visitreq')->with('message', '2 week notification sent!');
    }

    public function acceptVRequest($id)
    {
        $visitr = Visit::find($id);
        $visitr->accepted = 1;
        $visitr->save();
        if(User::find($visitr->cid)!=null){
            return $this->setController($visitr->cid);
        }

        ActivityLog::create(['note' => "Visiting Request {$id} accepted", 'user_id' => Auth::id(), 'log_state' => 2, 'log_type' => 10]);
        $artccs = ['ZAN','ZDV','HCF','ZLA','ZOA','ZLC','ZSE','ZAB','ZTL','ZFW','ZHU','ZJX','ZKC','ZME','ZMA','ZBW','ZAU','ZOB','ZID','ZMP','ZNY','ZDC','VATUSA','ZHQ'];
        if(in_array(strtoupper($visitr->home),$artccs)){ //No GRP check... let's assume VATUSA training standards are the same
            $User = User::create([
                'id' => $visitr->cid,
                'first_name' => $visitr->first_name,
                'last_name' => $visitr->last_name,
                'email' => $visitr->email,
                'rating_id' => $visitr->rating_id,
                'visitor' => 1,
                'visitor_from' => $visitr->home,
                'canTrain' => 1,
                'del' => $visitr->rating_id>1 ? 2 : 0,
                'gnd' => $visitr->rating_id>1 ? 2 : 0,
                'twr' => $visitr->rating_id>2 ? 2 : 0,
                'app' => $visitr->rating_id>3 ? 2 : 0,
                'ctr' => 0, //Nobody gets a CTR rating automatically
            ]);
        }else{ //GRP check required :-)
            $User = User::create([
                'id' => $visitr->cid,
                'first_name' => $visitr->first_name,
                'last_name' => $visitr->last_name,
                'email' => $visitr->email,
                'rating_id' => $visitr->rating_id,
                'visitor' => 1,
                'visitor_from' => $visitr->home,
                'canTrain' => 1,
                'del' => 0,
                'gnd' => 0,
                'twr' => 0,
                'app' => 0,
                'ctr' => 0,
            ]);
        }
        
        
        Mail::send('emails.newmember', ['user' => $User], function($message) use ($User)
        {
            $message->to($User->email)->subject('Welcome to ZJX');
            $message->cc('atm@zjxartcc.org');
            $message->cc('datm@zjxartcc.org');
        });

        ActivityLog::create(['note' => ''. Input::get('id') .' added as a visiting controller', 'user_id' => Auth::id(), 'log_state' => 1, 'log_type' => 9]);

        // Create SMF user
        Artisan::call('zjx:forum');
            
        return Redirect::to('admin/visitreq')->with('message', 'Visiting request accepted!');
    }

    public function denyVRequest($id)
    {
        $visitr = Visit::find($id);
        $visitr->accepted = 2;
        $visitr->sendVReqDeclineEmail();
        $visitr->save();

        ActivityLog::create(['note' => " Visiting Request {$id} denied", 'user_id' => Auth::id(), 'log_state' => 2, 'log_type' => 10]);
        
        return Redirect::to('/admin/visitreq')->with('message', 'Visiting Request Denied, email sent!');

    }

    public function showMentorHistory()
    {
        //
    }

    public function showAnnouncements()
    {
        $a = Announcement::find(1);
        return View::make('admin.announcements.index')->with('a', $a);
    }

    public function updateAnnouncements()
    {
        $purifier = new HTMLPurifier();
        $message = $purifier->purify(Input::get('message'));

        $a = Announcement::find(1);
        $a->class = Input::get('class');
        $a->message = $message;
        $a->active = Input::get('active') == 1 ? '1' : '0';
        $a->admin_id = Auth::id();
        $a->save();

        ActivityLog::create(['note' => "Front Page Announcement updated", 'user_id' => Auth::id(), 'log_state' => 2, 'log_type' => 1]);

        return Redirect::to('/admin/announcements')->with('message', 'Message Updated!');
    }

    public function showUserLog()
    {
        $user = User::orderBy('last_name', 'ASC')->get()->lists('full_name', 'id');
        return View::make('admin.logs')->with('user', $user);
    }

    public function log($id)
    {
        $user = User::where('id', '=', $id)->first();
        $log = ControllerLog::where('cid', '=', $id)->orderBy('id', 'DESC')->get();
        return View::make('admin.log')->with('log', $log)->with('user', $user);
    }

    public function showActivityLog()
    {
        $activity = ActivityLog::orderBy('created_at', 'DESC')->paginate(400);
        $user = User::where('status', '0')->orderBy('last_name', 'ASC')->get()->lists('backwards_name', 'id');
        $users = [0 => 'Please Select'] + $user;
        return View::make('admin.activitylog')->with('activity', $activity)->with('users', $users);
    }

    public function filterActivityLog($id)
    {
        $activity = ActivityLog::where('user_id', $id)->orderBy('created_at', 'DESC')->get();
        return Response::json($activity);
    }

    public function showRosterCleanup()
    {
        $users = User::where('status', '=', 0)->get();
        return View::make('admin.rostertidy')->with('users', $users);
    }
}
