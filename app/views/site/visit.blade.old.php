@extends('layouts.master')

@section('title')
@parent
| Visit
@stop

@section('content')
<div class="page-heading-two">
    <div class="container">
        <h2>Visiting Application</h2>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <p>Thanks for your interest in visiting Jacksonville Air Route Traffic Control Center (ZJX ARTCC).  If you are interested in transferring into ZJX, you are at the wrong page.  Visit <a href="http://www.vatusa.net">VATUSA</a> to get started.</p>
            <p>Visiting controllers must understand that primary training will be given to ZJX Controllers and <u>ZJX will not provide rating training to visiting controllers</u> in accordance to VATSIM's Visiting And Transferring Controller Policy.  Visitors will only be given procedure training.</p>
            <p>As allowed under the Visiting and Transferring Control Policy, visiting controllers are not permitted to control designated airspaces within the ZJX airspace until they have been certified to do so by a Jacksonville ARTCC Instructor.  This includes ANY MCO position, and Enroute control.</p>
            <p>If you agree with the above and the ZJX Policies, please fill out the form below to get started.</p>
            <br />
            <br />
            {{Form::open(['action'=>'AdminController@saveVisit'])}}
                    <div class="col-md-6">
                        <div class="form-group">
                            {{Form::label('id', 'CID:', ['class'=>'control-label'])}}
                            {{Form::text('id', null, ['class'=>'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('first_name', 'First Name:', ['class'=>'control-label'])}}
                            {{Form::text('first_name', null, ['class'=>'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('last_name', 'Last Name:', ['class'=>'control-label'])}}
                            {{Form::text('last_name', null, ['class'=>'form-control'])}}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{Form::label('email', 'Email Address:', ['class'=>'control-label'])}}
                            {{Form::text('email', null, ['class'=>'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('rating_id', 'Rating:', ['class'=>'control-label'])}}
                            {{Form::select('rating_id', Visit::$RatingShort, 1, ['class' => 'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('home', 'Home ARTCC or Division:', ['class'=>'control-label'])}}
                            {{Form::text('home', null, ['class'=>'form-control', 'placeholder'=>'i.e. ZHU or VATUK'])}}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {{Form::label('reason', 'Why do you want to visit ZJX?:', ['class'=>'control-label'])}}
                            {{Form::textarea('reason', null, ['class'=>'form-control'])}}
                        </div>
                    </div>
		    <div class="col-md-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success submitapp">Submit</button>
                        </div>
                    </div>
            {{Form::close()}}
        </div>
    </div>
</div>
@stop