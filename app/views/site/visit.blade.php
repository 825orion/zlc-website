@extends('layouts.master')

@section('title')
@parent
| Visiting Controller Application
@stop

@section('content')

<!-- Hero -->
<div class="bg-image bg-image-bottom" style="background-image: url('/assets_new/img/photos/visit_lp_bg.jpg');">
	<div class="bg-black-op-75">
		<div class="content content-center text-center">
			<div class="pt-50 pb-20">
				<h1 class="font-w700 text-white mb-10">Visiting Controller Application</h1>
				<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC</h2>
			</div>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<span class="breadcrumb-item active">Visitor Application</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<div class="content content-full">
	<div class="block">
		<div class="block-header block-header-default">
			<h3 class="block-title"><i class="fa fa-exclamation mr-10"></i> Visiting Controller Notice</h3>
		</div>
		<div class="block-content block-content-full">
			<p>Thank you for your interest in visiting Jacksonville ARTCC (ZJX). If you are interested in transferring into ZJX as a home controller, you are at the wrong page. Visit <a href="https://www.vatusa.net" target="_blank">VATUSA</a> to get started.</p>
            <p>Visiting controllers must understand ZJX home controllers will be given priority for training. Additionally, <b>ZJX will not provide rating training to visiting controllers</b> in accordance to VATSIM's Visiting And Transferring Controller Policy. Visitors will only be given local procedure training.</p>
            <p>Visiting controllers are not permitted to control designated airspaces within the ZJX airspace until they have been certified to do so by a Jacksonville ARTCC Instructor. This includes any Orlando (MCO) position and Enroute control. Additionally, ZJX reserves the right to conduct a checkout prior to granting access to minor fields.</p>
            <p>If you agree with the above and ZJX's policies, please fill out the form below to get started.</p>
		</div>
	</div>
	<div class="block">
		<div class="block-header block-header-default">
			<h3 class="block-title"><i class="fa fa-pencil mr-10"></i> Visiting Controller Application</h3>
		</div>
		<div class="block-content block-content-full">
			{{Form::open(['action'=>'AdminController@saveVisit'])}}
                    <div class="col-md-6">
                        <div class="form-group">
                            {{Form::label('id', 'CID:', ['class'=>'control-label'])}}
                            {{Form::text('id', null, ['class'=>'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('first_name', 'First Name:', ['class'=>'control-label'])}}
                            {{Form::text('first_name', null, ['class'=>'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('last_name', 'Last Name:', ['class'=>'control-label'])}}
                            {{Form::text('last_name', null, ['class'=>'form-control'])}}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{Form::label('email', 'Email Address:', ['class'=>'control-label'])}}
                            {{Form::text('email', null, ['class'=>'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('rating_id', 'Rating:', ['class'=>'control-label'])}}
                            {{Form::select('rating_id', Visit::$RatingShort, 1, ['class' => 'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('home', 'Home ARTCC or Division:', ['class'=>'control-label'])}}
                            {{Form::text('home', null, ['class'=>'form-control', 'placeholder'=>'i.e. ZHU or VATUK'])}}
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            {{Form::label('reason', 'Why do you want to visit ZJX?:', ['class'=>'control-label'])}}
                            {{Form::textarea('reason', null, ['class'=>'form-control'])}}
                        </div>
                    </div>
		    <div class="col-md-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success submitapp">Submit</button>
                        </div>
                    </div>
            {{Form::close()}}
		</div>
	</div>
</div>
@stop