@extends('layouts.master')

@section('title')
@parent| Home @stop

@section('content')

 <!-- Main Container -->
				@if(Auth::guest())
				<div class="bg-image" style="background-image: url('/assets_new/img/photos/hp-bg.jpg')">
                    <div class="hero bg-black-op-25">
					<div class="hero-inner">
                        <div class="content content-full text-center">
                                <h1 class="display-3 font-w700 text-white mb-10 invisible" data-toggle="appear" data-class="animated fadeInDown">Welcome to Jacksonville</h1>
                                <h2 class="font-w400 text-white-op mb-50 invisible" data-toggle="appear" data-class="animated fadeInDown">Part of VATUSA and VATSIM</h2>
                                <!--<a class="btn btn-hero btn-noborder btn-rounded btn-success mr-5 mb-10 invisible" data-toggle="appear" data-class="animated fadeInUp" href="#">
                                    <i class="fa fa-info mr-10"></i> About Us
                                </a>
                                <a class="btn btn-hero btn-noborder btn-rounded btn-primary mb-10 invisible" data-toggle="appear" data-class="animated fadeInUp" href="#">
                                    <i class="fa fa-user mr-10"></i> Join Us
                                </a>-->
                            </div>
					</div>
					</div>
                </div>
				@else
				<div class="bg-image" style="background-image: url('/assets_new/img/photos/hp-bg.jpg')">
                    <div class="hero bg-black-op-25">
					<div class="hero-inner">
                        <div class="content content-full text-center">
                                <h1 class="display-3 font-w700 text-white mb-10 invisible" data-toggle="appear" data-class="animated fadeInDown">Welcome Back to Jacksonville, {{Auth::user()->first_name}}.</h1>
                                <h2 class="font-w400 text-white-op mb-50 invisible" data-toggle="appear" data-class="animated fadeInDown"> 
								@if(!empty($stats['month']))
								{{{ $stats['month'] }}}
								@else
								0
								@endif
								hours controlled in <?php echo date ("F") ?></h2>
                                <a class="btn btn-hero btn-noborder btn-rounded btn-primary mb-10 invisible" data-toggle="appear" data-class="animated fadeInUp" href="/profile">
                                    <i class="fa fa-user mr-10"></i> My Profile
                                </a>
                            </div>
					</div>
					</div>
                </div>
				@endif

                <!-- END Hero -->
				<!-- Main Content -->
                <div class="content">
					
					<!-- Announcement -->
					@if($announcements)
						@foreach($announcements as $announcements)
							@if($announcements->class == 1)
							<div class="alert alert-info" role="alert">
								<h3 class="alert-heading font-size-h4 font-w400">Information</h3>
								<p class="mb-0">{{$announcements->message}}</p>
							</div>
							@elseif($announcements->class == 2)
							<div class="alert alert-danger" role="alert">
								<h3 class="alert-heading font-size-h4 font-w400">Critical Announcement</h3>
								<p class="mb-0">{{$announcements->message}}</p>
							</div>
							@elseif($announcements->class == 3)
							<div class="alert alert-warning" role="alert">
								<h3 class="alert-heading font-size-h4 font-w400">Important Announcement</h3>
								<p class="mb-0">{{$announcements->message}}</p>
							</div>
							@endif
						@endforeach
					@endif
					<!-- END Announcement -->
					
					<!-- Left Column -->
                    <div class="row items-push py-30">
                        <div class="col-xl-8">
							
							<!-- Top Controllers Module -->
							<h2 class="content-heading-hp">Top <?php echo date ("F"); ?> Controllers <small>By number of hours.</small></h2>
							<div class="row">
								 <?php $pos=0 ?>
								@if (count($currentTop3) == 3)
									@foreach($currentTop3 as $controller)
										<?php $pos++ ?>
										@if ($pos == 1)
											<div class="col-md-6 col-xl-4"><a class="block block-link-pop bg-gd-sun text-center"><div class="block-content block-content-full"><img class="img-avatar" src="/assets_new/img/avatars/avatar8.jpg" alt="Profile Picture"></div><div class="block-content block-content-full bg-black-op-5"><div class="font-w600 text-white mb-5">{{{ $controller->first_name . " " . $controller->last_name }}}</div><div class="font-size-sm text-white-op">{{{ $controller->duration_time }}}</div></div><div class="block-content block-content-full block-content-sm"><span class="font-w600 font-size-sm text-warning-light"><i class="fa fa-hourglass mr-10"></i>First Place</span></div></a></div>
										@elseif ($pos == 2)
											<div class="col-md-6 col-xl-4"><a class="block block-link-pop bg-muted text-center"><div class="block-content block-content-full"><img class="img-avatar" src="/assets_new/img/avatars/avatar8.jpg" alt="Profile Picture"></div><div class="block-content block-content-full bg-black-op-5"><div class="font-w600 text-white mb-5">{{{ $controller->first_name . " " . $controller->last_name }}}</div><div class="font-size-sm text-white-op">{{{ $controller->duration_time }}}</div></div><div class="block-content block-content-full block-content-sm"><span class="font-w600 font-size-sm text-white-op"><i class="fa fa-hourglass-half mr-10"></i>Second Place</span></div></a></div>
										@elseif ($pos == 3)
											<div class="col-md-6 col-xl-4"><a class="block block-link-pop bg-elegance-dark text-center"><div class="block-content block-content-full"><img class="img-avatar" src="/assets_new/img/avatars/avatar8.jpg" alt="Profile Picture"></div><div class="block-content block-content-full bg-black-op-5"><div class="font-w600 text-white mb-5">{{{ $controller->first_name . " " . $controller->last_name }}}</div><div class="font-size-sm text-white-op">{{{ $controller->duration_time }}}</div></div><div class="block-content block-content-full block-content-sm"><span class="font-w600 font-size-sm text-white-op"><i class="fa fa-hourglass-o mr-10"></i>Third Place</span></div></a></div>
										@endif
									@endforeach
								@elseif (count($currentTop3) == 2)
									@foreach($currentTop3 as $controller)
										<?php $pos++ ?>
										@if ($pos == 1)
											<div class="col-md-6 col-xl-6"><a class="block block-link-pop bg-gd-sun text-center"><div class="block-content block-content-full"><img class="img-avatar" src="/assets_new/img/avatars/avatar8.jpg" alt="Profile Picture"></div><div class="block-content block-content-full bg-black-op-5"><div class="font-w600 text-white mb-5">{{{ $controller->first_name . " " . $controller->last_name }}}</div><div class="font-size-sm text-white-op">{{{ $controller->duration_time }}}</div></div><div class="block-content block-content-full block-content-sm"><span class="font-w600 font-size-sm text-warning-light"><i class="fa fa-hourglass mr-10"></i>First Place</span></div></a></div>
										@elseif ($pos == 2)
											<div class="col-md-6 col-xl-6"><a class="block block-link-pop bg-muted text-center"><div class="block-content block-content-full"><img class="img-avatar" src="/assets_new/img/avatars/avatar8.jpg" alt="Profile Picture"></div><div class="block-content block-content-full bg-black-op-5"><div class="font-w600 text-white mb-5">{{{ $controller->first_name . " " . $controller->last_name }}}</div><div class="font-size-sm text-white-op">{{{ $controller->duration_time }}}</div></div><div class="block-content block-content-full block-content-sm"><span class="font-w600 font-size-sm text-white-op"><i class="fa fa-hourglass-half mr-10"></i>Second Place</span></div></a></div>
										@endif
									@endforeach
								@elseif (count($currentTop3) == 1)
									@foreach($currentTop3 as $controller)
										<?php $pos++ ?>
										@if ($pos == 1)
											<div class="col-md-6 col-xl-12"><a class="block block-link-pop bg-gd-sun text-center"><div class="block-content block-content-full"><img class="img-avatar" src="/assets_new/img/avatars/avatar8.jpg" alt="Profile Picture"></div><div class="block-content block-content-full bg-black-op-5"><div class="font-w600 text-white mb-5">{{{ $controller->first_name . " " . $controller->last_name }}}</div><div class="font-size-sm text-white-op">{{{ $controller->duration_time }}}</div></div><div class="block-content block-content-full block-content-sm"><span class="font-w600 font-size-sm text-warning-light"><i class="fa fa-hourglass mr-10"></i>First Place</span></div></a></div>
										@endif
									@endforeach
								@else
									<div class="col-md-6 col-xl-12"><a class="block block-transparent text-center bg-gd-primary"><div class="block-content bg-black-op-5"><p class="font-w600 text-white">No controllers have logged hours this month</p></div><div class="block-content"><p><i class="fa fa-exclamation-triangle fa-4x text-white-op"></i></p></div></a></div>
								@endif
							</div>
							<!-- END Top Controllers Module -->
							
							<!-- Notices -->
                    		<h2 class="content-heading-hp">Notices <small>Important notices for all controllers.</small></h2>
							
                          		@foreach($news as $n)
								<a class="block block-rounded block-link-shadow" data-toggle="modal" data-target="#{{$n->id_topic}}" href="#{{$n->id_topic}}">
									<div class="block-content block-content-full">
										<p class="font-size-sm text-muted float-sm-right mb-5"><em>{{$n->poster_time}}</em></p>
										<h4 class="font-size-default text-primary mb-0">
											<i class="fa fa-newspaper-o text-muted mr-5"></i> {{$n->subject}}
										</h4>
									</div>
								</a>
								@endforeach
							
							<!-- END Notices -->
							
							<!-- Events -->
                    		<h2 class="content-heading-hp">Events <small>Events in the Jacksonville ARTCC.</small></h2>
							<div class="row">
                           		@forelse ($events as $e)
									@if($e->banner_link == '')
									<div class="col-12">
										<div class="block">
											<div class="block-content">
												<p><a href="/event/{{{$e->id}}}">{{{$e->title}}}</a></p>
											</div>
										</div>
									</div>
									@else
									<div class="col-12">
										<div class="block">
											<div class="block-content">
												<p><a href="/event/{{{$e->id}}}"><img width="100%" src="{{{$e->banner_link}}}"></a></p>
											</div>
										</div>
									</div>
									@endif
									@empty
									<div class="col-md-12 col-xl-12">
										<div class="block-content block-content-full">
											<div class="py-50 text-center bg-white-op-25">
												<div class="font-size-h2 font-w700 mb-0 text-primary"><i class="fa fa-calendar-times-o text-muted mr-5"></i></div>
												<div class="font-size-h2 font-w700 mb-0 text-primary">No events scheduled</div>
												<div class="font-size-sm font-w600 text-uppercase">Check back soon!</div>
											</div>
										</div>
									</div>
									@endforelse
							</div>
                        	<!-- END Events -->
							
                            <hr class="d-xl-none">
                        </div>
						<!-- END Left Column -->

                        <!-- Right Column -->
                        <div class="col-xl-4">
							
							<!-- Discord -->
							<a class="block" href="/discord" target="_blank">
                                <div class="bg-image" style="background-image: url('/assets_new/img/photos/discord-logo.jpg');">
                                    <div class="block-content block-content-full bg-black-op">
                                        <div class="text-center py-50">
                                            <h4 class="font-w700 text-white text-uppercase mb-0">Join our Discord</h4>
                                        </div>
                                    </div>
                                </div>
                            </a>
							<!-- END Discord -->
							
							<!-- Table for Online Facilities -->
							<div class="block">
								<div class="block-header block-header-default">
									<h3 class="block-title"> <i class="si si-earphones mr-10"></i>Facilities</h3>
								</div>
								<div class="block-content" style="padding-top:0px;">
										<table class="table table-borderless table-vcenter" style="text-align: center">
											<tbody>
												<tr align="center" style="border-bottom:1px solid lightgrey;">
													<th style="vertical-align:middle; text-align:center;">Facility</th>
													<th style="vertical-align:middle; text-align:center;">Status</th>
												</tr>
												<tr align="center">
													<td>JAX Center</td><td>
													@if(!empty($online->getCenter()))
													<span class="badge badge-success">Online</span></td></tr>
													@else
													<span class="badge badge-danger">Offline</span></td></tr>
													@endif
								
													<td>F11 TRACON</td><td>
													@if(!empty($online->getF11()))
													<span class="badge badge-success">Online</span></td></tr>
													@else
													<span class="badge badge-danger">Offline</span></td></tr>
													@endif
					
													<td>MCO Airport</td><td>
													@if(!empty($online->getMCO()))
													<span class="badge badge-success"><?= implode("/", $online->getMCO()) ?></span></td></tr>
													@else
													<span class="badge badge-danger">Offline</span></td></tr>
													@endif
													
													<td>CAE Airport</td><td>
													@if(!empty($online->getCAE()))
													<span class="badge badge-success"><?= implode("/", $online->getCAE()) ?></span></td></tr>
													@else
													<span class="badge badge-danger">Offline</span></td></tr>
													@endif

													<td>CHS Airport</td><td>
													@if(!empty($online->getCHS()))
													<span class="badge badge-success"><?= implode("/", $online->getCHS()) ?></span></td></tr>
													@else
													<span class="badge badge-danger">Offline</span></td></tr>
													@endif

													<td>DAB Airport</td><td>
													@if(!empty($online->getDAB()))
													<span class="badge badge-success"><?= implode("/", $online->getDAB()) ?></span></td></tr>
													@else
													<span class="badge badge-danger">Offline</span></td></tr>
													@endif

													<td>JAX Airport</td><td>
													@if(!empty($online->getJAX()))
													<span class="badge badge-success"><?= implode("/", $online->getJAX()) ?></span></td></tr>
													@else
													<span class="badge badge-danger">Offline</span></td></tr>
													@endif

													<td>MYR Airport</td><td>
													@if(!empty($online->getMYR()))
													<span class="badge badge-success"><?= implode("/", $online->getMYR()) ?></span></td></tr>
													@else
													<span class="badge badge-danger">Offline</span></td></tr>
													@endif

													<td>PNS Airport</td><td>
													@if(!empty($online->getPNS()))
													<span class="badge badge-success"><?= implode("/", $online->getPNS()) ?></span></td></tr>
													@else
													<span class="badge badge-danger">Offline</span></td></tr>
													@endif

													<td>SAV Airport</td><td>
													@if(!empty($online->getSAV()))
													<span class="badge badge-success"><?= implode("/", $online->getSAV()) ?></span></td></tr>
													@else
													<span class="badge badge-danger">Offline</span></td></tr>
													@endif


													<td>SFB Airport</td><td>
													@if(!empty($online->getSFB()))
													<span class="badge badge-success"><?= implode("/", $online->getSFB()) ?></span></td></tr>
													@else
													<span class="badge badge-danger">Offline</span></td></tr>
													@endif

													<td>TLH Airport</td><td>
													@if(!empty($online->getTLH()))
													<span class="badge badge-success"><?= implode("/", $online->getTLH()) ?></span></td></tr>
													@else
													<span class="badge badge-danger">Offline</span></td></tr>
													@endif
											</tbody>
										</table>
								</div>
							</div>
							<!-- END Table for Online Facilities -->


							<!-- Table for Online Controllers -->
							<div class="block">
								<div class="block-header block-header-default">
									<h3 class="block-title"> <i class="fa fa-user mr-10"></i>ZJX Controllers Online</h3>
								</div>
								<div class="block-content">
									@if($online->count() == 0)
									<center>There are no controllers online.</center><br />
									@elseif($online->count() == 1)
									<table class="table table-borderless table-vcenter" style="text-align: center">
											<tbody>
												<tr align="center" style="border-bottom:1px solid lightgrey;">
													<th style="vertical-align:middle; text-align:center;">Position</th>
													<th style="vertical-align:middle; text-align:center;">Controller</th>
													<th style="vertical-align:middle; text-align:center;">Duration</th>
												</tr>
												@foreach($online as $controller)
													<tr align="center">
														<td style="vertical-align:middle;">{{ $controller->atc }}</td>
														<td style="vertical-align:middle;">
															@if($controller->controller)
																{{ $controller->controller->full_name }}
															@else
																{{ $controller->name }}
															@endif
														</td>
														<td style="vertical-align:middle;">{{ $controller->duration() }}</td>
													</tr>
												@endforeach
											</tbody>
									</table>
									<center>There is 1 controller online!</center><br />
									@else
									<table class="table table-borderless table-vcenter" style="text-align: center">
											<tbody>
												<tr align="center" style="border-bottom:1px solid lightgrey;">
													<th style="vertical-align:middle; text-align:center;">Position</th>
													<th style="vertical-align:middle; text-align:center;">Controller</th>
													<th style="vertical-align:middle; text-align:center;">Duration</th>
												</tr>
												@foreach($online as $controller)
													<tr align="center">
														<td style="vertical-align:middle;">{{ $controller->atc }}</td>
														<td style="vertical-align:middle;">
															@if($controller->controller)
																{{ $controller->controller->full_name }}
															@else
																{{ $controller->name }}
															@endif
														</td>
														<td style="vertical-align:middle;">{{ $controller->duration() }}</td>
													</tr>
												@endforeach
											</tbody>
									</table>
									<center>There are {{$online->count()}} controllers online!</center><br />
									@endif
								</div>
							</div>
							<!-- END Table for Online Controllers -->
							
							<!-- Table for Weather -->
							<div class="block">
								<div class="block-header block-header-default">
									<h3 class="block-title"> <i class="fa fa-thermometer mr-10"></i>Weather</h3>
								</div>
								<div class="block-content" style="padding-top:0px;">
										<table class="table table-borderless table-vcenter" style="text-align: center">
											<tbody>
												<tr align="center" style="border-bottom:1px solid lightgrey;">
													<th style="vertical-align:middle; text-align:center;">Airport ICAO</th>
													<th style="vertical-align:middle; text-align:center;">Condition</th>
												</tr>
												<?php 
													$wx_array = array('KMCO', 'KCAE', 'KCHS', 'KDAB', 'KJAX', 'KMYR', 'KPNS', 'KSAV', 'KSFB', 'KTLH');
													$wx_object = Weather::whereIn('id', $wx_array)->get();
												?>
											 	@foreach($wx_object as $wx)
													@if($wx->id == "KMCO")
													<tr align="center"><td style="vertical-align:middle;">{{{$wx->id}}}</td>
													<td><button type="button" class="btn btn-alt-{{$wx->type}}" data-toggle="popover" title="{{$wx->id}} METAR" data-placement="top" data-content="{{$wx->metar}}">{{$wx->type}}</button></td></tr>
													@endif
												@endforeach
												@foreach($wx_object as $wx)
													@if($wx->id == "KCAE")
													<tr align="center"><td style="vertical-align:middle;">{{{$wx->id}}}</td>
													<td><button type="button" class="btn btn-alt-{{$wx->type}}" data-toggle="popover" title="{{$wx->id}} METAR" data-placement="top" data-content="{{$wx->metar}}">{{$wx->type}}</button></td></tr>
													@endif
												@endforeach
												@foreach($wx_object as $wx)
													@if($wx->id == "KCHS")
													<tr align="center"><td style="vertical-align:middle;">{{{$wx->id}}}</td>
													<td><button type="button" class="btn btn-alt-{{$wx->type}}" data-toggle="popover" title="{{$wx->id}} METAR" data-placement="top" data-content="{{$wx->metar}}">{{$wx->type}}</button></td></tr>
													@endif
												@endforeach
												@foreach($wx_object as $wx)
													@if($wx->id == "KDAB")
													<tr align="center"><td style="vertical-align:middle;">{{{$wx->id}}}</td>
													<td><button type="button" class="btn btn-alt-{{$wx->type}}" data-toggle="popover" title="{{$wx->id}} METAR" data-placement="top" data-content="{{$wx->metar}}">{{$wx->type}}</button></td></tr>
													@endif
												@endforeach
												@foreach($wx_object as $wx)
													@if($wx->id == "KJAX")
													<tr align="center"><td style="vertical-align:middle;">{{{$wx->id}}}</td>
													<td><button type="button" class="btn btn-alt-{{$wx->type}}" data-toggle="popover" title="{{$wx->id}} METAR" data-placement="top" data-content="{{$wx->metar}}">{{$wx->type}}</button></td></tr>
													@endif
												@endforeach
												@foreach($wx_object as $wx)
													@if($wx->id == "KMYR")
													<tr align="center"><td style="vertical-align:middle;">{{{$wx->id}}}</td>
													<td><button type="button" class="btn btn-alt-{{$wx->type}}" data-toggle="popover" title="{{$wx->id}} METAR" data-placement="top" data-content="{{$wx->metar}}">{{$wx->type}}</button></td></tr>
													@endif
												@endforeach
												@foreach($wx_object as $wx)
													@if($wx->id == "KPNS")
													<tr align="center"><td style="vertical-align:middle;">{{{$wx->id}}}</td>
													<td><button type="button" class="btn btn-alt-{{$wx->type}}" data-toggle="popover" title="{{$wx->id}} METAR" data-placement="top" data-content="{{$wx->metar}}">{{$wx->type}}</button></td></tr>
													@endif
												@endforeach
												@foreach($wx_object as $wx)
													@if($wx->id == "KSAV")
													<tr align="center"><td style="vertical-align:middle;">{{{$wx->id}}}</td>
													<td><button type="button" class="btn btn-alt-{{$wx->type}}" data-toggle="popover" title="{{$wx->id}} METAR" data-placement="top" data-content="{{$wx->metar}}">{{$wx->type}}</button></td></tr>
													@endif
												@endforeach
												@foreach($wx_object as $wx)
													@if($wx->id == "KSFB")
													<tr align="center"><td style="vertical-align:middle;">{{{$wx->id}}}</td>
													<td><button type="button" class="btn btn-alt-{{$wx->type}}" data-toggle="popover" title="{{$wx->id}} METAR" data-placement="top" data-content="{{$wx->metar}}">{{$wx->type}}</button></td></tr>
													@endif
												@endforeach
												@foreach($wx_object as $wx)
													@if($wx->id == "KTLH")
													<tr align="center"><td style="vertical-align:middle;">{{{$wx->id}}}</td>
													<td><button type="button" class="btn btn-alt-{{$wx->type}}" data-toggle="popover" title="{{$wx->id}} METAR" data-placement="top" data-content="{{$wx->metar}}">{{$wx->type}}</button></td></tr>
													@endif
												@endforeach
											</tbody>
										</table>
								</div>
							</div>
							<!-- END Table for Weather -->
							
                        </div>
                        <!-- END Right Column -->
                    </div>
                </div>

				@foreach($news as $n)
				<div class="modal fade" id="{{$n->id_topic}}" tabindex="-1" role="dialog" aria-labelledby="{{$n->id_topic}}" style="display: none;" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
							<div class="block block-themed block-transparent mb-0">
								<div class="block-header bg-primary-dark">
									<h3 class="block-title">{{$n->subject}}</h3>
									<div class="block-options">
										<button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
											<i class="si si-close"></i>
										</button>
									</div>
								</div>
								<div class="block-content">
									<p><em>{{$n->poster_time}}</em></p>
									<p>{{$n->body}}</p>
								</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-alt-danger" data-dismiss="modal">
										<i class="fa fa-close"></i> Close
									</button>
								</div>
						</div>
					</div>
				</div>
				@endforeach
@stop
