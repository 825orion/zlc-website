@extends('layouts.master')

@section('title')
@parent
| Weather & Runways
@stop

@section('content')

<!-- Hero -->
<div class="bg-image bg-image-bottom" style="background-image: url('/assets_new/img/photos/runway_lp_bg.jpg');">
	<div class="bg-black-op-75">
		<div class="content content-center text-center">
			<div class="pt-50 pb-20">
				<h1 class="font-w700 text-white mb-10">Weather & Runways</h1>
				<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC</h2>
			</div>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<span class="breadcrumb-item active">Pilots</span>
			<span class="breadcrumb-item active">Weather & Runways</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<!-- Page Content -->
<div class="content content-full">

	<div class="row">
		<!-- Weather Information Notice -->
		<div class="col-md-12">
			<div class="alert alert-warning alert-dismissable" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h3 class="alert-heading font-size-h4 font-w400">Notice</h3>
				<p class="mb-0">The suggested runway configurations below are for planning purposes. Controllers may opt for different runway configurations depending on other operational factors.</p>
			</div>
		</div>
		<!-- END Weather Information Notice -->
		
		<!-- KMCO Weather Information -->
		<div class="col-md-6">
			<div class="block block-themed">
				@if($kmco->type == 'VFR')
				<div class="block-header bg-vfr">
				@elseif($kmco->type == 'MVFR')
				<div class="block-header bg-mvfr">
				@elseif($kmco->type == 'IFR')
				<div class="block-header bg-ifr">
				@elseif($kmco->type == 'LIFR')
				<div class="block-header bg-lifr">
				@endif
					<h3 class="block-title">Orlando International Airport (MCO)<span class="pull-right"><b>{{{$kmco->type}}}</b></span></h3>
				</div>
				<div class="block-content">
					<p>Suggested Departure Runways: {{ $kmco->mco_departure_runways }}
					<br />
					Suggested Arrival Runways: {{ $kmco->mco_arrival_runways }}
					<hr>
					<small>{{{$kmco->metar}}}</small></p>
				</div>
			</div>
			</div>
			<!-- END KMCO Weather Information -->
				
			<!-- KCAE Weather Information -->
			<div class="col-md-6">
				<div class="block block-themed">
				@if($kcae->type == 'VFR')
				<div class="block-header bg-vfr">
				@elseif($kcae->type == 'MVFR')
				<div class="block-header bg-mvfr">
				@elseif($kcae->type == 'IFR')
				<div class="block-header bg-ifr">
				@elseif($kcae->type == 'LIFR')
				<div class="block-header bg-lifr">
				@endif
					<h3 class="block-title">Columbia Metropolitan Airport (CAE)<span class="pull-right"><b>{{{$kcae->type}}}</b></span></h3>
				</div>
				<div class="block-content">
					<p>Suggested Departure Runways: {{ $kcae->cae_departure_runways }}
					<br />
					Suggested Arrival Runways: {{ $kcae->cae_arrival_runways }}
					<hr>
					<small>{{{$kcae->metar}}}</small></p>
					</div>
				</div>
			</div>
			<!-- END KCAE Weather Information -->
			
			<!-- KCHS Weather Information -->
			<div class="col-md-6">
				<div class="block block-themed">
				@if($kchs->type == 'VFR')
				<div class="block-header bg-vfr">
				@elseif($kchs->type == 'MVFR')
				<div class="block-header bg-mvfr">
				@elseif($kchs->type == 'IFR')
				<div class="block-header bg-ifr">
				@elseif($kchs->type == 'LIFR')
				<div class="block-header bg-lifr">
				@endif
					<h3 class="block-title">Charleston International Airport (CHS)<span class="pull-right"><b>{{{$kchs->type}}}</b></span></h3>
				</div>
				<div class="block-content">
					<p>Suggested Departure Runways: {{ $kchs->chs_departure_runways }}
					<br />
					Suggested Arrival Runways: {{ $kchs->chs_arrival_runways }}
					<hr>
					<small>{{{$kchs->metar}}}</small></p>
					</div>
				</div>
			</div>
			<!-- END KCHS Weather Information -->
					
			<!-- KDAB Weather Information -->
			<div class="col-md-6">
				<div class="block block-themed">
				@if($kdab->type == 'VFR')
				<div class="block-header bg-vfr">
				@elseif($kdab->type == 'MVFR')
				<div class="block-header bg-mvfr">
				@elseif($kdab->type == 'IFR')
				<div class="block-header bg-ifr">
				@elseif($kdab->type == 'LIFR')
				<div class="block-header bg-lifr">
				@endif
					<h3 class="block-title">Daytona Beach International Airport (DAB)<span class="pull-right"><b>{{{$kdab->type}}}</b></span></h3>
				</div>
				<div class="block-content">
					<p>Suggested Departure Runways: {{ $kdab->dab_departure_runways }}
					<br />
					Suggested Arrival Runways: {{ $kdab->dab_arrival_runways }}
					<hr>
					<small>{{{$kdab->metar}}}</small></p>
					</div>
				</div>
			</div>
			<!-- END KDAB Weather Information -->
				
			<!-- KJAX Weather Information -->
			<div class="col-md-6">
				<div class="block block-themed">
				@if($kjax->type == 'VFR')
				<div class="block-header bg-vfr">
				@elseif($kjax->type == 'MVFR')
				<div class="block-header bg-mvfr">
				@elseif($kjax->type == 'IFR')
				<div class="block-header bg-ifr">
				@elseif($kjax->type == 'LIFR')
				<div class="block-header bg-lifr">
				@endif
					<h3 class="block-title">Jacksonville International Airport (JAX)<span class="pull-right"><b>{{{$kjax->type}}}</b></span></h3>
				</div>
				<div class="block-content">
					<p>Suggested Departure Runways: {{ $kjax->jax_departure_runways }}
					<br />
					Suggested Arrival Runways: {{ $kjax->jax_arrival_runways }}
					<hr>
					<small>{{{$kjax->metar}}}</small></p>
					</div>
				</div>
			</div>
			<!-- END KJAX Weather Information -->
					
			<!-- KMYR Weather Information -->
			<div class="col-md-6">
				<div class="block block-themed">
				@if($kmyr->type == 'VFR')
				<div class="block-header bg-vfr">
				@elseif($kmyr->type == 'MVFR')
				<div class="block-header bg-mvfr">
				@elseif($kmyr->type == 'IFR')
				<div class="block-header bg-ifr">
				@elseif($kmyr->type == 'LIFR')
				<div class="block-header bg-lifr">
				@endif
					<h3 class="block-title">Myrtle Beach International Airport (MYR)<span class="pull-right"><b>{{{$kmyr->type}}}</b></span></h3>
				</div>
				<div class="block-content">
					<p>Suggested Departure Runways: {{ $kmyr->myr_departure_runways }}
					<br />
					Suggested Arrival Runways: {{ $kmyr->myr_arrival_runways }}
					<hr>
					<small>{{{$kmyr->metar}}}</small></p>
					</div>
				</div>
			</div>
			<!-- END KMYR Weather Information -->
					
			<!-- KPNS Weather Information -->
			<div class="col-md-6">
				<div class="block block-themed">
				@if($kpns->type == 'VFR')
				<div class="block-header bg-vfr">
				@elseif($kpns->type == 'MVFR')
				<div class="block-header bg-mvfr">
				@elseif($kpns->type == 'IFR')
				<div class="block-header bg-ifr">
				@elseif($kpns->type == 'LIFR')
				<div class="block-header bg-lifr">
				@endif
					<h3 class="block-title">Pensacola International Airport (PNS)<span class="pull-right"><b>{{{$kpns->type}}}</b></span></h3>
				</div>
				<div class="block-content">
					<p>Suggested Departure Runways: {{ $kpns->pns_departure_runways }}
					<br />
					Suggested Arrival Runways: {{ $kpns->pns_arrival_runways }}
					<hr>
					<small>{{{$kpns->metar}}}</small></p>
					</div>
				</div>
			</div>
			<!-- END KPNS Weather Information -->
					
			<!-- KSAV Weather Information -->
			<div class="col-md-6">
				<div class="block block-themed">
				@if($ksav->type == 'VFR')
				<div class="block-header bg-vfr">
				@elseif($ksav->type == 'MVFR')
				<div class="block-header bg-mvfr">
				@elseif($ksav->type == 'IFR')
				<div class="block-header bg-ifr">
				@elseif($ksav->type == 'LIFR')
				<div class="block-header bg-lifr">
				@endif
					<h3 class="block-title">Savannah/Hilton Head International Airport (SAV)<span class="pull-right"><b>{{{$ksav->type}}}</b></span></h3>
				</div>
				<div class="block-content">
					<p>Suggested Departure Runways: {{ $ksav->sav_departure_runways }}
					<br />
					Suggested Arrival Runways: {{ $ksav->sav_arrival_runways }}
					<hr>
					<small>{{{$ksav->metar}}}</small></p>
					</div>
				</div>
			</div>
			<!-- END KSAV Weather Information -->
					
			<!-- KSFB Weather Information -->
			<div class="col-md-6">
				<div class="block block-themed">
				@if($ksfb->type == 'VFR')
				<div class="block-header bg-vfr">
				@elseif($ksfb->type == 'MVFR')
				<div class="block-header bg-mvfr">
				@elseif($ksfb->type == 'IFR')
				<div class="block-header bg-ifr">
				@elseif($ksfb->type == 'LIFR')
				<div class="block-header bg-lifr">
				@endif
					<h3 class="block-title">Orlando Sanford International Airport (SFB)<span class="pull-right"><b>{{{$ksfb->type}}}</b></span></h3>
				</div>
				<div class="block-content">
					<p>Suggested Departure Runways: {{ $ksfb->sfb_departure_runways }}
					<br />
					Suggested Arrival Runways: {{ $ksfb->sfb_arrival_runways }}
					<hr>
					<small>{{{$ksfb->metar}}}</small></p>
					</div>
				</div>
			</div>
			<!-- END KSFB Weather Information -->
					
			<!-- KTLH Weather Information -->
			<div class="col-md-6">
				<div class="block block-themed">
				@if($ktlh->type == 'VFR')
				<div class="block-header bg-vfr">
				@elseif($ktlh->type == 'MVFR')
				<div class="block-header bg-mvfr">
				@elseif($ktlh->type == 'IFR')
				<div class="block-header bg-ifr">
				@elseif($ktlh->type == 'LIFR')
				<div class="block-header bg-lifr">
				@endif
					<h3 class="block-title">Tallahassee International Airport (TLH)<span class="pull-right"><b>{{{$ktlh->type}}}</b></span></h3>
				</div>
				<div class="block-content">
					<p>Suggested Departure Runways: {{ $ktlh->tlh_departure_runways }}
					<br />
					Suggested Arrival Runways: {{ $ktlh->tlh_arrival_runways }}
					<hr>
					<small>{{{$ktlh->metar}}}</small></p>
					</div>
				</div>
			</div>
			<!-- END KTLH Weather Information -->
				
			
	</div>
		
</div>
<!-- END Page Content -->


@stop
