@extends('layouts.master')

@section('title')
@parent
| Roster
@stop

@section('content')

<!-- Hero -->
<div class="bg-image bg-image-bottom" style="background-image: url('/assets_new/img/photos/roster_lp_bg.jpg');">
	<div class="bg-black-op-75">
		<div class="content content-center text-center">
			<div class="pt-50 pb-20">
				<h1 class="font-w700 text-white mb-10">Roster</h1>
				<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC</h2>
			</div>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<span class="breadcrumb-item active">Controller</span>
			<span class="breadcrumb-item active">Roster</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<!-- Page Content -->
<div class="content content-full">
	<!-- Staff Controller Roster -->
	<div class="block">
		<div class="block-header block-header-default">
			<h3 class="block-title"><i class="fa fa-home mr-10"></i> ARTCC Staff</h3>
		</div>
		<div class="block-content block-content-full">
			<div class="table-responsive">
			<table class="table table-bordered table-striped table-vcenter table-responsive">
				<thead>
					<tr>
						<th class="text-center">Full Name</th>
						<th class="text-center">VATSIM ID</th>
						<th class="text-center">Status</th>
						<th class="text-center">Position</th>
						<th class="text-center">Rating</th>
						<th class="text-center">DEL</th>
						<th class="text-center">GND</th>
						<th class="text-center">TWR</th>
						<th class="text-center">TRACON</th>
						<th class="text-center">CTR</th>
					</tr>
				</thead>
				<tbody>
					
					@forelse($homecontroller as $h)
					@if($h->hasStaff())
					<tr>
						@if(Auth::check())
						@if(Auth::user()->can('profile'))
							<td class="text-center"><a href="/profile/{{$h->id}}">{{{$h->backwards_name}}}</a></td>
						@else
						<td class="text-center">{{{$h->backwards_name}}}</td>
						@endif
					@else
						<td class="text-center">{{{$h->backwards_name}}}</td>
					@endif
						<td class="text-center">{{{$h->id}}}</td>
						<td class="text-center">
							@if($h->loa == '1') <span class="badge badge-info">Leave</span> @else <span class="badge badge-success">Active</span> @endif
						</td>
						<td class="text-center">{{$h->userFirstStaff()}}</td>
						<td class="text-center">{{{$h->rating_short}}}</td>
						
						<td class="text-center">@if($h->del == '0') <span class="badge badge-secondary">None</span> @elseif($h->del == '1') <span class="badge badge-warning">Minor Solo</span> @elseif($h->del == '2') <span class="badge badge-danger">Minor</span> @elseif($h->del == '3') <span class="badge badge-warning">Major Solo</span> @elseif($h->del == '4') <span class="badge badge-success">Major</span> @endif</td>
						
						<td class="text-center">@if($h->gnd == '0') <span class="badge badge-secondary">None</span> @elseif($h->gnd == '1') <span class="badge badge-warning">Minor Solo</span> @elseif($h->gnd == '2') <span class="badge badge-danger">Minor</span> @elseif($h->gnd == '3') <span class="badge badge-warning">Major Solo</span> @elseif($h->gnd == '4') <span class="badge badge-success">Major</span> @endif</td>
						
						<td class="text-center">@if($h->twr == '0') <span class="badge badge-secondary">None</span> @elseif($h->twr == '1') <span class="badge badge-warning">Minor Solo</span> @elseif($h->twr == '2') <span class="badge badge-danger">Minor</span> @elseif($h->twr == '3') <span class="badge badge-warning">Major Solo</span> @elseif($h->twr == '4') <span class="badge badge-success">Major</span> @endif</td>
						
						<td class="text-center">@if($h->app == '0') <span class="badge badge-secondary">None</span> @elseif($h->app == '1') <span class="badge badge-warning">Minor Solo</span> @elseif($h->app == '2') <span class="badge badge-danger">Minor</span> @elseif($h->app == '3') <span class="badge badge-warning">Major Solo</span> @elseif($h->app == '4') <span class="badge badge-success">Major</span> @endif</td>
						
						<td class="text-center">@if($h->ctr == '0') <span class="badge badge-secondary">None</span> @elseif($h->ctr == '1') <span class="badge badge-warning">Solo</span> @elseif($h->ctr == '2') <span class="badge badge-success">Certified</span> @endif</td>
					</tr>
					@endif
					@empty
					@endforelse
				</tbody>
			</table>
			</div>
		</div>
	</div>
	<!-- END Staff Controller Roster -->
	
	<!-- Training Staff Controller Roster -->
	<div class="block">
		<div class="block-header block-header-default">
			<h3 class="block-title"><i class="fa fa-pencil mr-10"></i> ARTCC Training Staff</h3>
		</div>
		<div class="block-content block-content-full">
			<div class="table-responsive">
			<table class="table table-bordered table-striped table-vcenter table-responsive">
				<thead>
					<tr>
						<th class="text-center">Full Name</th>
						<th class="text-center">VATSIM ID</th>
						<th class="text-center">Status</th>
						<th class="text-center">Position</th>
						<th class="text-center">Rating</th>
						<th class="text-center">DEL</th>
						<th class="text-center">GND</th>
						<th class="text-center">TWR</th>
						<th class="text-center">TRACON</th>
						<th class="text-center">CTR</th>
					</tr>
				</thead>
				<tbody>
					
					@forelse($homecontroller as $h)
					@if($h->hasTrainingStaff())
					<tr>
						@if(Auth::check())
						@if(Auth::user()->can('profile'))
							<td class="text-center"><a href="/profile/{{$h->id}}">{{{$h->backwards_name}}}</a></td>
						@else
						<td class="text-center">{{{$h->backwards_name}}}</td>
						@endif
					@else
						<td class="text-center">{{{$h->backwards_name}}}</td>
					@endif
						<td class="text-center">{{{$h->id}}}</td>
						<td class="text-center">
							@if($h->loa == '1') <span class="badge badge-info">Leave</span> @else <span class="badge badge-success">Active</span> @endif
						</td>
						<td class="text-center">@if ($h->hasRole('MTR')) Mentor @elseif ($h->hasRole('INS')) Instructor @endif</td>
						<td class="text-center">{{{$h->rating_short}}}</td>
						
						<td class="text-center">@if($h->del == '0') <span class="badge badge-secondary">None</span> @elseif($h->del == '1') <span class="badge badge-warning">Minor Solo</span> @elseif($h->del == '2') <span class="badge badge-danger">Minor</span> @elseif($h->del == '3') <span class="badge badge-warning">Major Solo</span> @elseif($h->del == '4') <span class="badge badge-success">Major</span> @endif</td>
						
						<td class="text-center">@if($h->gnd == '0') <span class="badge badge-secondary">None</span> @elseif($h->gnd == '1') <span class="badge badge-warning">Minor Solo</span> @elseif($h->gnd == '2') <span class="badge badge-danger">Minor</span> @elseif($h->gnd == '3') <span class="badge badge-warning">Major Solo</span> @elseif($h->gnd == '4') <span class="badge badge-success">Major</span> @endif</td>
						
						<td class="text-center">@if($h->twr == '0') <span class="badge badge-secondary">None</span> @elseif($h->twr == '1') <span class="badge badge-warning">Minor Solo</span> @elseif($h->twr == '2') <span class="badge badge-danger">Minor</span> @elseif($h->twr == '3') <span class="badge badge-warning">Major Solo</span> @elseif($h->twr == '4') <span class="badge badge-success">Major</span> @endif</td>
						
						<td class="text-center">@if($h->app == '0') <span class="badge badge-secondary">None</span> @elseif($h->app == '1') <span class="badge badge-warning">Minor Solo</span> @elseif($h->app == '2') <span class="badge badge-danger">Minor</span> @elseif($h->app == '3') <span class="badge badge-warning">Major Solo</span> @elseif($h->app == '4') <span class="badge badge-success">Major</span> @endif</td>
						
						<td class="text-center">@if($h->ctr == '0') <span class="badge badge-secondary">None</span> @elseif($h->ctr == '1') <span class="badge badge-warning">Solo</span> @elseif($h->ctr == '2') <span class="badge badge-success">Certified</span> @endif</td>
					</tr>
					@endif
					@empty
					@endforelse
				</tbody>
			</table>
			</div>
		</div>
	</div>
	<!-- END Training Staff Controller Roster -->
	
	<!-- Home Controller Roster -->
	<div class="block">
		<div class="block-header block-header-default">
			<h3 class="block-title"><i class="fa fa-home mr-10"></i> Home Controller Roster</h3>
		</div>
		<div class="block-content block-content-full">
			<div class="table-responsive">
			<table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
				<thead>
					<tr>
						<th class="text-center">Full Name</th>
						<th class="text-center">VATSIM ID</th>
						<th class="text-center">Status</th>
						<th class="text-center">Membership</th>
						<th class="text-center">Rating</th>
						<th class="text-center">DEL</th>
						<th class="text-center">GND</th>
						<th class="text-center">TWR</th>
						<th class="text-center">TRACON</th>
						<th class="text-center">CTR</th>
					</tr>
				</thead>
				<tbody>
					
					@forelse($homecontroller as $h)
					<tr>
						@if(Auth::check())
						@if(Auth::user()->can('profile'))
							<td class="text-center"><a href="/profile/{{$h->id}}">{{{$h->backwards_name}}}</a></td>
						@else
						<td class="text-center">{{{$h->backwards_name}}}</td>
						@endif
					@else
						<td class="text-center">{{{$h->backwards_name}}}</td>
					@endif
						<td class="text-center">{{{$h->id}}}</td>
						<td class="text-center">
							@if($h->loa == '1') <span class="badge badge-info">Leave</span> @else <span class="badge badge-success">Active</span> @endif
						</td>
						<td class="text-center">Home Controller</td>
						<td class="text-center">{{{$h->rating_short}}}</td>
						
						<td class="text-center">@if($h->del == '0') <span class="badge badge-secondary">None</span> @elseif($h->del == '1') <span class="badge badge-warning">Minor Solo</span> @elseif($h->del == '2') <span class="badge badge-danger">Minor</span> @elseif($h->del == '3') <span class="badge badge-warning">Major Solo</span> @elseif($h->del == '4') <span class="badge badge-success">Major</span> @endif</td>
						
						<td class="text-center">@if($h->gnd == '0') <span class="badge badge-secondary">None</span> @elseif($h->gnd == '1') <span class="badge badge-warning">Minor Solo</span> @elseif($h->gnd == '2') <span class="badge badge-danger">Minor</span> @elseif($h->gnd == '3') <span class="badge badge-warning">Major Solo</span> @elseif($h->gnd == '4') <span class="badge badge-success">Major</span> @endif</td>
						
						<td class="text-center">@if($h->twr == '0') <span class="badge badge-secondary">None</span> @elseif($h->twr == '1') <span class="badge badge-warning">Minor Solo</span> @elseif($h->twr == '2') <span class="badge badge-danger">Minor</span> @elseif($h->twr == '3') <span class="badge badge-warning">Major Solo</span> @elseif($h->twr == '4') <span class="badge badge-success">Major</span> @endif</td>
						
						<td class="text-center">@if($h->app == '0') <span class="badge badge-secondary">None</span> @elseif($h->app == '1') <span class="badge badge-warning">Minor Solo</span> @elseif($h->app == '2') <span class="badge badge-danger">Minor</span> @elseif($h->app == '3') <span class="badge badge-warning">Major Solo</span> @elseif($h->app == '4') <span class="badge badge-success">Major</span> @endif</td>
						
						<td class="text-center">@if($h->ctr == '0') <span class="badge badge-secondary">None</span> @elseif($h->ctr == '1') <span class="badge badge-warning">Solo</span> @elseif($h->ctr == '2') <span class="badge badge-success">Certified</span> @endif</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
			</div>
		</div>
	</div>
	<!-- END Home Controller Roster -->
	
	<!-- Visiting Controller Roster -->
	<div class="block">
		<div class="block-header block-header-default">
			<h3 class="block-title"><i class="fa fa-briefcase mr-10"></i> Visiting Controller Roster</h3>
		</div>
		<div class="block-content block-content-full">
			<div class="table-responsive">
			<table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination">
				<thead>
					<tr>
						<th class="text-center">Full Name</th>
						<th class="text-center">VATSIM ID</th>
						<th class="text-center">Status</th>
						<th class="text-center">Membership</th>
						<th class="text-center">Rating</th>
						<th class="text-center">DEL</th>
						<th class="text-center">GND</th>
						<th class="text-center">TWR</th>
						<th class="text-center">TRACON</th>
						<th class="text-center">CTR</th>
					</tr>
				</thead>
				<tbody>
					
					@forelse($visitcontroller as $h)
					<tr>
						@if(Auth::check())
						@if(Auth::user()->can('profile'))
							<td class="text-center"><a href="/profile/{{$h->id}}">{{{$h->backwards_name}}}</a></td>
						@else
						<td class="text-center">{{{$h->backwards_name}}}</td>
						@endif
					@else
						<td class="text-center">{{{$h->backwards_name}}}</td>
					@endif
						<td class="text-center">{{{$h->id}}}</td>
						<td class="text-center">
							@if($h->loa == '1') <span class="badge badge-info">Leave</span> @else <span class="badge badge-success">Active</span> @endif
						</td>
						<td class="text-center">Visiting Controller</td>
						<td class="text-center">{{{$h->rating_short}}}</td>
						
						<td class="text-center">@if($h->del == '0') <span class="badge badge-secondary">None</span> @elseif($h->del == '1') <span class="badge badge-warning">Minor Solo</span> @elseif($h->del == '2') <span class="badge badge-danger">Minor</span> @elseif($h->del == '3') <span class="badge badge-warning">Major Solo</span> @elseif($h->del == '4') <span class="badge badge-success">Major</span> @endif</td>
						
						<td class="text-center">@if($h->gnd == '0') <span class="badge badge-secondary">None</span> @elseif($h->gnd == '1') <span class="badge badge-warning">Minor Solo</span> @elseif($h->gnd == '2') <span class="badge badge-danger">Minor</span> @elseif($h->gnd == '3') <span class="badge badge-warning">Major Solo</span> @elseif($h->gnd == '4') <span class="badge badge-success">Major</span> @endif</td>
						
						<td class="text-center">@if($h->twr == '0') <span class="badge badge-secondary">None</span> @elseif($h->twr == '1') <span class="badge badge-warning">Minor Solo</span> @elseif($h->twr == '2') <span class="badge badge-danger">Minor</span> @elseif($h->twr == '3') <span class="badge badge-warning">Major Solo</span> @elseif($h->twr == '4') <span class="badge badge-success">Major</span> @endif</td>
						
						<td class="text-center">@if($h->app == '0') <span class="badge badge-secondary">None</span> @elseif($h->app == '1') <span class="badge badge-warning">Minor Solo</span> @elseif($h->app == '2') <span class="badge badge-danger">Minor</span> @elseif($h->app == '3') <span class="badge badge-warning">Major Solo</span> @elseif($h->app == '4') <span class="badge badge-success">Major</span> @endif</td>
						
						<td class="text-center">@if($h->ctr == '0') <span class="badge badge-secondary">None</span> @elseif($h->ctr == '1') <span class="badge badge-warning">Solo</span> @elseif($h->ctr == '2') <span class="badge badge-success">Certified</span> @endif</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
			</div>
		</div>
	</div>
	<!-- END Visiting Controller Roster -->
	
	
	
</div>

<!-- END Page Content -->


@stop
