@extends('layouts.master')

@section('title')
@parent
| Event
@stop

@section('content')

<!-- Hero -->
<div class="bg-image bg-image-bottom" style="background-image: url('/assets_new/img/photos/events_lp_bg.jpg');">
	<div class="bg-black-op-75">
		<div class="content content-center text-center">
			<div class="pt-50 pb-20">
				<h1 class="font-w700 text-white mb-10">Event Information & Sign-Up</h1>
				<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC</h2>
			</div>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<span class="breadcrumb-item active">Events</span>
			@if($event)
			<span class="breadcrumb-item active">{{{$event->title}}}</span>
			@else
			<span class="breadcrumb-item active">Error</span>
			@endif
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

@if($event !== null)
<!-- Page Content -->
<div class="content content-full">
	<div class="row">
		<div class="col-12">
			<div class="block block-themed block-rounded">
				<div class="block-header bg-primary-dark">
					<h3 class="block-title">Event Information</h3> 
					@if(Auth::guest()===false)
						@if(Auth::user()->can('events'))
						<a href="../admin/events/{{{$event->id}}}">Go to Admin Page</a>
						@endif
					@endif
					<div class="block-options">
					</div>
				</div>
				<div class="block-content">
					<div class="row">
		<div class="col-sm-12">
			<div class="well">
				<h4><img width="100%" src="{{{$event->banner_link}}}"></h4>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-7">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="font-w300">Event <small>{{{$event->title}}}</small></h3>
				</div>
				<div class="panel-body">
					<h3 class="font-w300">Host <small>{{{$event->host_long}}}</small></h3>
					<h3 class="font-w300">Start <small>{{{$event->event_start}}}z</small></h3>
					<h3 class="font-w300">End <small>{{{$event->event_end}}}z</small></h3>
					<h3 class="font-w300">Event Description</h3>
					<p>{{$event->description}}</p>
				</div>
			</div>
		</div>
		<div class="col-sm-5">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="font-w300">Position Assignments</h3>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th width="30%" style="text-align: center;">Position</th>
									<th width="60%" style="text-align: center;">Controller</th>
									@if(Auth::check())
									<th width="10%" style="text-align: center;">Actions</th>
								   @endif
								</tr>
							</thead>
							<tbody>
								@foreach($event->positions->sortBy('order_index') as $position)
								<tr>
									<td style="text-align: center;">{{$position->name}}</td>
									<td style="text-align: center;">{{$position->user ? "<b>".$position->user->full_name."</b>" : "Available"}}</td>
									@if(Auth::check())						
									@if($position->controller_id == Auth::id())
									<td style="text-align: center;">{{Form::open(['action'=>['EventController@selfUnnasign', $position->id], 'metho'=>'POST'])}}
											<button type="submit" class="btn btn-warning btn-sm simple-tooltip" title="Self Unnasign"><i class="fa fa-times"></i></button>
										{{Form::close()}}
									</td>
									@else
									<td></td>
									@endif
									@endif
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					@if(Auth::check())
						@if(is_null($userpos))
						{{ Form::open(['action' => ['EventController@requestPosition', $event->id], 'method' => 'POST']) }}
						<h3 class="font-w300">Request a Position</h3>
						<div class="row"><div class="col-sm-12"><div class="form-group"><p>Requested positions are all positions you are able and willing to work. Requests are <strong>not</strong> in order of preference.  Please only submit <strong>one</strong> request for each position: You can leave request fields blank if there are no other positions you wish to work.</p></div></div></div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									{{Form::select('position_id[]', $available_positions, !empty($userreq[0]) ? $userreq[0]->position_id : null, ['class'=>'form-control'])}}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									{{Form::select('position_id[]', $available_positions, !empty($userreq[1]) ? $userreq[1]->position_id : null, ['class'=>'form-control'])}}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									{{Form::select('position_id[]', $available_positions, !empty($userreq[2]) ? $userreq[2]->position_id : null, ['class'=>'form-control'])}}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<button type="submit" class="btn btn-success">Submit</button>
								</div>
							</div>
						</div>
						{{ Form::close() }}
						@endif
					@endif
					@if(Auth::check())
						@if(Auth::user()->can('events'))
						<h3 class="font-w300">Position Requests</h3>
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr>
											<th width="30%" style="text-align: center;">Position</th>
											<th width="60%" style="text-align: center;">Controller</th>
											<th width="10%" style="text-align: center;">Actions</th>
										</tr>
									</thead>
									<tbody>
										@forelse($pos_req as $p)
										<tr>
											<td style="text-align: center;">{{$p->eventPosition->name}}</td>
											<td style="text-align: center;">{{$p->user->full_name}}</td>
											<td style="text-align: center;"> {{ Form::open(['action' => ['EventController@assignPosition', $p->position_id, $p->user->id], 'method' => 'POST', 'style' => 'display: inline-block;']) }}
											<button class="btn btn-success btn-sm btn-addon simple-tooltip" title="Accept Position"><i class="fa fa-check"></i></button>{{ Form::close() }} {{ Form::open(['action' => ['EventController@deleteRequest', $p->id], 'method' => 'DELETE', 'style' => 'display: inline-block;']) }}
											<button class="btn btn-danger btn-sm btn-addon simple-tooltip" title="Delete Request"><i class="fa fa-times"></i></button>{{ Form::close() }}
											</td>
										</tr>
										@empty
										<tr>
											<td colspan="3" style="text-align: center;">No open position requests.</td>
										</tr>
										@endforelse
									</tbody>
								</table>
							</div>
						@endif
					@endif
				</div>
			</div>
		</div>
	</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- END Page Content -->
	
@else
<!-- Page Content -->
<div class="content content-full">
	<div class="hero-inner">
		<div class="content content-full">
			<div class="py-30 text-center">
				<div class="display-3 text-danger">
					<i class="fa fa-warning"></i> Event Error
				</div>
				<h1 class="h2 font-w700 mt-30 mb-10">You just found an error page!</h1>
				<h2 class="h3 font-w400 text-muted mb-50">We are sorry. The event you are looking for was not found.</h2>
				<a class="btn btn-hero btn-rounded btn-alt-secondary" href="/">
					<i class="fa fa-arrow-left mr-10"></i> Back to Homepage
				</a>
			</div>
		</div>
	</div>
</div>
<!-- END Page Content -->
@endif


@stop
