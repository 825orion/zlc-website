@extends('layouts.master')

@section('title')
@parent
| Staff Team
@stop

@section('content')

<?php function rolepos($pos){
    return function($role) use ($pos) {
        return $role->name == $pos;
    };
} ?>

<!-- Hero -->
<div class="bg-image bg-image-bottom" style="background-image: url('/assets_new/img/photos/staff_lp_bg.jpg');">
	<div class="bg-black-op-75">
		<div class="content content-center text-center">
			<div class="pt-50 pb-20">
				<h1 class="font-w700 text-white mb-10">Staff Team</h1>
				<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC</h2>
			</div>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<span class="breadcrumb-item active">Controllers</span>
			<span class="breadcrumb-item active">Staff Team</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<!-- Page Content -->
<div class="content content-full">
	<!-- Senior ARTCC Staff -->
	<h2 class="content-heading">Senior ARTCC Staff</h2>
	<div class="row gutters-tiny py-20">
		<div class="col-md-6 col-xl-4">
			<div class="block text-center">
				<div class="block-content block-content-full">
					<div class="font-size-h4 font-w600 mb-0">
						@forelse($Roles->filter(rolepos('ATM'))->first()->users as $atm)
                        	{{{$atm->full_name}}}
                    	@empty
							Vacant
						@endforelse
                        </div>
					<div class="font-size-h5 text-muted">Air Traffic Manager</div>
				</div>
				<div class="block-content block-content-full bg-body-light">
					<button type="button" class="btn btn-circle btn-secondary" data-toggle="popover" title="Job Description" data-placement="top" data-content="The Air Traffic Manager (ATM) is responsible to the VATUSA Southern Region Director for the  administration of the ARTCC. The ATM is responsible for appointing ARTCC staff members and delegation of authorities."><i class="fa fa-info"></i></button>
					@forelse($Roles->filter(rolepos('ATM'))->first()->users as $atm)
					@if(Auth::check())
						@if(Auth::user()->can('profile'))
						<a class="btn btn-circle btn-secondary" href="/profile/{{{$atm->id}}}">
							<i class="fa fa-id-card-o"></i>
						</a>
						@else
						@endif
					@else
					@endif
					<a class="btn btn-circle btn-secondary" href="mailto:atm@zjxartcc.org">
						<i class="fa fa-envelope"></i>
					</a>
					@empty
					@endforelse
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xl-4">
			<div class="block text-center">
				<div class="block-content block-content-full">
					<div class="font-size-h4 font-w600 mb-0">
						@forelse($Roles->filter(rolepos('DATM'))->first()->users as $datm)
                        	{{{$datm->full_name}}}
                    	@empty
							Vacant
						@endforelse
                        </div>
					<div class="font-size-h5 text-muted">Deputy Air Traffic Manager</div>
				</div>
				<div class="block-content block-content-full bg-body-light">
					<button type="button" class="btn btn-circle btn-secondary" data-toggle="popover" title="Job Description" data-placement="top" data-content="The Deputy Air Traffic Manager (DATM) reports to the Air Traffic Manager and acts as the Air Traffic Manager in their absence. The DATM is jointly responsible for administration of the ARTCC."><i class="fa fa-info"></i></button>
					@forelse($Roles->filter(rolepos('DATM'))->first()->users as $datm)
                    @if(Auth::check())
						@if(Auth::user()->can('profile'))
						<a class="btn btn-circle btn-secondary" href="/profile/{{{$datm->id}}}">
							<i class="fa fa-id-card-o"></i>
						</a>
						@else
						@endif
					@else
					@endif
					<a class="btn btn-circle btn-secondary" href="mailto:datm@zjxartcc.org">
						<i class="fa fa-envelope"></i>
					</a>
					@empty
					@endforelse
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xl-4">
			<div class="block text-center">
				<div class="block-content block-content-full">
					<div class="font-size-h4 font-w600 mb-0">
						@forelse($Roles->filter(rolepos('TA'))->first()->users as $ta)
                        	{{{$ta->full_name}}}
                    	@empty
							Vacant
						@endforelse
                        </div>
					<div class="font-size-h5 text-muted">Training Administrator</div>
				</div>
				<div class="block-content block-content-full bg-body-light">
					<button type="button" class="btn btn-circle btn-secondary" data-toggle="popover" title="Job Description" data-placement="top" data-content="The Training Administrator (TA) manages the ARTCC's training program, establishes training procedures, and recommends instructors and mentors."><i class="fa fa-info"></i></button>
					@forelse($Roles->filter(rolepos('TA'))->first()->users as $ta)
                   @if(Auth::check())
						@if(Auth::user()->can('profile'))
						<a class="btn btn-circle btn-secondary" href="/profile/{{{$ta->id}}}">
							<i class="fa fa-id-card-o"></i>
						</a>
						@else
						@endif
					@else
					@endif
					<a class="btn btn-circle btn-secondary" href="mailto:ta@zjxartcc.org">
						<i class="fa fa-envelope"></i>
					</a>
					@empty
					@endforelse
				</div>
			</div>
		</div>
		@if(count($Roles->filter(rolepos('ATA'))->first()->users))
		<div class="col-md-6 col-xl-4">
			<div class="block text-center">
				<div class="block-content block-content-full">
					<div class="font-size-h4 font-w600 mb-0">
						@forelse($Roles->filter(rolepos('ATA'))->first()->users as $ata)
                        	{{{$ata->full_name}}}
                    	@empty
							Vacant
						@endforelse
                        </div>
					<div class="font-size-h5 text-muted">Assistant Training Administrator</div>
				</div>
				<div class="block-content block-content-full bg-body-light">
					<button type="button" class="btn btn-circle btn-secondary" data-toggle="popover" title="Job Description" data-placement="top" data-content="The Assistant Training Administrator (ATA) is responsible to the Training Administrator. The ATA assists with management of the ARTCC's training program."><i class="fa fa-info"></i></button>
					@forelse($Roles->filter(rolepos('ATA'))->first()->users as $ata)
                    @if(Auth::check())
						@if(Auth::user()->can('profile'))
						<a class="btn btn-circle btn-secondary" href="/profile/{{{$ata->id}}}">
							<i class="fa fa-id-card-o"></i>
						</a>
						@else
						@endif
					@else
					@endif
					<a class="btn btn-circle btn-secondary" href="mailto:ata@zjxartcc.org">
						<i class="fa fa-envelope"></i>
					</a>
					@empty
					@endforelse
				</div>
			</div>
		</div>
		@endif
		<div class="col-md-6 col-xl-4">
			<div class="block text-center">
				<div class="block-content block-content-full">
					<div class="font-size-h4 font-w600 mb-0">
						@forelse($Roles->filter(rolepos('EC'))->first()->users as $ec)
                        	{{{$ec->full_name}}}
                    	@empty
							Vacant
						@endforelse
                        </div>
					<div class="font-size-h5 text-muted">Events Coordinator</div>
				</div>
				<div class="block-content block-content-full bg-body-light">
					<button type="button" class="btn btn-circle btn-secondary" data-toggle="popover" title="Job Description" data-placement="top" data-content="The Events Coordinator (EC) is responsible to the Deputy Air Traffic Manager. The EC is responsible for the coordination, planning, dissemination, and creation of events."><i class="fa fa-info"></i></button>
					@forelse($Roles->filter(rolepos('EC'))->first()->users as $ec)
                    @if(Auth::check())
						@if(Auth::user()->can('profile'))
						<a class="btn btn-circle btn-secondary" href="/profile/{{{$ec->id}}}">
							<i class="fa fa-id-card-o"></i>
						</a>
						@else
						@endif
					@else
					@endif
					<a class="btn btn-circle btn-secondary" href="mailto:ec@zjxartcc.org">
						<i class="fa fa-envelope"></i>
					</a>
					@empty
					@endforelse
				</div>
			</div>
		</div>
		@if(count($Roles->filter(rolepos('AEC'))->first()->users))
		<div class="col-md-6 col-xl-4">
			<div class="block text-center">
				<div class="block-content block-content-full">
					<div class="font-size-h4 font-w600 mb-0">
						@forelse($Roles->filter(rolepos('AEC'))->first()->users as $aec)
                        	{{{$aec->full_name}}}
                    	@empty
							Vacant
						@endforelse
                        </div>
					<div class="font-size-h5 text-muted">Assistant Events Coordinator</div>
				</div>
				<div class="block-content block-content-full bg-body-light">
					<button type="button" class="btn btn-circle btn-secondary" data-toggle="popover" title="Job Description" data-placement="top" data-content="The Assistant Events Coordinator (AEC) is responsible to the Events Coordinator. The AEC assists with the coordination, planning, dissemination, and creation of events."><i class="fa fa-info"></i></button>
					@forelse($Roles->filter(rolepos('AEC'))->first()->users as $aec)
                    @if(Auth::check())
						@if(Auth::user()->can('profile'))
						<a class="btn btn-circle btn-secondary" href="/profile/{{{$aec->id}}}">
							<i class="fa fa-id-card-o"></i>
						</a>
						@else
						@endif
					@else
					@endif
					<a class="btn btn-circle btn-secondary" href="mailto:aec@zjxartcc.org">
						<i class="fa fa-envelope"></i>
					</a>
					@empty
					@endforelse
				</div>
			</div>
		</div>
		@endif
		<div class="col-md-6 col-xl-4">
			<div class="block text-center">
				<div class="block-content block-content-full">
					<div class="font-size-h4 font-w600 mb-0">
						@forelse($Roles->filter(rolepos('FE'))->first()->users as $fe)
                        	{{{$fe->full_name}}}
                    	@empty
							Vacant
						@endforelse
                        </div>
					<div class="font-size-h5 text-muted">Facility Engineer</div>
				</div>
				<div class="block-content block-content-full bg-body-light">
					<button type="button" class="btn btn-circle btn-secondary" data-toggle="popover" title="Job Description" data-placement="top" data-content="The Facility Engineer (FE) is responsible to the Deputy Air Traffic Manager. The FE is responsible for the creation of sector files, radar client files, training scenarios, letters of agreement, operating procedures, and other requests as directed by senior staff."><i class="fa fa-info"></i></button>
					@forelse($Roles->filter(rolepos('FE'))->first()->users as $fe)
                    @if(Auth::check())
						@if(Auth::user()->can('profile'))
						<a class="btn btn-circle btn-secondary" href="/profile/{{{$fe->id}}}">
							<i class="fa fa-id-card-o"></i>
						</a>
						@else
						@endif
					@else
					@endif
					<a class="btn btn-circle btn-secondary" href="mailto:fe@zjxartcc.org">
						<i class="fa fa-envelope"></i>
					</a>
					@empty
					@endforelse
				</div>
			</div>
		</div>
		@if(count($Roles->filter(rolepos('AFE'))->first()->users))
		<div class="col-md-6 col-xl-4">
			<div class="block text-center">
				<div class="block-content block-content-full">
					<div class="font-size-h4 font-w600 mb-0">
						@forelse($Roles->filter(rolepos('AFE'))->first()->users as $afe)
                        	{{{$afe->full_name}}}
                    	@empty
							Vacant
						@endforelse
                        </div>
					<div class="font-size-h5 text-muted">Assistant Facility Engineer</div>
				</div>
				<div class="block-content block-content-full bg-body-light">
					<button type="button" class="btn btn-circle btn-secondary" data-toggle="popover" title="Job Description" data-placement="top" data-content="The Assistant Facility Engineer (AFE) is responsible to the Facility Engineer. The AFE assists with the creation of sector files, radar client files, training scenarios, letters of agreement, operating procedures, and other requests as directed by senior staff."><i class="fa fa-info"></i></button>
					@forelse($Roles->filter(rolepos('AFE'))->first()->users as $afe)
                    @if(Auth::check())
						@if(Auth::user()->can('profile'))
						<a class="btn btn-circle btn-secondary" href="/profile/{{{$afe->id}}}">
							<i class="fa fa-id-card-o"></i>
						</a>
						@else
						@endif
					@else
					@endif
					<a class="btn btn-circle btn-secondary" href="mailto:afe@zjxartcc.org">
						<i class="fa fa-envelope"></i>
					</a>
					@empty
					@endforelse
				</div>
			</div>
		</div>
		@endif
		<div class="col-md-6 col-xl-4">
			<div class="block text-center">
				<div class="block-content block-content-full">
					<div class="font-size-h4 font-w600 mb-0">
						@forelse($Roles->filter(rolepos('WM'))->first()->users as $wm)
                        	{{{$wm->full_name}}}
                    	@empty
							Vacant
						@endforelse
                        </div>
					<div class="font-size-h5 text-muted">Webmaster</div>
				</div>
				<div class="block-content block-content-full bg-body-light">
					<button type="button" class="btn btn-circle btn-secondary" data-toggle="popover" title="Job Description" data-placement="top" data-content="The Webmaster (WM) is responsible to the Air Traffic Manager for the operation and maintenance of all IT services including, but not limited to, the website, Discord, and email services."><i class="fa fa-info"></i></button>
					@forelse($Roles->filter(rolepos('WM'))->first()->users as $wm)
                    @if(Auth::check())
						@if(Auth::user()->can('profile'))
						<a class="btn btn-circle btn-secondary" href="/profile/{{{$wm->id}}}">
							<i class="fa fa-id-card-o"></i>
						</a>
						@else
						@endif
					@else
					@endif
					<a class="btn btn-circle btn-secondary" href="mailto:wm@zjxartcc.org">
						<i class="fa fa-envelope"></i>
					</a>
					@empty
					@endforelse
				</div>
			</div>
		</div>
		@if(count($Roles->filter(rolepos('AWM'))->first()->users))
		<div class="col-md-6 col-xl-4">
			<div class="block text-center">
				<div class="block-content block-content-full">
					<div class="font-size-h4 font-w600 mb-0">
						@forelse($Roles->filter(rolepos('AWM'))->first()->users as $awm)
                        	{{{$awm->full_name}}}
                    	@empty
							Vacant
						@endforelse
                        </div>
					<div class="font-size-h5 text-muted">Assistant Webmaster</div>
				</div>
				<div class="block-content block-content-full bg-body-light">
					<button type="button" class="btn btn-circle btn-secondary" data-toggle="popover" title="Job Description" data-placement="top" data-content="The Assistant Webmaster (AWM) is responsible to the Webmaster. The AWM assists with for the operation and maintenance of all IT services including, but not limited to, the website, Discord, and email services."><i class="fa fa-info"></i></button>
					@forelse($Roles->filter(rolepos('AWM'))->first()->users as $awm)
                    @if(Auth::check())
						@if(Auth::user()->can('profile'))
						<a class="btn btn-circle btn-secondary" href="/profile/{{{$awm->id}}}">
							<i class="fa fa-id-card-o"></i>
						</a>
						@else
						@endif
					@else
					@endif
					<a class="btn btn-circle btn-secondary" href="mailto:awm@zjxartcc.org">
						<i class="fa fa-envelope"></i>
					</a>
					@empty
					@endforelse
				</div>
			</div>
		</div>
		@endif
	</div>
	<!-- END Senior ARTCC Staff -->
	
	<!-- Instructors -->
	<h2 class="content-heading">Instructors</h2>
	<div class="row gutters-tiny pb-20">
	<p>Instructors report to the Training Administrator. Instructor's responsibilities include assisting the TA with developing the training policy, content, and syllabus; ensuring students perform to VATUSA and ZJX’s training standards; administering over the shoulder (OTS) and written exams to ensure controller competency; and endorsing controller ratings.</p>
	@forelse($Roles->filter(rolepos('INS'))->first()->users as $ins)
		<div class="col-md-6 col-xl-4">
			<div class="block text-center">
				<div class="block-content block-content-full">
					<div class="font-size-h4 font-w600 mb-0">{{{$ins->full_name}}}</div>
					<div class="font-size-h5 text-muted">Instructor</div>
				</div>
				<div class="block-content block-content-full bg-body-light">
					<!--<button type="button" class="btn btn-circle btn-secondary" data-toggle="popover" title="Job Description" data-placement="top" data-content="Instructors report to the Training Administrator. Instructor's responsibilities include assisting the TA with developing the training policy, content, and syllabus; ensuring students perform to VATUSA and ZJX’s training standards; administering over the shoulder (OTS) and written exams to ensure controller competency; and endorsing controller ratings."><i class="fa fa-info"></i></button>-->
                    @if(Auth::check())
						@if(Auth::user()->can('profile'))
						<a class="btn btn-circle btn-secondary" href="/profile/{{{$ins->id}}}">
							<i class="fa fa-id-card-o"></i>
						</a>
						@else
						@endif
					@else
					@endif
					<a class="btn btn-circle btn-secondary" href="mailto:training@zjxartcc.org">
						<i class="fa fa-envelope"></i>
					</a>
				</div>
			</div>
		</div>
	@empty
		<h4>There are no current ARTCC instructors.</h4>
	@endforelse
	</div>
	<!-- END Instructors -->
	
	<!-- Mentors -->
	<h2 class="content-heading">Mentors</h2>
	<div class="row gutters-tiny pb-20">
	<p>Mentors report to the Training Administrator. Mentor's responsibilities include ensuring students perform to VATUSA and ZJX’s training standards; conducting training sessions with controllers; and recommending controllers for OTS exams.</p>
	@forelse($Roles->filter(rolepos('MTR'))->first()->users as $mtr)
		<div class="col-md-6 col-xl-4">
			<div class="block text-center">
				<div class="block-content block-content-full">
					<div class="font-size-h4 font-w600 mb-0">{{{$mtr->full_name}}}</div>
					<div class="font-size-h5 text-muted">Mentor</div>
				</div>
				<div class="block-content block-content-full bg-body-light">
					<!--<button type="button" class="btn btn-circle btn-secondary" data-toggle="popover" title="Job Description" data-placement="top" data-content="Mentors report to the Training Administrator. Mentor's responsibilities include ensuring students perform to VATUSA and ZJX’s training standards; conducting training sessions with controllers; and recommending controllers for OTS exams."><i class="fa fa-info"></i></button>-->
                    @if(Auth::check())
						@if(Auth::user()->can('profile'))
						<a class="btn btn-circle btn-secondary" href="/profile/{{{$mtr->id}}}">
							<i class="fa fa-id-card-o"></i>
						</a>
						@else
						@endif
					@else
					@endif
					<a class="btn btn-circle btn-secondary" href="mailto:training@zjxartcc.org">
						<i class="fa fa-envelope"></i>
					</a>
				</div>
			</div>
		</div>
	@empty
		<h4>There are no current ARTCC mentors.</h4>
	@endforelse
	</div>
	<!-- END Instructors -->


</div>

@stop
