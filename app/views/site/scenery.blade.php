@extends('layouts.master')

@section('title')
@parent
| Scenery
@stop

@section('content')


<!-- Hero -->
<div class="bg-image bg-image-bottom" style="background-image: url('/assets_new/img/photos/scenery_lp_bg.jpg');">
	<div class="bg-black-op-75">
		<div class="content content-center text-center">
			<div class="pt-50 pb-20">
				<h1 class="font-w700 text-white mb-10">Scenery</h1>
				<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC</h2>
			</div>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<span class="breadcrumb-item active">Pilots</span>
			<span class="breadcrumb-item active">Scenery</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->


<!-- Main Container -->
                <!-- Page Content -->
                <div class="content">
                    <!-- Scenery Tabs -->
                    <div class="row">
                        <div class="col-lg-12">
							<div class="block">
                                <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#fsx">FSX/P3D</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#xplane">X-Plane</a>
                                    </li>
                                </ul>
                                <div class="block-content tab-content">
                                    <div class="tab-pane active" id="fsx" role="tabpanel">
                                        <h4 class="font-w400">FSX/P3D Scenery</h4>
                                        <div class="table-responsive">
										<table class="table table-striped table-vcenter">
											<thead>
												<tr>
													<th>Scenery</th>
													<th>Description</th>
													<th class="text-center" style="width: 100px;">Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($fsx as $f)
												<tr>
													<td>{{{$f->name}}}</td>
													@if($f->description == '0')
													<td>No description available.</td>
													@else
													<td>{{$f->description}}</td>
													@endif
													<td class="text-center">
														<div class="btn-group">
															<?php if (strpos($f->description, 'Payware') !== false){ ?>
															<a href="{{$f->link}}" target="_blank" class="btn btn-secondary mr-5 mb-5" data-toggle="tooltip" data-placement="top" title="Product Page">
																<i class="fa fa-shopping-cart"></i> Product Page
															</button></a>
															<?php }else{ ?>
															<a href="{{$f->link}}" target="_blank" class="btn btn-secondary mr-5 mb-5" data-toggle="tooltip" data-placement="top" title="Download">
																	<i class="fa fa-download"></i> Download
																</button></a>
															<?php } ?>
														</div>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table
									</div>
                                    </div>
                                    </div>
                                    <div class="tab-pane" id="xplane" role="tabpanel">
                                        <h4 class="font-w400">X-Plane Scenery</h4>
                                       <div class="table-responsive">
										<table class="table table-striped table-vcenter">
											<thead>
												<tr>
													<th>Scenery</th>
													<th>Description</th>
													<th class="text-center" style="width: 100px;">Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($xpl as $x)
												<tr>
													<td>{{{$x->name}}}</td>
													@if($x->description == '0')
													<td>No description available.</td>
													@else
													<td>{{$x->description}}</td>
													@endif
													<td class="text-center">
														<div class="btn-group">
															<?php if (strpos($x->description, 'Payware') !== false){ ?>
															<a href="{{$x->link}}" target="_blank" class="btn btn-secondary mr-5 mb-5" data-toggle="tooltip" data-placement="top" title="Product Page">
																<i class="fa fa-shopping-cart"></i> Product Page
															</button></a>
															<?php }else{ ?>
															<a href="{{$x->link}}" target="_blank" class="btn btn-secondary mr-5 mb-5" data-toggle="tooltip" data-placement="top" title="Download">
																<i class="fa fa-download"></i> Download
															</button></a>
															<?php } ?>
														</div>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
										</div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                       
                    </div>
                    <!-- END Scenery Tabs -->
	</div>
		
<!-- END Page Content -->



@stop