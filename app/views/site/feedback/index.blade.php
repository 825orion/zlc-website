@extends('layouts.master')

@section('title')
@parent
| Feedback
@stop

@section('content')

<!-- Hero -->
<div class="bg-image bg-image-bottom" style="background-image: url('/assets_new/img/photos/feedback_lp_bg.png');">
	<div class="bg-primary-dark-op py-30">
		<div class="content content-full text-center">

			<!-- Header -->
			<div class="content content-center text-center">
				<div class="pt-50 pb-20">
					<h1 class="font-w700 text-white mb-10">Feedback</h1>
					<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC</h2>
				</div>
			</div>
			<!-- END Header -->

			<!-- Actions -->
			<a href="/feedback/create" type="button" class="btn btn-rounded btn-hero btn-sm btn-success mb-5">
				<i class="fa fa-pencil mr-5"></i> Provide Feedback
			</a>
			<!-- END Actions -->
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<span class="breadcrumb-item active">Feedback</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->


<div class="content content-full">
	<!-- Historical Feedback -->
	<div class="block">
		<div class="block-header block-header-default">
			<h3 class="block-title"><i class="fa fa-list mr-10"></i> Historical Feedback</h3>
		</div>
		<div class="block-content block-content-full">
			<div class="table-responsive">
			<table class="table table-bordered table-striped table-vcenter js-dataTable-full-feedback">
				<thead>
					<tr>
						<th class="text-center">Date Submitted</th>
						<th class="text-center">Controller</th>
						<th class="text-center">Controller VATSIM ID</th>
						<th class="text-center">Position</th>
						<th class="text-center">Service Rating</th>
						<th class="text-center"></th>
					</tr>
				</thead>
				<tbody>
					@forelse($feedback as $h)
							@if($h->status==1)
							<tr>
								<td class="text-center">{{{$h->created_at}}}z</td>
								<td class="text-center">{{{$h->controller->backwards_name}}}</td>
								<td class="text-center">{{{$h->controller->id}}}</td>
								<td class="text-center">{{{$h->position}}}</td>
								<td class="text-center">{{{$h->level_text}}}</td>
								<td class="text-center">
									<div class="btn-group">
										<a href="/feedback/{{$h->id}}" type="button" class="btn btn-sm btn-secondary" >
											<i class="fa fa-comments"></i>
										</a>
									</div>
								</td>
							</tr>
							@endif
							@empty
							@endforelse
				</tbody>
			</table>
			</div>
		</div>
	</div>
	<!-- END Historical Feedback -->
</div>

@stop
