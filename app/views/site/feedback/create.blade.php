@extends('layouts.master')

@section('title')
@parent
| Provide Feedback
@stop

@section('content')
<!-- Hero -->
<div class="bg-image bg-image-bottom" style="background-image: url('/assets_new/img/photos/feedback_lp_bg.png');">
	<div class="bg-primary-dark-op py-30">
		<div class="content content-full text-center">

			<!-- Header -->
			<div class="content content-center text-center">
				<div class="pt-50 pb-20">
					<h1 class="font-w700 text-white mb-10">Provide Feedback</h1>
					<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC</h2>
				</div>
				
			</div>
			<!-- END Header -->
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/feedback">Feedback</a>
			<span class="breadcrumb-item active">Provide Feedback</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

	<div class="content content-full">
		<div class="row">
			<div class="col-md-12">
				<div class="block block-themed">
					<div class="block-header">
						<h3 class="block-title">Provide Feedback for a Jacksonville Controller </h3>
						
					</div>
					<div class="block-content">
						{{Form::open(['action'=>'FeedbackController@store'])}}
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										{{Form::label('controller_id', 'Controller:', ['class'=>'control-label'])}}
										{{Form::select('controller_id', $members, null, ['class'=>'form-control'])}}
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										{{Form::label('position', 'Position:', ['class'=>'control-label'])}}
										{{Form::select('position', Feedback::$Positions, 'JAX_CTR', ['class'=>'form-control'])}}
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										{{Form::label('level', 'Service Level', ['class'=>'control-label'])}}
										{{Form::select('level', array(
												'4' => 'Excellent', 
												'3' => 'Good', 
												'2' => 'Fair', 
												'1' => 'Poor', 
												'0' => 'Unsatisfactory'), null, ['class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										{{Form::label('pilot_id', 'VATSIM CID:', ['class'=>'control-label'])}}
										{{Form::text('pilot_id', null, ['class'=>'form-control'])}}
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										{{Form::label('flight_callsign', 'Flight Callsign:', ['class'=>'control-label'])}}
										{{Form::text('flight_callsign', null, ['class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										{{Form::label('pilot_name', 'Pilot Name:', ['class'=>'control-label'])}}
										{{Form::text('pilot_name', null, ['class'=>'form-control'])}}
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										{{Form::label('pilot_email', 'Pilot Email:', ['class'=>'control-label'])}}
										{{Form::text('pilot_email', null, ['class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										{{Form::label('comments', 'Comments:', ['class'=>'control-label'])}}
										{{Form::textarea('comments', null, ['class'=>'form-control'])}}
									</div> 
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<button type="submit" class="btn btn-success">Submit</button>
									</div>
								</div>
							</div>
							{{Form::close()}}
					</div>
				</div>
			</div>
		</div>
	</div>

@stop