@extends('layouts.master')

@section('title')
@parent
| View Feedback
@stop

@section('content')

<!-- Hero -->
<div class="bg-image bg-image-bottom" style="background-image: url('/assets_new/img/photos/feedback_lp_bg.png');">
	<div class="bg-primary-dark-op py-30">
		<div class="content content-full text-center">

			<!-- Header -->
			<div class="content content-center text-center">
				<div class="pt-50 pb-20">
					<h1 class="font-w700 text-white mb-10">View Feedback</h1>
					<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC</h2>
				</div>
				
			</div>
			<!-- END Header -->
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/feedback">Feedback</a>
			<span class="breadcrumb-item active">View Feedback</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

@if($f)
	<div class="content content-full">
		<div class="row">
			<div class="col-md-12">
				<div class="block block-themed">
					<div class="block-header">
						<h3 class="block-title">Pilot Feedback for {{{$f->controller->full_name}}} (ID: {{$f->id}}) </h3>
						
					</div>
					<div class="block-content">
						@if(Auth::check())
							@if(Auth::user()->can('snrstaff') || Auth::user()->can('instruct'))
							<p><b>Submitter:</b> {{$f->pilot_name}} ({{$f->pilot_id}}) flying as {{$f->flight_callsign}}</p>
							@endif
						@endif
						<p><b>Date:</b> {{$f->created_at}}z</p>
						<p><b>Service Rating:</b> {{$f->level_text}}</p>
					 	<p><b>Controller's Name:</b> {{{$f->controller->full_name}}}</p>
					 	<p><b>Controller's VATSIM ID:</b> {{{$f->controller->id}}}</p>
					 	<p><b>Controller's Position:</b> {{$f->position}}</p>
						<p><b>Feedback:</b> {{$f->comments}}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
@else
	<!-- Page Content -->
	<div class="content content-full">
		<div class="hero-inner">
			<div class="content content-full">
				<div class="py-30 text-center">
					<div class="display-3 text-danger">
						<i class="fa fa-warning"></i> Feedback Error
					</div>
					<h1 class="h2 font-w700 mt-30 mb-10">You just found an error page!</h1>
					<h2 class="h3 font-w400 text-muted mb-50">We are sorry. The feedback you are looking for was not found.</h2>
					<a class="btn btn-hero btn-rounded btn-alt-secondary" href="/">
						<i class="fa fa-arrow-left mr-10"></i> Back to Homepage
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- END Page Content -->
@endif

@stop