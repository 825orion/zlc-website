<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="utf-8">
</head>
<body>
<p>Dear {{$user->first_name}},</p>
<p>I am pleased to welcome you to Jacksonville ARTCC! My name is Petey Shivery and I am Jacksonville&rsquo;s Air Traffic Manager. Jacksonville is home to a diverse array of airports, ranging from Orlando, to Daytona Beach, and even Charleston. We also have over 60 controllers who call  Jacksonville ARTCC home. We are excited to welcome you to the community!</p>
<p>As a new  member, there are a few things you need to take care of to get started:</p>
<ol>
  <li><strong>Login to our website</strong>. You can do this at zjxartcc.org. Click the login button on the top right corner of the website and use your VATSIM login. This will redirect you to your controller profile. You&rsquo;ll be able to access your controller hours, the training center, and additional controller resources here. <br>
    <br>
  </li>
  <li><strong>Download Discord</strong>. Discord is the official method of communication for our community. Discord has a mobile app, text capabilities, and  voice communication. You&rsquo;ll be able to become part of the community by being involved in our Discord. You can access our Discord server at: <a href="https://vats.im/zjxdiscord">https://vats.im/zjxdiscord</a> <br>
    <br>
  </li>
  <li><strong>Review the ARTCC&rsquo;s documentation.</strong> Now that you have the basics down, <a href="https://zjxartcc.org/documents">please review our documentation</a>. If you&rsquo;re new  to VATSIM, don&rsquo;t worry, in your first training session we&rsquo;ll go over the basics.  Essentially, you must complete three (3) hours controlling or training every quarter to remain an active home  controller. If you&rsquo;re a visiting controller, you have no hourly requirements! Enjoy controlling Jacksonville at your leisure. <br><br>
  </li>
  <li><strong>Request training.</strong>   You can do this by going to our <a href="https://zjxartcc.setmore.com/">training booking website</a> and selecting the position you'd like to train on.<br><br></li>
	  <li><strong>Take our on-boarding course.</strong> All controllers are required to take our on-boarding course at <a href="https://vatusa.net">VATUSA</a>. Once on VATUSA's website, navigate to Division Info, then click Computer Based Training (CBT). Afterwards, select "ZJX" from the drop-down menu and read through all classes under "On-Boarding." This will prepare you to control at Jacksonville.</li>

</ol>
<p>That&rsquo;s it for now! If you have any questions, please feel free to email me personally at <a href="mailto:atm@zjxartcc.org">atm@zjxartcc.org</a>. I can&rsquo;t wait to see you on the scopes! </p>
<p>All the  best,</p>
<p><strong>Petey Shivery</strong><br>
  Air Traffic Manager<br>
  Jacksonville ARTCC </p>
</body>
</html>