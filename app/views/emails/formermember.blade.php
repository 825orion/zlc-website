<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="utf-8">
</head>
<body>
Member,<br />
<br />
You have been removed from the Jacksonville ARTCC roster. <br />
<br />
If you wish to find out why, or to appeal the decision, please email the ATM/DATM found at <a href="mailto:staff@zjxartcc.org">staff@zjxartcc.org</a>.<br />
<br />
Regards,<br />
<br />
Petey Shivery<br />
Jacksonville ARTCC Air Traffic Manager<br />
</body>
</html>
