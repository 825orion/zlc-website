<p>Thank you for submitting feedback to the ZJX ARTCC!</p>
<p>Your feedback has been reviewed and accepted. Please find your feedback below alongside any comments made by our staff.</p>

<h2>Feedback:</h2>

<ul>
	<li>Controller: <b>{{ $feedback->controller->full_name }}</b></li>
	<li>Position: <b>{{ $feedback->position }}</b></li>
	<li>Rating: <b>{{ $feedback->level_text }}</b></li>
	<li>Pilot Comments: <b>{{ $feedback->comments }}</b></li>
	<li>Staff Comments: <b>{{ $feedback->staff_comments }}</b></li>
</ul>

<p>Regards,</p>	
<p>Jacksonville ARTCC</p>