<h2>You have been assigned a position for an event!</h2>

<h2>Assignment Details:</h2>

<ul>
	<li>Event: <b>{{ $position->event->title }}</b></li>
	<li>Start Date: <b>{{ $position->event->event_start }}</b></li>
	<li>End Date: <b>{{ $position->event->event_end }}</b></li>
	<li>Position Assigned: <b>{{ $position->name }}</b></li>
	<li>Controller Assigned to Position: <b>{{ $position->user->first_name }} {{ $position->user->last_name }}</b></li>
</ul>

<p>Please reach out to the Events Coordinator with any additional questions.</p>