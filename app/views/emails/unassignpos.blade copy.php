<h2>You have been removed from an event position!</h2>

<h2>Details:</h2>

<ul>
	<li>Event: <b>{{ $position->event->title }}</b></li>
	<li>Start Date: <b>{{ $position->event->event_start }}</b></li>
	<li>End Date: <b>{{ $position->event->event_end }}</b></li>
	<li>Position Unassigned: <b>{{ $position->name }}</b></li>
	<li>Controller Unassigned from Position: <b>{{ $position->user->first_name }} {{ $position->user->last_name }}</b></li>
</ul>

<p>Please reach out to the Events Coordinator with any additional questions.</p>