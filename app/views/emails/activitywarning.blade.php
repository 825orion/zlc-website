<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="utf-8">
</head>
<body>
Dear {{$user->first_name}},<br />
<br />
We hope you are well!  This is an automated email to notify you that you have not met your quarterly activity requirement.  In order to remain an active Jacksonville home controller, you must complete a combined three (3) hours of controlling or training every quarter.  If you do not, you are subject to removal. <br /><br/>
We would love to keep you at Jacksonville!  If you would like to remain a Jacksonville controller, please <a href="mailto:staff@zjxartcc.org">email us</a> to request an LOA or complete one (1) hour of controlling or training in the next week.  Unfortunately, if we do not see any action within the next week, you will be removed from the roster for inactivity. <br /><br/>
Please <a href="mailto:staff@zjxartcc.org">let us know</a> if you are experiencing any trouble completing the activity requirement.  We want to ensure you remain part of the community if you want to be here! <br />
<br />
Regards,<br />
<br />
Jacksonville ARTCC<br />
</body>
</html>