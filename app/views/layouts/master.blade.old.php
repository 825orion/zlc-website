<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">

	<title>
		@section('title')
			vZJX ARTCC
		@show
	</title>

	<meta name="description" content="The official website of Jacksonville ARTCC of VATSIM.">
	<meta name="keywords" content="VATSIM,VATUSA,ZJX,Jacksonville,Orlando,Jacksonville ARTCC,Virtual ATC">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-123789597-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-123789597-1');
	</script>

	<link rel="icon" type="image/png" href="/assets/images/icons/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="/assets/images/icons/favicon-16x16.png" sizes="16x16" />

	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/fontawesome.min.css">
	<link rel="stylesheet" href="/assets/css/magnific-popup.min.css">
	<link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="/assets/css/revolutionslider.min.css">
	<link rel="stylesheet" href="/assets/css/theme.css">
	<link rel="stylesheet" href="/assets/css/tipped.css">
	<link rel="stylesheet" href="/assets/css/main.css">
	<link rel="stylesheet" href="/assets/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/r/bs/dt-1.10.9,r-1.0.7/datatables.min.css">
	<style type="text/css"></style>

	<script src="/assets/js/jquery.min.js"></script>
	<script src="/assets/js/jquery-ui.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			Tipped.create('.simple-tooltip');
		});

		$(window).load(function(){
			$('#flash-modal').modal('show');
		});
	</script>

</head>
<body>

	<div class="outer">
		<div class="header-2">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<a href="https://zjxartcc.org"><img style="max-width: 55px; margin-top:10px; margin-bottom:10px;" src="/assets/images/logo.png" /></a>
						<!--<img style="max-width: 210px;" src="/assets/images/logo.png" />-->
					</div>
					<div class="col-md-8">
						<div class="navy">
							<ul>
								<li><a href="/">Home</a></li>
								<li><a>Pilots</a>
									<ul>
										<li><a href="http://www.airnav.com/" target="_blank">Charts</a></li>
										<li><a href="/weather">Weather</a></li>
										<li><a href="/scenery">Scenery</a></li>
										<!--<li><a href="/airports">Airports</a></li>-->
										<li><a href="/runways">Suggested Runways</a></li>
										<li><a href="http://www.flightaware.com/statistics/ifr-route/" target="_blank">IFR Routes</a></li>
									</ul>
								</li>
								<li><a>Controllers</a>
									<ul>
										<li><a href="/staff">Staff</a></li>
										<li><a href="/roster">Roster</a></li>
										{{-- <li><a href="/feedback">Feedback Archive</a></li> --}}
										<li><a href="/documents">Documents</a></li>
										<!--<li><a href="/comms">Communications</a></li>-->
										<li><a href="/stats">Controller Stats</a></li>
									</ul>
								</li>
								<li><a href="/feedback">Feedback</a></li>
								<!--<li><a href="https://forum.zjxartcc.org">Forum</a></li>-->
								@if(Auth::check())
								<li><a>{{Auth::user()->full_name}} - ({{Auth::user()->rating_short}})</a>
									<ul>
										<li><a href="/profile">My Profile</a></li>
										<li><a>Training</a>
											<ul>
												@if(Auth::user()->canTrain == 1)
												<li><a href="https://zjxartcc.setmore.com/" target="_blank">Mentor Availability</a></li>
												@endif
												<li><a href="/training/notes">History</a></li>
												<li><a href="/atcast">ATCast Videos</a></li>
												<!--<li><a href="/trainingcenter">Training Center</a></li>-->
											</ul>
										</li>
										<li role="separator" class="divider-1"></li>
										@if(Auth::user()->can('roster_edit'))
										<li><a>Roster</a>
											<ul>
												<li><a href="/admin/roster">View Roster</a></li>
												@if(Auth::user()->can('roster_add'))
												<li><a href="/admin/roster/create">Add Controller</a></li>
												@endif
												@if(Auth::user()->can('visit'))
												<li><a href="/admin/visitreq">Visiting Requests</a></li>
												@endif
												@if(Auth::user()->hasRole('ATM') || Auth::user()->hasRole('DATM'))
												<li><a href="/admin/rostertidy">Roster Cleanup</a></li>
												@endif
											</ul>
										</li>
										@endif
										@if(Auth::user()->can('docs'))
										<li><a href="/admin/docs">Documents</a></li>
										@endif
										@if(Auth::user()->can('events'))
										<li><a href="/admin/events">Events</a></li>
										@endif
										<!--<li><a>Communications</a>
											<ul>
												<li><a href="/admin/comms">Management</a></li>
												<li><a href="/admin/comms/atis">ATIS Management</a></li>
											</ul>
										</li>-->
										@if(Auth::user()->can('scenery'))
										<li><a href="/admin/scenery">Scenery</a></li>
										@endif
										<li role="separator" class="divider-1"></li>
										@if(Auth::user()->can('mentor'))
										<li><a>Instructing</a>
											<ul>
													<li><a href="/admin/mentor/files">Training Files</a></li>
													<li><a href="/admin/mentor/checklists">Training Checklists</a></li>
												<li role="separator" class="divider-1"></li>
													<!--<li><a href="/admin/mentor/avail">Manage Your Availability</a></li>
													<li><a href="/admin/mentor/requests">View Training Sessions</a></li>-->
												<li role="separator" class="divider-1"></li>
													<li><a href="/admin/mentor/students">Find Student Notes</a></li>
													<li><a href="/admin/mentor/addnote">Add Training Note</a></li>
													
												<!--<li role="separator" class="divider-1"></li>
													<li><a href="/admin/mentor/otsrec">Recommend OTS</a></li>

												@if(Auth::user()->can('instruct'))
													<li role="separator" class="divider-1"></li>
													<li><a href="/admin/instructor/otsrec">OTS Center</a></li>
												@endif -->
											</ul>
										</li>
										@endif
										@if(Auth::user()->can('snrstaff') || Auth::user()->can('scenery') || Auth::user()->can('staff'))
										<li><a>Admin</a>
											<ul>
												@if(Auth::user()->can('staff'))
												<!--<li><a href="/admin/broadcast">Broadcast Email</a></li>-->
												<li><a href="/admin/activitylog">Site Activity</a></li>
												@endif
												@if(Auth::user()->can('snrstaff'))
												<!--<li><a href="/admin/mentorhist">Mentor History</a></li>-->
												<li><a href="/admin/feedback">Pending Feedback</a></li>
												@endif
												<!--@if(Auth::user()->can('scenery'))
												<li><a href="/admin/announcements">Announcements</a></li>
												@endif-->
												@if(Auth::user()->can('staff'))
        										<li><a href="/webmail" target="_blank">Staff Email</a></li>
        										@endif
											</ul>
										</li>
										<!--@if(Auth::user()->hasRole('ATM') || Auth::user()->hasRole('DATM') || Auth::user()->hasRole('TA') || Auth::user()->hasRole('EC') || Auth::user()->hasRole('FE') || Auth::user()->hasRole('WM'))
										<li><a href="/admin/email-password">Email Password</a></li>
										@endif-->
										@endif
										<li role="separator" class="divider-1"></li>
										<li><a href="/logout">Logout</a></li>
									</ul>
								</li>
								@else
								<li><a href="/visit">Visiting Application</a></li>
								<li><a href="/login">Login</a></li>
								@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="main-content">
	@yield('content')
	</div>

	<!-- Site Footer -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
    					<p class="pull-left">Copyright &copy; <?php echo date("Y"); ?> by the Virtual Jacksonville ARTCC ARTCC.<br /><a href="http://vatstar.com" target="_blank"><img src="/assets/images/vatstar.png" style="max-height: 50px"></a> &nbsp;&nbsp; <a href="https://www.flyjetbluevirtual.org" target="_blank"><img src="/assets/images/JetblueVirtual.png" style="max-height: 50px"></a></p>
					<ul class="list-inline pull-right">
						<li>For entertainment purposes only.  Do not use for real world purposes.  </li><li></li>Part of the <a href="http://www.vatusa.net">VATUSA</a> division on the <a href="http://www.vatsim.net">VATSIM</a> Network.</li>
						<!--<a href="https://seal.beyondsecurity.com/vulnerability-scanner-verification/zjxartcc.org"><img src="https://seal.beyondsecurity.com/verification-images/zjxartcc.org/vulnerability-scanner-2.gif" alt="Website Security Test" border="0" /></a>-->
					</ul>
				</div>
			</div>
		</div>
	</footer>

	@if(Session::has('message'))
	<div class="modal fade" id="flash-modal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<p><strong>{{{Session::get('message')}}}</strong></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	@endif

	@if(count($errors) > 0)
	<div class="modal fade" id="flash-modal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					@foreach($errors->all() as $error)
					<p><strong>{{ $error }}</strong></p>
					@endforeach
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	@endif

	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/tipped.js"></script>
	<script src="/assets/js/home.js"></script>
	<script src="https://cdn.datatables.net/r/bs/dt-1.10.9,r-1.0.7/datatables.min.js"></script>
	<script src="/assets/js/jquery.countTo.js"></script>
	<script src="/assets/js/jquery.magnific-popup.min.js"></script>
	<script src="/assets/js/jquery.tablesorter.js"></script>
	<script src="/assets/js/jquery.themepunch.revolutionslider.min.js"></script>
	<script src="/assets/js/jquery.themepunch.tools.min.js"></script>
	<script src="/assets/js/owl.carousel.min.js"></script>
	<script src="/assets/js/placeholders.min.js"></script>
	<script src="/assets/js/plupload.js"></script>
	<script src="/assets/js/site.js"></script>
	<script src="/assets/js/waypoints.min.js"></script>
	<script src="/assets/js/moment.min.js"></script>
	<script src="/assets/js/bootstrap-datetimepicker.min.js"></script>
</body>
</html>