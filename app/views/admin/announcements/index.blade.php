@extends('layouts.master')

@section('title')
@parent
| Edit Site Announcement | Administrator Center
@stop

@section('content')

<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				Edit Site Announcement
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<span class="breadcrumb-item active">Edit Site Announcement</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<div class="content content-full">
	<div class="container">
		<div class="row">
			{{ Form::open(['action' => ['AdminController@updateAnnouncements', $a->id]]) }}
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
						{{Form::label('message', 'Message:', ['class'=>'control-label'])}}
						{{Form::hidden('message', $a->message)}}
						@include('partials.quill', ['id' => 'message'])
					</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							{{Form::label('class', 'Annoucement Type:', ['class'=>'control-label'])}}
							{{Form::select('class', Announcement::$type, $a->class, ['class' => 'form-control'])}}
						</div>
						<div class="checkbox">
							<label>
								{{Form::checkbox('active', '1', $a->active)}}
								Is Active?
							</label>
						</div>
						<div class="form-group">
							{{Form::submit('Save', ['class' => 'btn btn-primary'])}}
						</div>
					</div>
				</div>
			{{ Form::close() }}
		</div>
	</div>
</div>

@stop
