@extends('layouts.master')

@section('title')
@parent
| View Training Note
@stop

@section('content')

<!-- Hero -->
<div class="bg-gd-sea">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				View Training Note
			</h1>
			@if(Auth::user()->canTrain == 1)
			<br />
			<a class="btn btn-hero btn-noborder btn-rounded btn-alt-primary js-appear-enabled animated fadeInUp" data-toggle="appear" data-class="animated fadeInUp" data-timeout="300" href="https://zjxartcc.setmore.com/"><i class="fa fa-calendar-plus-o mr-5"></i> Book a Training Session</a>
			@endif
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/profile">Profile</a>
			<a class="breadcrumb-item" href="/training/notes">My Training Center</a>
			<span class="breadcrumb-item active">View Training Note</span>
		</nav>
	</div>
</div>
<!-- END Breadcrumb -->

@if($note)
	@if($note->controller_id == Auth::id() || Auth::user()->can('snrstaff'))
		<div class="content content-full">
			<div class="row">
				<div class="col-md-12">
					<div class="block block-themed">
						<div class="block-header">
							<h3 class="block-title">Training Note for {{$note->controller->full_name}} (ID: {{$note->id}})</h3>
						</div>
						<div class="block-content">
							<p><b>Session Date:</b> {{$note->date}} at {{$note->session_begin}}z</p>
							<p><b>Session Type:</b> {{$note->ses_type}}</p>
							<p><b>Position:</b> {{$note->ses_pos}}</p>
							<p><b>Training Staff:</b> {{$note->instructor->full_name}}</p>
	                        <p><b>Duration:</b> {{$note->duration}} minutes</p>
	                        <p><b>Comments:</b></p>
	                        <p>{{$note->comments}}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	@else
	    <!-- Page Content -->
		<div class="content content-full">
			<div class="row">
				<div class="col-12">
					<div class="block block-themed block-rounded">
						<div class="block-header bg-danger">
							<h3 class="block-title"><center>Error Information</center></h3>
							<div class="block-options">
							</div>
						</div>
						<div class="block-content">
							<p align="center"><i class="fa fa-exclamation-circle fa-3x"></i><br><br>You are not authorized to view this training note.<center><a href="/" type="button" class="btn btn-hero btn-rounded btn-noborder btn-success mr-5 mb-5"><i class="si si-login mr-5"></i>Back to Homepage</a></center></p>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END Page Content -->
	@endif
@else
		<!-- Page Content -->
		<div class="content content-full">
			<div class="row">
				<div class="col-12">
					<div class="block block-themed block-rounded">
						<div class="block-header bg-danger">
							<h3 class="block-title"><center>Error Information</center></h3>
							<div class="block-options">
							</div>
						</div>
						<div class="block-content">
							<p align="center"><i class="fa fa-exclamation-circle fa-3x"></i><br><br>We could not find the requested training note.<center><a href="/" type="button" class="btn btn-hero btn-rounded btn-noborder btn-success mr-5 mb-5"><i class="si si-login mr-5"></i>Back to Homepage</a></center></p>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END Page Content -->
@endif

@stop