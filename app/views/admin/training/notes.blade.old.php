@extends('layouts.master')

@section('title')
@parent
| Training
@stop

@section('content')

<div class="page-heading-two mt-3">
    <div class="container">
        <h2>Training - My Training Notes</h2>
    </div>
</div>
    
<div class="container">
	<div class="row">	
		<div class="col-sm-3">
			<div class="table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <th width="40%">Delivery</th>
                        <th width="60%">{{{$user->del_cert}}}</th>
                    </tr>
                    <tr>
                        <th width="40%">Ground</th>
                        <th width="60%">{{{$user->gnd_cert}}}</th>
                    </tr>
                    <tr>
                        <th width="40%">Tower</th>
                        <th width="60%">{{{$user->twr_cert}}}</th>
                    </tr>
                    <tr>
                        <th width="40%">Approach</th>
                        <th width="60%">{{{$user->app_cert}}}</th>
                    </tr>
                    <tr>
                        <th width="40%">Center</th>
                        <th width="60%">{{{$user->ctr_cert}}}</th>
                    </tr>
				</table>
            </div>
		</div>
		<div class="col-sm-9">
			<ul class="nav nav-tabs nav-justified" >
				<li class="btn btn-primary col-md-6"><a data-toggle="tab" style="color:white;" href="#notes">Training Notes</a></li>
		  		<li class="btn btn-primary col-md-6"><a data-toggle="tab" style="color:white;" href="#exams">Exams</a></li>
	  		</ul>
	  		<div class="tab-content">
		    	<div id="notes" class="tab-pane fade-in active">
			        <div class="table-responsive">
	                <table class="table table-bordered">
		                <thead>    
		                    <tr>
		                        <th width="20%">Date</th>
		                        <th width="15%">Position</th>
		                        <th width="20%">Instructor</th>
		                        <th width="15%">Type</th>
		                        <th width="10%">Actions</th>
		                    </tr>
		                </thead>
		                <tbody>
		                	@forelse($notes as $n)
		                	<tr>
		                		<td>{{{$n->date}}} - {{{$n->session_begin}}}</td>
		                		<td>{{{$n->ses_pos}}}</td>
		                		<td>{{{$n->instructor->full_name}}}</td>
		                		<td>{{{$n->ses_type}}}</td>
		                		<td><a href="/training/note/{{{$n->id}}}" class="btn btn-success btn-sm simple-tooltip" title="View Details"><i class="fa fa-info-circle"></i></a></td>
		                	</tr>
		                	@empty
		                	<tr>
		                		<td colspan="6"><center>No Session Notes Available</center></td>
		                	</tr>
		                	@endforelse
		                </tbody>
					</table>
            		</div>
				</div>
				<div id="exams" class="tab-pane fade-in">
			        <div class="table-responsive">
	                <table class="table table-bordered">
		                <thead>    
		                    <tr>
		                        <th width="60%">Exam Name</th>
								<th width="20%">Date</th>
								<th width="10%">Score</th>
								<th width="10%">Passed?</th>
		                    </tr>
		                </thead>
		                <tbody>
		                	<?php 
		                	    if(count($exam)==0){
		                	        echo '<tr><td colspan="4"><center>No Exam History Available</center></td></tr>';
		                	    }else{
		                	        foreach($exam as $e){
		                	            if($e['exam_name']!=""){
        		                	        echo '<tr><td>'.$e['exam_name'].'</td>';
        		                	        echo '<td>'.date('M d, Y',strtotime(explode("T",$e['date'])[0])).'</td>';
        		                	        echo '<td>'.$e['score'].'%</td>';
        		                	        if($e['passed']=="1"){
        		                	            echo '<td style="background-color:#63f28c;">Yes</td>';
        		                	        }else{
        		                	            echo '<td style="background-color:#f26363;">No</td>';
        		                	        }
        		                	        echo '</tr>';
		                	            }
    		                	    }
		                	    }
		                	?>
		                </tbody>
					</table>
            		</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop
