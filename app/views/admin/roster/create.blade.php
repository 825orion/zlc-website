@extends('layouts.master')

@section('title')
@parent
| Add Controller | Administrator Center
@stop

@section('content')

<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				Add Controller
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<a class="breadcrumb-item" href="/admin/roster">Manage Roster</a>
			<span class="breadcrumb-item active">Add Controller</span>
		</nav>
	</div>
</div>
<!-- END Breadcrumb -->

<!-- Main Content -->
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="block block-rounded">
				<div class="block-content">
					{{ Form::open(['action' => 'RosterController@store']) }}
					<div class="form-group">
							{{Form::label('id', 'CID:', ['class'=>'control-label'])}}
						<div class="input-group">
						  {{Form::text('id', null, ['class'=>'form-control'])}}
						</div>
					</div>
					<div class="form-group">
						{{Form::label('first_name', 'First Name:', ['class'=>'control-label'])}}
						{{Form::text('first_name', null, ['class'=>'form-control'])}}
					</div>
					<div class="form-group">
						{{Form::label('last_name', 'Last Name:', ['class'=>'control-label'])}}
						{{Form::text('last_name', null, ['class'=>'form-control'])}}
					</div>
					<div class="form-group">
						{{Form::label('email', 'E-mail:', ['class'=>'control-label'])}}
						{{Form::text('email', null, ['class'=>'form-control'])}}
					</div>
					<div class="form-group">
						{{Form::label('rating_id', 'Rating:', ['class'=>'control-label'])}}
						{{Form::select('rating_id', User::$RatingShort, 1, ['class' => 'form-control'])}}
					  </div>

					<div class="checkbox visitor">
						<label>
							{{Form::checkbox('visitor', '1', false)}}
							Visitor
						</label>
					</div>
					<div class="form-group visitor-from">
						{{Form::label('visitor_from', 'Visiting From:', ['class'=>'control-label'])}}
						{{Form::text('visitor_from', null, ['class'=>'form-control'])}}
					</div>

					<div class="form-group">
						{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
					</div>

			   {{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END Main Content -->

<script>
	$(document).ready(function(){
		var toggleVisitorFrom = function(){
			var visitorChecked = $("input[name=visitor]").is(':checked');
			if (visitorChecked) {
				$(".visitor-from").toggle(true);
			} else {
				$(".visitor-from").toggle(false);
			}
		};    		

		$("input[name=visitor]").change(function(evt){
			toggleVisitorFrom();
		});

		toggleVisitorFrom();


	$(".search-cid").click(function(){
	  var cid = $("#id").val();
	  $("#id").parents(".form-group").removeClass('is-invalid');

	  $.get('/admin/roster/vatsim/' + cid, function(data){
		if (data.error) {
		  $("#id").parents(".form-group").addClass('is-invalid');
		  return;
		}

		$("#first_name").val(data.name_first);
		$("#last_name").val(data.name_last);
		$("#email").val(data.email);
		$("#rating_id").val(data.rating);
	  });
	});
	});
</script>

@stop