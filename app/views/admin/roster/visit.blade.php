@extends('layouts.master')

@section('title')
@parent
| Visiting Requests | Administrator Center
@stop

@section('content')

<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				Pending Visiting Requests
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<a class="breadcrumb-item" href="/admin/roster">Manage Roster</a>
			<span class="breadcrumb-item active">Pending Visiting Requests</span>
		</nav>
	</div>
</div>
<!-- END Breadcrumb -->

<!-- Main Content -->
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="block block-rounded">
				<div class="block-content">
					<div class="row">
						<div class="col-sm-12">
							<div class="table-responsive">
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th class="text-center">CID</th>
											<th class="text-center">Controller Name</th>
											<th class="text-center">Email</th>
											<th class="text-center">Rating</th>
											<th class="text-center">Home ARTCC/Division</th>
											<th class="text-center">Reason</th>
											<th class="text-center">Requested At</th>
											<th class="text-center">Actions</th>
										</tr>
									</thead>
									<tbody>
										@forelse($visit as $v)
										<tr>
											<td class="text-center">{{{$v->cid}}}</td>
											<td class="text-center">{{$v->full_name}}</td>
											<td class="text-center">{{{$v->email}}}</td>
											<td class="text-center">{{{$v->rating_short}}}</td>
											<td class="text-center">{{{$v->home}}}</td>
											<td class="text-center">{{{$v->reason}}}</td>
											<td  class="text-center">{{{$v->created_at}}}z</td>
											<td  class="text-center">{{Form::open(['action'=>['AdminController@acceptVRequest', $v->id], 'style'=>'display:inline-block'])}}
												<button type="submit" class="btn btn-success btn-sm simple-tooltip" title="Accept Request"><i class="fa fa-check"></i></button>
												{{Form::close()}}
												{{Form::open(['action'=>['AdminController@denyVRequest', $v->id], 'style'=>'display:inline-block'])}}
												<button type="submit" class="btn btn-danger btn-sm simple-tooltip" title="Reject Request"><i class="fa fa-times"></i></button>
												{{Form::close()}}
												@if($v->updated == 0)
												{{Form::open(['action'=>['AdminController@updateVRequest', $v->id], 'style'=>'display:inline-block'])}}
												<button type="submit" class="btn btn-warning btn-sm simple-tooltip" title="Send 2 Week Notification"><i class="fa fa-envelope"></i></button>
												{{Form::close()}}
												@endif</td>
										</tr>
										@empty
										<tr>
											<td colspan="8"><center>No pending visiting requests.</center></td>
										</tr>
										@endforelse
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END Main Content -->

@stop