@extends('layouts.master')

@section('title')
@parent
| Manage Roster | Administrator Center
@stop

@section('content')

<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				Manage Roster
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<span class="breadcrumb-item active">Manage Roster</span>
		</nav>
	</div>
</div>
<!-- END Breadcrumb -->

<!-- Main Content -->
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="block block-rounded">
				<div class="block-content">
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-vcenter js-dataTable-rosteradmin">
							<thead>
								<tr>
									<th class="text-center">Full Name</th>
									<th class="text-center">VATSIM ID</th>
									<th class="text-center">Email</th>
									<th class="text-center">Status</th>
									<th class="text-center">Membership</th>
									<th class="text-center">Rating</th>
									<th class="text-center">Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($homecontroller as $h)
									<tr>
									    <td class="text-center">{{{$h->backwards_name}}}</td>
									    <td class="text-center">{{{$h->id}}}</td>
									    <td class="text-center">{{{$h->email}}}</td>
										<td class="text-center">
											@if($h->loa == '1') <span class="badge badge-info">Leave</span> @else <span class="badge badge-success">Active</span> @endif
										</td>
									    <td class="text-center">Home Controller</td>
									    <td class="text-center">{{{$h->rating_long}}}</td>
									    <td class="text-center">
											<a href="/admin/roster/{{$h->id}}/edit" style="display:inline-block" class="btn btn-success btn-xs simple-tooltip" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
											 @if(Auth::user()->can('roster_del'))
												@if($h->loa == 0)
												{{Form::open(['action'=>['AdminController@setLOA', $h->id], 'style'=>'display:inline-block'])}}
													<button type="submit" class="btn btn-primary btn-xs simple-tooltip" title="Mark LOA"><i class="fa fa-pause"></i></button>
												{{Form::close()}} 
												@elseif($h->loa == 1)
												{{Form::open(['action'=>['AdminController@setActive', $h->id], 'style'=>'display:inline-block'])}}
													<button type="submit" class="btn btn-warning btn-xs simple-tooltip" title="Mark Active"><i class="fa fa-play"></i></button>
												{{Form::close()}}
												@endif
										
												{{Form::open(['action'=>['AdminController@setFormerController', $h->id], 'style'=>'display:inline-block'])}}
													<button type="submit" class="btn btn-danger btn-xs simple-tooltip" title="Delete"><i class="fa fa-times"></i></button>
												{{Form::close()}} 
											@endif 
									    </td>
									</tr>
								@endforeach
								@foreach($visitcontroller as $v)
									<tr>
										<td class="text-center">{{{$v->backwards_name}}}</td>
									    <td class="text-center">{{{$v->id}}}</td>
									    <td class="text-center">{{{$v->email}}}</td>
										<td class="text-center">
											@if($v->loa == '1') <span class="badge badge-info">Leave</span> @else <span class="badge badge-success">Active</span> @endif
										</td>
									    <td class="text-center">Visiting Controller ({{{$v->visitor_from}}})</td>
									    <td class="text-center">{{{$v->rating_long}}}</td>
									    <td class="text-center">
									  		<a href="/admin/roster/{{$v->id}}/edit" style="display:inline-block" class="btn btn-success btn-xs simple-tooltip" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
											@if(Auth::user()->can('roster_del'))
												@if($v->loa == 0)
												{{Form::open(['action'=>['AdminController@setLOA', $v->id], 'style'=>'display:inline-block'])}}
													<button type="submit" class="btn btn-primary btn-xs simple-tooltip" title="Mark LOA"><i class="fa fa-pause"></i></button>
												{{Form::close()}} 
												@elseif($v->loa == 1)
												{{Form::open(['action'=>['AdminController@setActive', $v->id], 'style'=>'display:inline-block'])}}
													<button type="submit" class="btn btn-warning btn-xs simple-tooltip" title="Mark Active"><i class="fa fa-play"></i></button>
												{{Form::close()}}
												@endif
										
												{{Form::open(['action'=>['AdminController@setFormerController', $v->id], 'style'=>'display:inline-block'])}}
													<button type="submit" class="btn btn-danger btn-xs simple-tooltip" title="Delete"><i class="fa fa-times"></i></button>
												{{Form::close()}} 
											@endif 
										</td>
									</tr>
									@endforeach
									@if(Auth::user()->can('roster_add'))
									@foreach($formercontroller as $f)
										<tr>
										  <td class="text-center">{{{$f->backwards_name}}}</td>
										  <td class="text-center">{{{$f->id}}}</td>
										  <td class="text-center">{{{$f->email}}}</td>
										  <td class="text-center"><span class="badge badge-danger">Inactive</span></td>
										  <td class="text-center">Former Controller</td>
										  <td class="text-center">{{{$f->rating_long}}}</td>
										  <td class="text-center"><a href="/admin/roster/{{$f->id}}/edit" style="display:inline-block" class="btn btn-success btn-xs simple-tooltip" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
											{{Form::open(['action'=>['AdminController@setController', $f->id], 'style'=>'display:inline-block'])}}
											<button type="submit" class="btn btn-primary btn-xs simple-tooltip" title="Add Back to Roster"><i class="fa fa-check"></i></button>
											{{Form::close()}} </td>
										</tr>
									@endforeach
									@endif
							</tbody>
						</table>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END Main Content -->

@stop
