@extends('layouts.master')

@section('title')
@parent
| Search for Student | Administrator Center
@stop

@section('content')

<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				Search for Student
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<span class="breadcrumb-item active">Training Admin</span>
			<span class="breadcrumb-item active">Search for Student</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<div class="content content-full">
	<div class="row">
		<div class="col-md-12">
			<div class="block block-themed">
				<div class="block-content">
					{{ Form::open(['action' => 'MentorController@findStudent']) }}
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									{{Form::label('controller', 'Controller:', ['class'=>'control-label'])}}
									{{Form::select('controller', $user, null, ['class'=>'form-control'])}}
								</div>
								<div class="form-group">
									{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
								</div>
							</div>
						</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>

@stop