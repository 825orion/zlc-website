@extends('layouts.master')

@section('title')
@parent
| View Training Note | Administrator Center
@stop

@section('content')

<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				Training Note
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
		</div>
	</div>
</div>
<!-- END Hero -->

@if($note)
<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<span class="breadcrumb-item active">Training Admin</span>
			<a class="breadcrumb-item" href="/admin/mentor/student/{{$note->controller->id}}">Student Profile</a>
			<span class="breadcrumb-item active">View Training Note</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<div class="content content-full">
	<div class="row">
		<div class="col-md-12">
			<div class="block block-themed">
				<div class="block-header">
					<h3 class="block-title">Training Note for {{$note->controller->full_name}} (ID: {{$note->id}})</h3>
				</div>
				<div class="block-content">
						<p><b>Session Date:</b> {{$note->date}} at {{$note->session_begin}}z</p>
						<p><b>Session Type:</b> {{$note->ses_type}}</p>
						<p><b>Position:</b> {{$note->ses_pos}}</p>
						<p><b>Training Staff:</b> {{$note->instructor->full_name}}</p>
						<p><b>Duration:</b> {{$note->duration}} minutes</p>
						<p><b>Comments:</b></p>
						<p>{{$note->comments}}</p>
				</div>
			</div>
		</div>
	</div>
</div>
@else
<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<span class="breadcrumb-item active">Training Admin</span>
			<span class="breadcrumb-item active">View Training Note</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<!-- Page Content -->
<div class="content content-full">
	<div class="row">
		<div class="col-12">
			<div class="block block-themed block-rounded">
				<div class="block-header bg-danger">
					<h3 class="block-title"><center>Error Information</center></h3>
					<div class="block-options">
					</div>
				</div>
				<div class="block-content">
					<p align="center"><i class="fa fa-exclamation-circle fa-3x"></i><br><br>We could not find the requested training note.<center><a href="/admin/dashboard" type="button" class="btn btn-hero btn-rounded btn-noborder btn-success mr-5 mb-5"><i class="si si-login mr-5"></i>Back to Admin Center</a></center></p>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- END Page Content -->
@endif

@stop