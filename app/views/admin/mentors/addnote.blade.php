@extends('layouts.master')

@section('title')
@parent
| Add Training Note | Administrator Center
@stop

@section('content')

<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				Add Training Note
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<span class="breadcrumb-item active">Training Admin</span>
			<span class="breadcrumb-item active">Add Training Note</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<div class="content content-full">
	<div class="row">
		<div class="col-md-12">
			<div class="block block-themed">
				<div class="block-content">
					{{ Form::open(['action' => 'MentorController@saveNote']) }}
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group">
									{{Form::label('controller_id', 'Controller:', ['class'=>'control-label'])}}
									{{Form::select('controller_id', $user, null, ['class'=>'form-control'])}}
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									{{Form::label('position', 'Position:', ['class'=>'control-label'])}}
									{{Form::select('position', TrainingNote::$SesPos, 1, ['class'=>'form-control'])}}
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									{{Form::label('type', 'Session Type:', ['class'=>'control-label'])}}
									{{Form::select('type', TrainingNote::$SesType, 1, ['class'=>'form-control'])}}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group">
									{{Form::label('date', 'Date (YYYY-MM-DD):', ['class'=>'control-label'])}}
										<div class="input-group date" id="date">
										{{Form::text('date', null, ['class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									{{Form::label('session_begin', 'Start Time:', ['class'=>'control-label'])}}
									{{Form::select('session_begin', TrainingNote::$TimeStart, 1, ['class'=>'form-control'])}}
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									{{Form::label('duration', 'Duration (mins):', ['class'=>'control-label'])}}
									{{Form::text('duration', null, ['class'=>'form-control'])}}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									{{Form::label('comments', 'Comments and Notes:', ['class'=>'control-label'])}}
									{{Form::textarea('comments', null, ['class'=>'form-control'])}}
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
								</div>
							</div>
						</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>

@stop