@extends('layouts.master')

@section('title')
@parent
| View Student Profile | Administrator Center
@stop

@section('content')

<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				View Student Profile: {{{$user->full_name}}} ({{{$user->id}}})
				<br /> <span class="badge badge-secondary">{{{$user->rating_long}}}</span>
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<span class="breadcrumb-item active">Training Admin</span>
			<a class="breadcrumb-item" href="/admin/mentor/students">Search for Student</a>
			<span class="breadcrumb-item active">Profile</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<!-- Main Page Content -->
<div class="content">
	<!-- Certifications -->
	<h2 class="content-heading">
		<i class="fa fa-address-card mr-5"></i> Certifications
	</h2>
	<div class="row items-push">
		<div class="col-md-6 col-xl-6">
			<div class="block block-content block-content-full ribbon-left ribbon ribbon-bookmark ribbon-danger" style="height:350px;">
				<div class="ribbon-box">Minor Certifications</div>
				<br /><br /><div class="block-content block-content-full block-content-sm">
					<table class="table table-borderless table-vcenter" style="font-size: 1.5em;">
							<tbody>
								<tr>
									@if($user->del == 2 || $user->del == 3 || $user->del == 4)
										<td><span class="badge badge-pill badge-danger"><i class="fa fa-check-circle mr-5"></i>Minor Delivery Certification</span></td>
									@elseif($user->del == 1)
										<td><span class="badge badge-pill badge-warning"><i class="fa fa-times-circle mr-5"></i>Minor Delivery Training</span></td>
									@elseif($user->del == 0)
										<td><span class="badge badge-pill badge-secondary"><i class="fa fa-times-circle mr-5"></i>No Minor Delivery Certification</span></td>
									@endif
								</tr>
								<tr>
									@if($user->gnd == 2 || $user->gnd == 3 || $user->gnd == 4)
										<td><span class="badge badge-pill badge-danger"><i class="fa fa-check-circle mr-5"></i>Minor Ground Certification</span></td>
									@elseif($user->gnd == 1)
										<td><span class="badge badge-pill badge-warning"><i class="fa fa-times-circle mr-5"></i>Minor Ground Training</span></td>
									@elseif($user->gnd == 0)
										<td><span class="badge badge-pill badge-secondary"><i class="fa fa-times-circle mr-5"></i>No Minor Ground Certification</span></td>
									@endif
								</tr>
								<tr>
									@if($user->twr == 2 || $user->twr == 3 || $user->twr == 4)
										<td><span class="badge badge-pill badge-danger"><i class="fa fa-check-circle mr-5"></i>Minor Tower Certification</span></td>
									@elseif($user->twr == 1)
										<td><span class="badge badge-pill badge-warning"><i class="fa fa-times-circle mr-5"></i>Minor Tower Training</span></td>
									@elseif($user->twr == 0)
										<td><span class="badge badge-pill badge-secondary"><i class="fa fa-times-circle mr-5"></i>No Minor Tower Certification</span></td>
									@endif
								</tr>
								<tr>
									@if($user->app == 2 || $user->app == 3 || $user->app == 4)
										<td><span class="badge badge-pill badge-danger"><i class="fa fa-check-circle mr-5"></i>Minor TRACON Certification</span></td>
									@elseif($user->app == 1)
										<td><span class="badge badge-pill badge-warning"><i class="fa fa-times-circle mr-5"></i>Minor TRACON Training</span></td>
									@elseif($user->app == 0)
										<td><span class="badge badge-pill badge-secondary"><i class="fa fa-times-circle mr-5"></i>No Minor TRACON Certification</span></td>
									@endif
								</tr>
							</tbody>
						</table>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xl-6">
		<div class="block block-content block-content-full ribbon-left ribbon ribbon-bookmark ribbon-success" style="height:350px;">
				<div class="ribbon-box">ZJX Major Certifications</div>
				<br /><br /><div class="block-content block-content-full block-content-sm">
					<table class="table table-borderless table-vcenter" style="font-size: 1.5em;">
							<tbody>
								<tr>
									@if($user->del == 4)
										<td><span class="badge badge-pill badge-success"><i class="fa fa-check-circle mr-5"></i>Orlando Delivery Certification</span></td>
									@elseif($user->del == 3)
										<td><span class="badge badge-pill badge-warning"><i class="fa fa-times-circle mr-5"></i>Orlando Delivery Training</span></td>
									@elseif($user->del == 2 || $user->del == 1 || $user->del == 0)
										<td><span class="badge badge-pill badge-secondary"><i class="fa fa-times-circle mr-5"></i>No Orlando Delivery Certification</span></td>
									@endif
								</tr>
								<tr>
									@if($user->gnd == 4)
										<td><span class="badge badge-pill badge-success"><i class="fa fa-check-circle mr-5"></i>Orlando Ground Certification</span></td>
									@elseif($user->gnd == 3)
										<td><span class="badge badge-pill badge-warning"><i class="fa fa-times-circle mr-5"></i>Orlando Ground Training</span></td>
									@elseif($user->gnd == 2 || $user->gnd == 1 || $user->gnd == 0)
										<td><span class="badge badge-pill badge-secondary"><i class="fa fa-times-circle mr-5"></i>No Orlando Ground Certification</span></td>
									@endif
								</tr>
								<tr>
									@if($user->twr == 4)
										<td><span class="badge badge-pill badge-success"><i class="fa fa-check-circle mr-5"></i>Orlando Tower Certification</span></td>
									@elseif($user->twr == 3)
										<td><span class="badge badge-pill badge-warning"><i class="fa fa-times-circle mr-5"></i>Orlando Tower Training</span></td>
									@elseif($user->twr == 2 || $user->twr == 1 || $user->twr == 0)
										<td><span class="badge badge-pill badge-secondary"><i class="fa fa-times-circle mr-5"></i>No Orlando Tower Certification</span></td>
									@endif
								</tr>
								<tr>
									@if($user->app == 4)
										<td><span class="badge badge-pill badge-success"><i class="fa fa-check-circle mr-5"></i>F11 TRACON Certification</span></td>
									@elseif($user->app == 3)
										<td><span class="badge badge-pill badge-warning"><i class="fa fa-times-circle mr-5"></i>F11 TRACON Training</span></td>
									@elseif($user->app == 2 || $user->app == 1 || $user->app == 0)
										<td><span class="badge badge-pill badge-secondary"><i class="fa fa-times-circle mr-5"></i>No F11 TRACON Certification</span></td>
									@endif
								</tr>
								<tr>
									@if($user->ctr == 2)
										<td><span class="badge badge-pill badge-success"><i class="fa fa-check-circle mr-5"></i>JAX Center Certification</span></td>
									@elseif($user->ctr == 1)
										<td><span class="badge badge-pill badge-warning"><i class="fa fa-times-circle mr-5"></i>JAX Center Training</span></td>
									@elseif($user->ctr == 0)
										<td><span class="badge badge-pill badge-secondary"><i class="fa fa-times-circle mr-5"></i>No JAX Center Certification</span></td>
									@endif
								</tr>
							</tbody>
						</table>
				</div>
			</div>
		</div>
	</div>
	<!-- END Certifications -->

	<!-- Training Notes and Exams -->
	<h2 class="content-heading">
		<i class="fa fa-sticky-note mr-5"></i> Training Information
	</h2>
	<div class="row">
		<div class="col-md-12">
			<div class="block block-rounded">
				<div class="block-header block-header-default">
					<h3 class="block-title"><i class="fa fa-pencil mr-5"></i> Training Notes</h3>
				</div>
				<div class="block-content">
					<table class="table table-striped table-vcenter">
							<thead>
								<tr>
									<th style="text-align:center;">Date</th>
									<th style="text-align:center;">Position</th>
									<th style="text-align:center;">Training Staff</th>
									<th style="text-align:center;">Type</th>
									<th style="text-align:center;">Actions</th>
								</tr>
							</thead>
							<tbody>
								@forelse($note as $n)
									<tr>
										<td style="text-align:center;">{{{$n->date}}} {{{$n->session_begin}}}z</td>
										<td style="text-align:center;">{{{$n->ses_pos}}}</td>
										<td style="text-align:center;">{{{$n->instructor->full_name}}}</td>
										<td style="text-align:center;">{{{$n->ses_type}}}</td>
										<td style="text-align:center;"><a href="/admin/mentor/note/{{{$n->id}}}" class="btn btn-success btn-sm"><i class="fa fa-info-circle"></i></a></td>
									</tr>
								@empty
									<tr>
										<td colspan="5"><center>No training notes are available.</center></td>
									</tr>
								@endforelse
							</tbody>
						</table>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="block block-rounded">
				<div class="block-header block-header-default">
					<h3 class="block-title"><i class="fa fa-graduation-cap mr-5"></i> Examinations</h3>
				</div>
				<div class="block-content">
					<table class="table table-striped table-vcenter">
							<thead>
								<tr>
									<th style="text-align:center;">Exam Name</th>
									<th style="text-align:center;">Date</th>
									<th style="text-align:center;">Score</th>
									<th style="text-align:center;">Result</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									if(count($exam)==0){
										echo '<tr><td colspan="4"><center>No exam history available.</center></td></tr>';
									}
									else{
										foreach($exam as $e){
											if($e['exam_name']!=""){
												echo '<tr><td style="text-align:center;">'.$e['exam_name'].'</td>';
												echo '<td style="text-align:center;">'.date('Y-m-d',strtotime(explode("T",$e['date'])[0])).'</td>';
												echo '<td style="text-align:center;">'.$e['score'].'%</td>';
												if($e['passed']=="1"){
													echo '<td style="text-align:center;"><span class="badge badge-pill badge-success"><i class="fa fa-check-circle mr-5"></i>Passed</span></td>';
												}else{
													echo '<td style="text-align:center;"><span class="badge badge-pill badge-danger"><i class="fa fa-close mr-5"></i>Failed</span></td>';
												}
												echo '</tr>';
											}
										}
									}
								?>
							</tbody>
							</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END Main Page Content -->

@stop
