@extends('layouts.master')

@section('title')
@parent
| Roster Tidy
@stop

@section('content')

<div class="page-heading-two">
    <div class="container">
        <h2>Roster Cleanup</h2>
    </div>
</div>
    
    
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="table-responsive">
                <table class="table table-condensed" id="datatable">
                    <thead>
                        <tr>
                            <th>CID</th>
                            <th>Name</th>
                            <th>Rating</th>
                            <th>Added to Roster</th>
                            <th>Last Controlled</th>
                            <th>Time Controlled This Quarter</th>
                            <th>Last Training Session</th>
                            <th>Actions</th>
                        </tr> 
                    </thead> 
                    <tbody>
                        @foreach($users as $user)
                        <tr> 
                            <th scope="row">{{ $user->id }}</th> 
                            @if($user->visitor == 1)
                            <td>{{ $user->full_name }} - <b>Visitor</b></td>
                            @elseif($user->loa == 1)
                            <td>{{ $user->full_name }} - <b>LOA</b></td>
                            @else
                            <td>{{ $user->full_name }}</td>
                            @endif

                            <td>{{ $user->rating_short }}</td>

                            <td>{{ date('j M Y', strtotime($user->created_at)) }}</td>
                            
                            @if($user->controlActivity())
                            <td>{{ date('j M Y', strtotime($user->controlActivity()->date)) }}</td>
                            @else
                            <td><em>N/A since 01 May 2016</em></td>
                            @endif

                            @if($user->controlActivity())
                            <?php 
                            $month = date('m');
                            if($month>=1 && $month <=3){
                                $quarter = 1;
                            }else if($month>=4 && $month <=6){
                                $quarter = 2;
                            }else if($month>=7 && $month <=9){
                                $quarter = 3;
                            }else if($month>=10 && $month <=12){
                                $quarter = 4;
                            }
                            $year = date('Y');
                            ?>
                            <td>{{$user->quarterlyHours($quarter,$year)}}</td>
                            @else
                            <td><em>N/A since 01 May 2016</em></td>
                            @endif

                            @if($user->trainingHistory())
                            <td>{{ date('j M Y', strtotime($user->trainingHistory()->date)) }}</td>
                            @else
                            <td><em>N/A since 01 May 2016</em></td>
                            @endif
                            <td>
                                <a href="/admin/roster/{{$user->id}}/edit" style="display:inline-block" class="btn btn-success btn-xs simple-tooltip" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
								 @if(Auth::user()->can('roster_del'))
    								@if($user->loa == 0)
        								{{Form::open(['action'=>['AdminController@setLOA', $user->id], 'style'=>'display:inline-block'])}}
        									<button type="submit" class="btn btn-primary btn-xs simple-tooltip" title="Mark LOA"><i class="fa fa-lock"></i></button>
        								{{Form::close()}} 
    								@elseif($user->loa == 1)
        								{{Form::open(['action'=>['AdminController@setActive', $user->id], 'style'=>'display:inline-block'])}}
        									<button type="submit" class="btn btn-info btn-xs simple-tooltip" title="Mark Active"><i class="fa fa-unlock"></i></button>
        								{{Form::close()}}
    								@endif
    						        {{Form::open(['action'=>['AdminController@sendWarning', $user->id], 'style'=>'display:inline-block'])}}
    									<button type="submit" class="btn btn-warning btn-xs simple-tooltip" title="Send Warning"><i class="fa fa-envelope"></i></button>
    								{{Form::close()}} 
    								{{Form::open(['action'=>['AdminController@setFormerController', $user->id], 'style'=>'display:inline-block'])}}
    									<button type="submit" class="btn btn-danger btn-xs simple-tooltip" title="Remove from Active Roster"><i class="fa fa-times"></i></button>
    								{{Form::close()}} 
								@endif 
						    </td>
                        </tr>
                        @endforeach
                    </tbody> 
                </table>
            </div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function(){
		$("#datatable").DataTable({
			"lengthChange": false,
			"bPaginate": false,
	        "bFilter": true,
	        "bInfo": false
		});
	});
</script>

@stop