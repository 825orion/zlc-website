@extends('layouts.master')



@section('title')

@parent

| Edit Document or Download | Administrator Center

@stop



@section('content')



<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				Edit Document or Download
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<a class="breadcrumb-item" href="/admin/docs">Documents and Downloads</a>
			<span class="breadcrumb-item active">Edit</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->


<div class="content content-full">
	<div class="row">
		<div class="col-md-12">
			<div class="block block-themed">
				<div class="block-content">
					{{Form::open(['action'=>['DocumentController@update', $Document->id], 'method' => 'PUT', 'files' => true])}}

						<div class="form-group">

							{{Form::label('name', 'Document/Download Name:', ['class'=>'control-label'])}}

							{{Form::text('name', $Document->name, ['class'=>'form-control'])}}

						</div>

						<div class="form-group">

							{{Form::label('file', 'File:', ['class'=>'control-label'])}}

							{{Form::file('file', null, ['class'=>'form-control'])}}

						</div>

						<div class="form-group">

							{{Form::label('type', 'Type:', ['class'=>'control-label'])}}

							{{Form::select('type', Document::$DownloadCats, $Document->type, ['class'=>'form-control'])}}

						</div>

						<div class="form-group">

							{{Form::label('comments', 'Comments:', ['class'=>'control-label'])}}

							{{Form::textarea('comments', $Document->comments , ['class'=>'form-control'])}}

						</div>

						<div class="form-group">

							<button type="submit" class="btn btn-success">Save</button>

						</div>

					{{Form::close()}}
				</div>
			</div>
		</div>
	</div>
</div>

@stop

