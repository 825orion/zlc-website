@extends('layouts.master')



@section('title')

@parent

| Documents and Downloads | Administrator Center

@stop



@section('content')



<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				Documents and Downloads
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
			<!-- Actions -->
			<a href="/admin/docs/create/" type="button" class="btn btn-rounded btn-hero btn-sm btn-success mb-5">
				<i class="fa fa-plus-circle mr-5"></i> Create Document or Download
			</a>
			<!-- END Actions -->
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<span class="breadcrumb-item active">Documents and Downloads</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<!-- Page Content -->
<div class="content">
	<!-- Documents and Downloads Tabs -->
	<div class="row">
		<div class="col-lg-12">
			<div class="block">
				<ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" href="#vrc">VRC</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#vstars">vSTARS</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#veram">vERAM</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#vatis">vATIS</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#sops">SOPs</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#loas">LOAs</a>
					</li>
				</ul>
				<div class="block-content tab-content">
					<div class="tab-pane active" id="vrc" role="tabpanel">
						<h4 class="font-w400">VRC</h4>
						<div class="table-responsive">

							<table class="table table-striped table-bordered">

								<thead>

									<tr>

									    <th width="17%">Name</th>

									    <th width="53%">Description</th>

									    <th width="15%">Updated</th>

										<th width="15%">Actions</th>

									</tr>

								</thead>

								<tbody>

								@forelse ($vrc as $v)

									<tr>

										<td>{{{$v->name}}}</td>

									    @if(empty($v->comments))

									    <td>No Description Available</td>

									    @else

									    <td>{{{$v->comments}}}</td>

									    @endif

									    <td>{{{$v->updated_at}}} z</td>

									    <td>

									    	<a href="{{ $v->url }}" target="_blank" class="btn btn-success btn-sm simple-tooltip" data-toggle="tooltip" title="Download"><i class="fa fa-download"></i></a>

									    	<a href="/admin/docs/{{$v->id}}/edit" class="btn btn-primary btn-sm simple-tooltip" title="Edit"><i class="fa fa-pencil"></i></a>

									    	{{Form::open(['action'=>['DocumentController@destroy', $v->id], 'method' => 'delete', 'style' => 'display: inline-block'])}}

									    		<button type="submit" class="btn btn-danger btn-sm simple-tooltip" title="Delete"><i class="fa fa-times"></i></button>

									    	{{Form::close()}}

									    </td>

									</tr>

								@empty

                                    <tr>

                                      <td colspan="4"><center>No Documents To Show</center></td>

                                    </tr>

                                @endforelse

		                    	</tbody>

		                    </table>

						</div>
					</div>
					<div class="tab-pane" id="vstars" role="tabpanel">
						<h4 class="font-w400">vSTARS</h4>
						<div class="table-responsive">

							<table class="table table-striped table-bordered">

								<thead>

									<tr>

									    <th width="17%">Name</th>

									    <th width="53%">Description</th>

									    <th width="15%">Updated</th>

										<th width="15%">Actions</th>

									</tr>

								</thead>

								<tbody>

							</tr>

								@forelse ($vstars as $vs)

									<tr>

										<td>{{{$vs->name}}}</td>

									    @if(empty($vs->comments))

									    <td>No Description Available</td>

									    @else

									    <td>{{{$vs->comments}}}</td>

									    @endif

									    <td>{{{$vs->updated_at}}} z</td>

									    <td>

									    	<a href="{{ $vs->url }}" target="_blank" class="btn btn-success btn-sm simple-tooltip" data-toggle="tooltip" title="Download"><i class="fa fa-download"></i></a>

									    	<a href="/admin/docs/{{$vs->id}}/edit" class="btn btn-primary btn-sm simple-tooltip" title="Edit"><i class="fa fa-pencil"></i></a>

									    	{{Form::open(['action'=>['DocumentController@destroy', $vs->id], 'method' => 'delete', 'style' => 'display: inline-block'])}}

									    		<button type="submit" class="btn btn-danger btn-sm simple-tooltip" title="Delete"><i class="fa fa-times"></i></button>

									    	{{Form::close()}}

									    </td>

									</tr>

								@empty

                                    <tr>

                                      <td colspan="4"><center>No Documents To Show</center></td>

                                    </tr>

                                @endforelse

		                    	</tbody>

		                    </table>

						</div>
					</div>
					<div class="tab-pane" id="veram" role="tabpanel">
						<h4 class="font-w400">vERAM</h4>
						<div class="table-responsive">

							<table class="table table-striped table-bordered">

								<thead>

									<tr>

									    <th width="17%">Name</th>

									    <th width="53%">Description</th>

									    <th width="15%">Updated</th>

										<th width="15%">Actions</th>

									</tr>

								</thead>

								<tbody>

							</tr>

								@forelse ($veram as $ver)

									<tr>

										<td>{{{$ver->name}}}</td>

									    @if(empty($ver->comments))

									    <td>No Description Available</td>

									    @else

									    <td>{{{$ver->comments}}}</td>

									    @endif

									    <td>{{{$ver->updated_at}}} z</td>

									    <td>

									    	<a href="{{ $ver->url }}" target="_blank" class="btn btn-success btn-sm simple-tooltip" data-toggle="tooltip" title="Download"><i class="fa fa-download"></i></a>

									    	<a href="/admin/docs/{{$ver->id}}/edit" class="btn btn-primary btn-sm simple-tooltip" title="Edit"><i class="fa fa-pencil"></i></a>

									    	{{Form::open(['action'=>['DocumentController@destroy', $ver->id], 'method' => 'delete', 'style' => 'display: inline-block'])}}

									    		<button type="submit" class="btn btn-danger btn-sm simple-tooltip" title="Delete"><i class="fa fa-times"></i></button>

									    	{{Form::close()}}

									    </td>

									</tr>

								@empty

                                    <tr>

                                      <td colspan="4"><center>No Documents To Show</center></td>

                                    </tr>

                                @endforelse

		                    	</tbody>

		                    </table>

						</div>
					</div>
					<div class="tab-pane" id="vatis" role="tabpanel">
						<h4 class="font-w400">vATIS</h4>
						<div class="table-responsive">

							<table class="table table-striped table-bordered">

								<thead>

									<tr>

									    <th width="17%">Name</th>

									    <th width="53%">Description</th>

									    <th width="15%">Updated</th>

										<th width="15%">Actions</th>

									</tr>

								</thead>

								<tbody>

								@forelse ($vatis as $va)

									<tr>

										<td>{{{$va->name}}}</td>

									    @if(empty($va->comments))

									    <td>No Description Available</td>

									    @else

									    <td>{{{$va->comments}}}</td>

									    @endif

									    <td>{{{$va->updated_at}}} z</td>

									    <td>

									    	<a href="{{ $va->url }}" target="_blank" class="btn btn-success btn-sm simple-tooltip" data-toggle="tooltip" title="Download"><i class="fa fa-download"></i></a>

									    	<a href="/admin/docs/{{$va->id}}/edit" class="btn btn-primary btn-sm simple-tooltip" title="Edit"><i class="fa fa-pencil"></i></a>

									    	{{Form::open(['action'=>['DocumentController@destroy', $va->id], 'method' => 'delete', 'style' => 'display: inline-block'])}}

									    		<button type="submit" class="btn btn-danger btn-sm simple-tooltip" title="Delete"><i class="fa fa-times"></i></button>

									    	{{Form::close()}}

									    </td>

									</tr>

								@empty

                                    <tr>

                                      <td colspan="4"><center>No Documents To Show</center></td>

                                    </tr>

                                @endforelse

		                    	</tbody>

		                    </table>

						</div>

					</div>
					<div class="tab-pane" id="sops" role="tabpanel">
						<h4 class="font-w400">SOPs</h4>
						<div class="table-responsive">

							<table class="table table-striped table-bordered">

								<thead>

									<tr>

									    <th width="17%">Name</th>

									    <th width="53%">Description</th>

									    <th width="15%">Updated</th>

										<th width="15%">Actions</th>

									</tr>

								</thead>

								<tbody>

								@forelse ($sop as $s)

									<tr>

										<td>{{{$s->name}}}</td>

									    @if(empty($s->comments))

									    <td>No Description Available</td>

									    @else

									    <td>{{{$s->comments}}}</td>

									    @endif

									    <td>{{{$s->updated_at}}} z</td>

									    <td>

									    	<a href="{{ $s->url }}" target="_blank" class="btn btn-success btn-sm simple-tooltip" data-toggle="tooltip" title="Download"><i class="fa fa-download"></i></a>

									    	<a href="/admin/docs/{{$s->id}}/edit" class="btn btn-primary btn-sm simple-tooltip" title="Edit"><i class="fa fa-pencil"></i></a>

									    	{{Form::open(['action'=>['DocumentController@destroy', $s->id], 'method' => 'delete', 'style' => 'display: inline-block'])}}

									    		<button type="submit" class="btn btn-danger btn-sm simple-tooltip" title="Delete"><i class="fa fa-times"></i></button>

									    	{{Form::close()}}

									    </td>

									</tr>

								@empty

                                    <tr>

                                      <td colspan="4"><center>No Documents To Show</center></td>

                                    </tr>

                                @endforelse

		                    	</tbody>

		                    </table>

						</div>
					</div>
					<div class="tab-pane" id="loas" role="tabpanel">
						<h4 class="font-w400">LOAs</h4>
						<div class="table-responsive">

							<table class="table table-striped table-bordered">

								<thead>

									<tr>

									    <th width="17%">Name</th>

									    <th width="53%">Description</th>

									    <th width="15%">Updated</th>

										<th width="15%">Actions</th>

									</tr>

								</thead>

								<tbody>

								@forelse ($loa as $l)

									<tr>

										<td>{{{$l->name}}}</td>

									    @if(empty($l->comments))

									    <td>No Description Available</td>

									    @else

									    <td>{{{$l->comments}}}</td>

									    @endif

									    <td>{{{$l->updated_at}}} z</td>

									    <td> 

									    	<a href="{{ $l->url }}" target="_blank" class="btn btn-success btn-sm simple-tooltip" data-toggle="tooltip" title="Download"><i class="fa fa-download"></i></a>

									    	<a href="/admin/docs/{{$l->id}}/edit" class="btn btn-primary btn-sm simple-tooltip" title="Edit"><i class="fa fa-pencil"></i></a>

									    	{{Form::open(['action'=>['DocumentController@destroy', $l->id], 'method' => 'delete', 'style' => 'display: inline-block'])}}

									    		<button type="submit" class="btn btn-danger btn-sm simple-tooltip" title="Delete"><i class="fa fa-times"></i></button>

									    	{{Form::close()}}</td>

									</tr>

								@empty

                                    <tr>

                                      <td colspan="4"><center>No Documents To Show</center></td>

                                    </tr>

                                @endforelse

		                    	</tbody>

		                    </table>

						</div>
					</div>
				</div>
			</div>

		</div>

	</div>
	<!-- END Documents and Downloads Tabs -->
</div>
<!-- END Page Content -->

@stop

