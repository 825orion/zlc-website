@extends('layouts.master')

@section('title')
@parent
| Edit an Event | Administrator Center
@stop

@section('content')

<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				Edit an Event
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<a class="breadcrumb-item" href="/admin/events">Events</a>
			<span class="breadcrumb-item active">Edit</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

	<div class="content content-full">
		<div class="row">
			<div class="col-md-12">
				<div class="block block-themed">
					<div class="block-content">
						<div class="container">
							 {{ Form::open(['action' => ['EventController@update', $event->id], 'method' => 'PUT']) }}
									<div class="col-sm-12">
										<div class="form-group">
											{{Form::label('title', 'Event Title:', ['class'=>'control-label'])}}
											{{Form::text('title', $event->title, ['class'=>'form-control'])}}
										</div>
										<div class="form-group">
											{{Form::label('description', 'Event Description:', ['class'=>'control-label'])}}
											{{Form::hidden('description', $event->description)}}
											@include('partials.quill', ['id' => 'description'])
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{Form::label('event_start', 'Event Start Time (MM/DD/YYY HH:MM):', ['class'=>'control-label'])}}
											<div class="input-group date" id="start-date">
												{{Form::text('event_start', $event->event_start->format('m/d/Y H:i'), ['class'=>'form-control'])}}
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											{{Form::label('event_end', 'Event End Time (MM/DD/YYY HH:MM):', ['class'=>'control-label'])}}
											<div class="input-group date" id="end-date">
												{{Form::text('event_end', $event->event_end->format('m/d/Y H:i'), ['class'=>'form-control'])}}
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											{{Form::label('banner_link', 'Banner Link:', ['class'=>'control-label'])}}
											{{Form::text('banner_link', $event->banner_link, ['class'=>'form-control'])}}
										</div>
										<div class="form-group">
											{{Form::label('host', 'Host ARTCC/Facility:', ['class'=>'control-label'])}}
											{{Form::select('host', Events::$HostLong, $event->host, ['class' => 'form-control'])}}
										</div>
									</div>

									<div class="col-sm-12">
										<div class="form-group">
											{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
										</div>
									</div>

							 {{ Form::close() }}

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	

@stop
