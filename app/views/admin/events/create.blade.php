@extends('layouts.master')

@section('title')
@parent
| Create Event | Administrator Center
@stop


@section('content')

<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				Create an Event
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<a class="breadcrumb-item" href="/admin/events">Events</a>
			<span class="breadcrumb-item active">Create</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<div class="content content-full">
	<div class="row">
		<div class="col-md-12">
			<div class="block block-themed">
				<div class="block-content">
					<div class="container">
					   {{ Form::open(['action' => 'EventController@store']) }}
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										{{Form::label('title', 'Event Title:', ['class'=>'control-label'])}}
										{{Form::text('title', null, ['class'=>'form-control'])}}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										{{Form::label('description', 'Event Description:', ['class'=>'control-label'])}}
										{{Form::hidden('description', null)}}
										@include('partials.quill', ['id' => 'description'])
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										{{Form::label('start', 'Event Start Time (MM/DD/YYYY HH:MM):', ['class'=>'control-label'])}}
										<div class="input-group date" id="start-date">
											{{Form::text('event_start', null, ['class'=>'form-control'])}}
											<span class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										{{Form::label('end', 'Event End Time (MM/DD/YYYY HH:MM):', ['class'=>'control-label'])}}
										<div class="input-group date" id="end-date">
		       								{{Form::text('event_end', null, ['class'=>'form-control'])}}
											<span class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<div class="col-sm-12" style="padding-left:0px;">
										{{Form::label('banner_link', 'Banner Link:', ['class'=>'control-label'])}}
										</div>
										<div class="col-sm-8" style="padding-left:0px; padding-right:0px;">
											{{Form::text('banner_link', null, ['class'=>'form-control'])}}
										</div>
										<!--<a class="btn btn-success btn-sm col-sm-4" data-toggle="modal" data-target="#upload-banner">Upload Banner</a>-->
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										{{Form::label('host', 'Host ARTCC/Facility:', ['class'=>'control-label'])}}
										{{Form::select('host', Events::$HostLong, 'ZJX', ['class' => 'form-control'])}}
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										{{Form::label('defaultpos', 'Default Positions:', ['class'=>'control-label'])}}
										{{Form::select('defaultpos', Events::$DefaultPos, 'null', ['class' => 'form-control'])}}
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										{{Form::submit('Submit', ['class' => 'btn btn-primary btn-block'])}}
									</div>
								</div>
							</div>

					   {{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(function () {
		var dtpOptions = {
			icons: {
				time: "fa fa-clock-o",
				date: "fa fa-calendar",
				up: "fa fa-arrow-up",
				down: "fa fa-arrow-down"
			},
			format: 'MM/DD/YYYY H:mm'
		};

		$('#start-date').datetimepicker(dtpOptions);
		$('#end-date').datetimepicker(dtpOptions);
	});
</script>

@stop
