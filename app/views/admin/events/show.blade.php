@extends('layouts.master')

@section('title')
@parent
| Event Information | Administrator Center
@stop

@section('content')

@if($event) 

<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				Event Information
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<a class="breadcrumb-item" href="/admin/events">Events</a>
			<span class="breadcrumb-item active">Information</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<div class="content content-full">
	<div class="row">
		<div class="col-md-12">
			<div class="block block-themed block-rounded">
				<div class="block-header bg-primary-dark">
					<h3 class="block-title">Event Information</h3>
					<div class="block-options">
					</div>
				</div>
				<div class="block-content">
					<div class="row">
						<div class="col-sm-12">
							<div class="well">
								<h4><img width="100%" src="{{{$event->banner_link}}}"></h4>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="panel panel-primary">
								<div class="panel-heading">
									<h3 class="font-w300">Event <small>{{{$event->title}}}</small></h3>
								</div>
								<div class="panel-body">
									<h3 class="font-w300">Host <small>{{{$event->host_long}}}</small></h3>
									<h3 class="font-w300">Start <small>{{{$event->event_start}}}z</small></h3>
									<h3 class="font-w300">End <small>{{{$event->event_end}}}z</small></h3>
									<h3 class="font-w300">Event Description</h3>
									<p>{{$event->description}}</p>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-primary">
								<div class="panel-heading">
									<h3 class="font-w300">Position Assignments</h3>
									<p>Click and drag a position to reorder it within the table.</p>
								</div>
								<div class="panel-body">
									<a class="btn btn-primary min-width-125" data-toggle="modal" data-target="#add-position">Add Position</a>
									<a class="btn btn-warning min-width-125" data-toggle="modal" data-target="#assign-position-manual">Assign Position</a><br />
									<div class="table-responsive">
										<table class="table table-striped available-positions">
											<thead>
												<tr>
													<th width="30%" style="text-align: center;">Position</th>
													<th width="50%" style="text-align: center;">Controller</th>
													<th width="20%" style="text-align: center;">Actions</th>
												</tr>
											</thead>
											<tbody>
												@foreach($event->positions->sortBy('order_index') as $position)
												<tr data-id="{{ $position->id }}">
													<td style="text-align: center;">{{$position->name}}</td>
													<td style="text-align: center;">{{$position->user ? $position->user->full_name : "No Assignment"}}</td>
													<td style="text-align: center;">
														{{ Form::open(['action' => ['EventController@deletePosition', $event->id, $position->id], 'method' => 'DELETE', 'style' => 'display: inline-block;']) }}
														<button type="submit" class="btn btn-danger btn-sm btn-addon simple-tooltip" title="Remove Position"><i class="fa fa-times"></i></button>
														{{ Form::close() }}
														@if(!$position->controller_id == null)
														{{ Form::open(['action' => ['EventController@unassignPosition', $event->id, $position->id], 'method' => 'POST', 'style' => 'display: inline-block;']) }}
														<button type="submit" class="btn btn-warning btn-sm btn-addon simple-tooltip" title="Unassign Controller"><i class="fa fa-ban"></i></button>
														{{ Form::close() }}
														@endif
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
									<div class="panel-heading">
										<h3 class="font-w300">Position Requests</h3>
									</div>
									<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr>
													<th width="30%" style="text-align: center;">Position</th>
													<th width="50%" style="text-align: center;">Controller</th>
													<th width="20%" style="text-align: center;">Actions</th>
												</tr>
											</thead>
											<tbody>
												@forelse($pos_req as $p)
												<tr>
													<td style="text-align: center;">{{$p->eventPosition->name}}</td>
													<td style="text-align: center;">{{$p->user->full_name}}</td>
													<td style="text-align: center;"> {{ Form::open(['action' => ['EventController@assignPosition', $p->position_id, $p->user->id], 'method' => 'POST', 'style' => 'display: inline-block;']) }}
													<button class="btn btn-success btn-sm btn-addon simple-tooltip" title="Accept Position"><i class="fa fa-check"></i></button>{{ Form::close() }} {{ Form::open(['action' => ['EventController@deleteRequest', $p->id], 'method' => 'DELETE', 'style' => 'display: inline-block;']) }}
													<button class="btn btn-danger btn-sm btn-addon simple-tooltip" title="Delete Request"><i class="fa fa-times"></i></button>{{ Form::close() }}
													</td>
												</tr>
												@empty
												<tr>
													<td colspan="3" style="text-align: center;">No open position requests.</td>
												</tr>
												@endforelse
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="add-position" tabindex="-1" role="dialog" aria-labelledby="add-position" style="display: none;" aria-hidden="true">
	{{ Form::open(['action' => ['EventController@createPosition', $event->id], 'method' => 'POST']) }}
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="block block-themed block-transparent mb-0">
				<div class="block-header bg-primary-dark">
					<h3 class="block-title">Add Position</h3>
					<div class="block-options">
						<button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
							<i class="si si-close"></i>
						</button>
					</div>
				</div>
				<div class="block-content">
					<div class="form-group">
						<label for="position">Position Name</label>
						<input id="position" name="name" type="text" class="form-control" />
					</div>
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-alt-danger" data-dismiss="modal">
						<i class="fa fa-close"></i> Close
					</button>
					<button type="submit" class="btn btn-alt-success">
						<i class="fa fa-check"></i> Submit
					</button>
				</div>
		</div>
	</div>
  {{ Form::close() }}
</div>

<div class="modal fade" id="assign-position-manual" tabindex="-1" role="dialog" aria-labelledby="assign-position-manual" style="display: none;" aria-hidden="true">
	{{ Form::open(['action' => ['EventController@assignPositionManual'], 'method' => 'POST']) }}
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="block block-themed block-transparent mb-0">
				<div class="block-header bg-primary-dark">
					<h3 class="block-title">Assign Position</h3>
					<div class="block-options">
						<button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
							<i class="si si-close"></i>
						</button>
					</div>
				</div>
				<div class="block-content">
					<div class="form-group">
						<label for="position_id">Position</label>
						<select name="position_id" id="position_id" class="form-control">
						@foreach($event->positions->sortBy('order_index') as $position)
						<option value="{{ $position->id }}">{{$position->name}}</option>
						@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="controller_id">Controller Name</label>
						<select name="controller_id" id="controller_id" class="form-control">
						@foreach($controllers as $c)
							<option value="{{{$c->id}}}">{{{$c->backwards_name}}}</option>
						@endforeach
						</select>
						<!--<input id="controller_id" name="controller_id" type="text" class="form-control" />-->
					</div>
				</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-alt-danger" data-dismiss="modal">
						<i class="fa fa-close"></i> Close
					</button>
					<button type="submit" class="btn btn-alt-success">
						<i class="fa fa-check"></i> Submit
					</button>
				</div>
		</div>
	</div>
  {{ Form::close() }}
</div>

<script>
var fixHelper = function(e, ui) {
	ui.children().each(function() {
		$(this).width($(this).width());
	});
	return ui;
};

$(".available-positions tbody").sortable({
	helper: fixHelper
}).disableSelection();

$(".available-positions tbody").on('sortstop', function(e, ui){
	var new_order = [];

	$(".available-positions tbody tr").each(function(i, row){
		new_order.push({
			id: $(row).data('id'),
			order_index: i
		});
	});

	$.post('/admin/events/{{ $event->id }}/sort_positions', JSON.stringify(new_order), function(r){
		console.log("Successfully resorted the positions");
	}, 'json');
})
</script>

@else
<!-- Page Content -->
<div class="content content-full">
	<div class="hero-inner">
		<div class="content content-full">
			<div class="py-30 text-center">
				<div class="display-3 text-danger">
					<i class="fa fa-warning"></i> Event Error
				</div>
				<h1 class="h2 font-w700 mt-30 mb-10">You just found an error page!</h1>
				<h2 class="h3 font-w400 text-muted mb-50">We are sorry. The event you are looking for was not found.</h2>
				<a class="btn btn-hero btn-rounded btn-alt-secondary" href="/admin/events">
					<i class="fa fa-arrow-left mr-10"></i> Back to Events
				</a>
			</div>
		</div>
	</div>
</div>
<!-- END Page Content -->
@endif


@stop
