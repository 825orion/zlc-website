@extends('layouts.master')

@section('title')
@parent
| Events | Administrator Center
@stop

@section('content')

<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
			Events
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
			<!-- Actions -->
			<a href="/admin/events/create" type="button" class="btn btn-hero btn-noborder btn-rounded btn-success mb-10 js-appear-enabled animated fadeInUp">
				<i class="fa fa-plus-circle mr-5"></i> Create an Event
			</a>
			<!-- END Actions -->
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<span class="breadcrumb-item active">Events</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<div class="content content-full">
	<div class="row">
		<div class="col-md-12">
			<div class="block block-themed">
				<div class="block-content">
					<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th width="25%">Event Title</th>
								<th width="25%">Event Start</th>
								<th width="25%">Event End</th>
								<th width="10%">Status</th>
								<th width="15%">Actions</th>
							</tr>
						</thead>
						<tbody>
						@forelse($event as $e)
							<tr>
								<td>{{{$e->title}}}</td>
								<td>{{{$e->event_start}}}</td>
								<td>{{{$e->event_end}}}</td>
								<td>{{{$e->status_long}}}</td>
								<td>@if($e->status == '1')
									{{Form::open(['action'=>['EventController@setEventHidden', $e->id], 'style'=>'display:inline-block'])}}
									<button type="submit" class="btn btn-warning btn-sm simple-tooltip" title="Set Hidden"><i class="fa fa-eye-slash"></i></button>
									{{Form::close()}}
									@elseif($e->status == '0')
									{{Form::open(['action'=>['EventController@setEventActive', $e->id], 'style'=>'display:inline-block'])}}
										<button type="submit" class="btn btn-success btn-sm simple-tooltip" title="Set Active"><i class="fa fa-eye"></i></button>
									{{Form::close()}}
									@endif
									{{Form::open(['action'=>['EventController@destroy', $e->id], 'method'=>'delete', 'style'=>'display:inline-block'])}}
										<button type="submit" class="btn btn-danger btn-sm simple-tooltip" title="Delete"><i class="fa fa-times"></i></button>
									{{Form::close()}}
									<a href="/admin/events/{{$e->id}}/edit" style="display:inline-block" class="btn btn-success btn-sm simple-tooltip" title="Edit"><i class="fa fa-pencil-square-o"></i></a> <a href="/admin/events/{{$e->id}}" style="display:inline-block" class="btn btn-info btn-sm simple-tooltip" title="View"><i class="fa fa-info-circle"></i></a></td>
							</tr>
						@empty
							<tr>
								<td colspan="5">No Events To Show</td>
							</tr>
						@endforelse
						</tbody>
					</table>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop