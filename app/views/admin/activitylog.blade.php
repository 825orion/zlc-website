@extends('layouts.master')



@section('title')

@parent

| Website Activity Log | Administrator Center

@stop



@section('content')



<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				Website Activity Log
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<span class="breadcrumb-item active">Website Activity Log</span>
		</nav>
	</div>
</div>
<!-- END Breadcrumb -->

<!-- Main Content -->
<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="block block-rounded">
				<div class="block-content">

					<div class="table-responsive">

						<table class="table table-bordered table-striped table-vcenter js-dataTable-activitytable">

							<thead>

								<tr>

									<th width="20%">User</th>

									<th width="50%">Activity</th>

									<th width="30%">Date</th>

								</tr>

							</thead>

							<tbody>

								@foreach($activity as $l)

								<tr>

									<td>{{ $l->User->full_name }}</td>

									<td>{{ $l->note }}</td>

									<td>{{ $l->created_at }}</td>

								</tr>

								@endforeach

							</tbody>

						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END Main Content -->



@stop