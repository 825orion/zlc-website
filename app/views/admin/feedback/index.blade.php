@extends('layouts.master')

@section('title')
@parent
| Pending Feedback | Administrator Center
@stop

@section('content')

<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				Pending Feedback
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<span class="breadcrumb-item active">Pending Feedback</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<div class="content content-full">
	<div class="row">
		<div class="col-md-12">
			<div class="block block-themed">
				<div class="block-content">
					<div class="row">
						<div class="col-sm-12">
							<div class="table-responsive">
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th><center>Controller</center></th>
											<th><center>Service Level</center></th>
											<th><center>Position</center></th>
											<th><center>Pilot Information</center></th>
											<th><center>Pilot Comments</center></th>
											<th><center>Date</center></th>
											<th width="10%"><center>Actions</center></th>
										</tr>
									</thead>
									<tbody>
										@forelse($feedback as $h)
										<tr>
											<td>{{{$h->controller->full_name}}} ({{{$h->controller->id}}})</td>
											<td>{{{$h->level_text}}}</td>
											<td>{{{$h->position}}}</td>
											<td>{{{$h->pilot_name}}} ({{{$h->pilot_id}}}) flying as {{{$h->flight_callsign}}}</td>
											<td>{{{$h->comments}}}</td>
											<td>{{{$h->created_at}}}z</td>
											<td>
												<a href="#" data-toggle="modal" data-target="#approvalModal-{{$h->id}}" class="btn btn-success btn-sm simple-tooltip" title="Accept Feedback"><i class="fa fa-check"></i></a>
												{{Form::open(['action'=>['FeedbackController@rejectFeedback', $h->id], 'style'=>'display:inline-block'])}}
													<button type="submit" class="btn btn-danger btn-sm simple-tooltip" title="Reject Feedback"><i class="fa fa-times"></i></button>
												{{Form::close()}}
											</td>
										</tr>
										@empty
										<tr><td colspan="7">No pending feedback.</td></tr>
										@endforelse
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@forelse($feedback as $h)
<div class="modal fade" id="approvalModal-{{$h->id}}" tabindex="-1" role="dialog" aria-labelledby="approvalModal-{{$h->id}}" style="display: none;" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		{{ Form::open(['action'=>['FeedbackController@approveFeedback', $h->id]])}}
		<div class="block block-themed block-transparent mb-0">
			<div class="block-header bg-primary-dark">
				<h3 class="block-title">Approve Feedback</h3>
				<div class="block-options">
					<button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
						<i class="si si-close"></i>
					</button>
				</div>
			</div>
			<div class="block-content">
				<div class="form-group">
					<label>
					Pilot Comments
					</label>
					<textarea name="comments" class="form-control">{{$h->comments}}</textarea>
				</div>
				<div class="form-group">
					<label>
					Staff Comments
					</label>
					<textarea name="staff_comments" class="form-control"></textarea>
				</div>
			</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-alt-danger" data-dismiss="modal">
					<i class="fa fa-close"></i> Close
				</button>
				<button type="submit" class="btn btn-alt-success">
					<i class="fa fa-check"></i> Submit
				</button>
			</div>

		{{ Form::close() }}
	</div>
</div>
</div>
@empty
@endforelse


   
@stop