@extends('layouts.master')

@section('title')
@parent
| Manage Scenery | Administrator Center
@stop

@section('content')

<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				Manage Scenery
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
			
			<!-- Actions -->
			<a href="/admin/scenery/create" type="button" class="btn btn-rounded btn-hero btn-sm btn-success mb-5">
				<i class="fa fa-plus mr-5"></i> Add Scenery
			</a>
			<!-- END Actions -->
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<span class="breadcrumb-item active">Manage Scenery</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<!-- Page Content -->
<div class="content">
	<!-- Scenery Tabs -->
	<div class="row">
		<div class="col-lg-12">
			<div class="block">
				<ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" href="#fsx">FSX/P3D</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#xplane">X-Plane</a>
					</li>
				</ul>
				<div class="block-content tab-content">
					<div class="tab-pane active" id="fsx" role="tabpanel">
						<h4 class="font-w400">FSX/P3D Scenery</h4>
						<div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="30%"><center>Name</center></th>
                                        <th width="60%"><center>Description</center></th>
                                        <th width="10%"><center>Actions</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($fsx as $f)
                                    <tr>
                                      <td>{{{$f->name}}}</td>
                                      <td>{{$f->description}}</td>
                                      <td><a href="/admin/scenery/{{$f->id}}/edit" style="display:inline-block" class="btn btn-success btn-sm simple-tooltip" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                          {{Form::open(['action'=>['SceneryController@destroy', $f->id], 'method' => 'delete', 'style'=>'display:inline-block'])}}
                                                <button type="submit" class="btn btn-danger btn-sm simple-tooltip" title="Delete"><i class="fa fa-times"></i></button>
                                            {{Form::close()}}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                      <td colspan="3"><center>No Scenery To Show</center></td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
					</div>
					<div class="tab-pane" id="xplane" role="tabpanel">
						<h4 class="font-w400">X-Plane Scenery</h4>
					   <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="30%"><center>Name</center></th>
                                        <th width="60%"><center>Description</center></th>
                                        <th width="10%"><center>Actions</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($xpl as $x)
                                    <tr>
                                      <td><center>{{{$x->name}}}</center></td>
                                      <td><center>{{$x->description}}</center></td>
                                      <td><a href="/admin/scenery/{{$x->id}}/edit" style="display:inline-block" class="btn btn-success btn-sm simple-tooltip" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                          {{Form::open(['action'=>['SceneryController@destroy', $x->id], 'method' => 'delete', 'style'=>'display:inline-block'])}}
                                                <button type="submit" class="btn btn-danger btn-sm simple-tooltip" title="Delete"><i class="fa fa-times"></i></button>
                                            {{Form::close()}}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                      <td colspan="3"><center>No Scenery To Show</center></td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
					</div>
				</div>
			</div>

		</div>

	</div>
	<!-- END Scenery Tabs -->


@stop