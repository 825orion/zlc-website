@extends('layouts.master')

@section('title')
@parent
| Add Scenery | Administrator Center
@stop

@section('content')

<!-- Hero -->
<div class="bg-gd-dusk">
	<div class="bg-black-op-25">
		<div class="content content-top content-full text-center">
			<h1 class="h3 text-white font-w700 mb-10">
				Add Scenery
			</h1>
			<h2 class="h4 font-w400 text-white-op">Jacksonville ARTCC Administrator Center</h2>
		</div>
	</div>
</div>
<!-- END Hero -->

<!-- Breadcrumb -->
<div class="bg-body-light border-b">
	<div class="content py-5 text-center">
		<nav class="breadcrumb bg-body-light mb-0">
			<a class="breadcrumb-item" href="/">Home</a>
			<a class="breadcrumb-item" href="/admin/dashboard">Administrator Center</a>
			<a class="breadcrumb-item" href="/admin/scenery">Manage Scenery</a>
			<span class="breadcrumb-item active">Add</span>
		</nav>
	</div>
</div>
<!-- End Breadcrumb -->

<div class="content content-full">
	<div class="row">
		<div class="col-md-12">
			<div class="block block-themed">
				<div class="block-content">
					{{ Form::open(['action' => 'SceneryController@store']) }}
						<div class="form-group">
							{{Form::label('name', 'Scenery Name:', ['class'=>'control-label'])}}
							{{Form::text('name', null, ['class'=>'form-control'])}}
						</div>
						<div class="form-group">
							{{Form::label('description', 'Description:', ['class'=>'control-label'])}}
							{{Form::textarea('description', null, ['class'=>'form-control'])}}
						</div>
						<div class="form-group">
							{{Form::label('type', 'Scenery For:', ['class'=>'control-label'])}}
							{{Form::select('type', array(
														'1' => 'FSX/P3D',
														'2' => 'X-Plane'), null, ['class'=>'form-control'])}}
						</div>
						<div class="form-group">
							{{Form::label('link', 'Link To Scenery:', ['class'=>'control-label'])}}
							{{Form::text('link', null, ['class'=>'form-control'])}}
						</div>
						<div class="form-group">
							{{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
						</div>

				   {{ Form::close() }} 
				</div>
			</div>
		</div>
	</div>
</div>

@stop